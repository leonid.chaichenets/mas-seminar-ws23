#!/usr/bin/env python
import os
import sys
from setuptools import setup, find_packages

if sys.version_info < (3, 5, 0, 'final', 0):
    raise SystemExit('Python 3.5 or later is required.')

module_dir = os.path.dirname(os.path.abspath(__file__))

setup(name='rotagaporp-c',
      version='2019.4a1',
      description='Scalable propagators for classical molecular dynamics',
      long_description=open(os.path.join(module_dir, 'README.md')).read(),
      author='Ivan Kondov',
      author_email='ivan.kondov@kit.edu',
      url='https://git.scc.kit.edu/jk7683/rotagaporp-c',
      keywords=['molecular dynamics', 'time integrator', 'propagator'],
      license='License :: Other/Proprietary License',
      packages=find_packages(),
      package_data={},
      install_requires=['ase>=3.17.0', 'cloudpickle>=0.8.1',
                        'matplotlib>=3.0.3', 'mpmath>=1.1.0', 'nose>=1.3.7',
                        'numpy>=1.16.2', 'scipy>=1.2.1', 'sympy>=1.3',
                        'theano>=1.0.4', 'tqdm>=4.31.1'],
      extras_require={'examples': ['pandas>=0.24.2', 'pycairo>=1.18.0',
                                   'pyqt5>=5.12.1', 'pyyaml>=5.1',
                                   'tensorflow>=1.13.1']},
      classifiers=['Programming Language :: Python :: 3 :: Only',
                   'Development Status :: 3 - Alpha',
                   'Intended Audience :: Science/Research',
                   'Operating System :: OS Independent',
                   'Environment :: Console',
                   'Topic :: Scientific/Engineering :: Mathematics',
                   'Topic :: Scientific/Engineering :: Physics',
                   'Topic :: Scientific/Engineering :: Chemistry'],
      test_suite='tests',
      tests_require=['nose'])
