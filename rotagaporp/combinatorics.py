""" Auxiliary functions to compute coefficients from recursion expressions """
from itertools import combinations
import numpy as np
import sympy as sp
from scipy.special import binom, factorial

def binomial(n, i):
    """ Own function for binomial coefficient. The scipy.special.binom()
        function makes an error for n >= 32, probably due to rounding """
    result = 1
    for j in range(1, n-i+1):
        result = result * (i+j) // j
    return result

def cnk_newton_1(lambdas, nchtr, dtype=complex, fpprec='fixed', prec=None):
    """ Evaluate the expansion coefficients C^(n)_k of the Newton polynomial
    prod_j=0^{n-1} (L-lambda_j) = sum_k=1^n C^(n)_k L^{n-k} using the formula
    C^(n)_k = (-1)^k sum_l=1^binom(n,k) [lambda_0 ... lambda_{n-1}]^k, n > 0
    where [lambda_0 ... lambda_{n-1}]^k product of k-combinations from
    n lambdas and C^(n)_k = 1 for k = 0 """

    zero = {'fixed': dtype(0), 'multi': sp.N(dtype(0), prec)}
    one = {'fixed': dtype(1), 'multi': sp.N(dtype(1), prec)}

    assert nchtr <= len(lambdas)
    cnk = np.full((nchtr+1, nchtr+1), zero[fpprec])
    cnk[:, 0] = one[fpprec]
    for n in range(1, nchtr+1):
        for k in range(1, n+1):
            cnk[n, k] = zero[fpprec]
            for comb in combinations(lambdas[:n], k):
                cnk[n, k] = cnk[n, k] + np.prod(comb)
            if k % 2 > 0: cnk[n, k] = -cnk[n, k]
    return cnk

def cnk_newton_2(lambdas, nchtr, dtype=complex, fpprec='fixed', prec=None):
    """ Evaluate the expansion coefficients C^(n)_k of Newton polynomial
    prod_j=0^{n-1} (L-lambda_j) = sum_k=1^n C^(n)_k L^{n-k} as the
    coefficients of the polynomial prod_j=0^{n-1} (1-lambda_j x) """

    from functools import reduce
    import operator

    zero = {'fixed': dtype(0), 'multi': sp.N(dtype(0), prec)}
    one = {'fixed': dtype(1), 'multi': sp.N(dtype(1), prec)}

    # algorithm not valid for zeros
    assert not np.any(np.isclose(lambdas, zero[fpprec]))
    assert nchtr <= len(lambdas)
    domains = {complex: 'CC', float: 'RR', int: 'ZZ'}
    z = sp.Symbol('z')
    cnk = np.full((nchtr+1, nchtr+1), zero[fpprec])
    cnk[0, 0] = one[fpprec]
    for n in range(1, nchtr+1):
        product = reduce(operator.mul, (1 - l*z for l in lambdas[:n]), 1)
        poly = sp.Poly(product, domain=domains[dtype])
        cnk[n, :n+1] = np.array(list(reversed(poly.coeffs())))
    return cnk

def cnk_chebyshev_T1(n, k, signed=True):
    """ Calculate the coefficients C^(n)_k to represent non-recursively the
        Chebyshev polynomials of the first kind T_n(x), i.e.
        T_n(x) = sum_k^n C^(n)_k x^k. Source: http://oeis.org/A008310
        Unsigend version from http://oeis.org/A081265 (recursion with +) """
    if n == k == 0:
        cnk = 1
    elif n >= k and (n+k)%2 == 0:
        fact = factorial(np.arange(n+k), exact=True)
        cnk = int(2**(k-1)*n*fact[(n+k)//2-1]/(fact[(n-k)//2]*fact[k]))
        if signed:
            cnk *= (-1)**((n-k)//2)
    else:
        cnk = 0
    return cnk

def cnk_chebyshev_T2(n, k, signed=True):
    """ Calculate the coefficients C^(n)_k to represent non-recursively the
        Chebyshev polynomials of the first kind T_n(x), i.e.
        T_n(x) = sum_k^n C^(n)_k x^k. Source: http://oeis.org/A053120
        Unsigend version from http://oeis.org/A081265 (recursion with +) """
    if n < k or (n+k)%2 != 0:
        cnk = 0
    elif k == 0 and n%2 == 0:
        cnk = (-1)**(n//2) if signed else 1
    else:
        cnk = int((2**(k-1))*n*binom((n+k)//2-1, k-1)//k)
        if signed:
            cnk *= ((-1)**((n+k)//2+k))
    return cnk

def cnk_chebyshev_T3(n, k, signed=True):
    """ Calculate the coefficients C^(n)_k to represent non-recursively the
        Chebyshev polynomials of the first kind T_n(x), i.e.
        T_n(x) = sum_k^n C^(n)_k x^k. Source: http://oeis.org/A053120
        Unsigend version from http://oeis.org/A081265 (recursion with +) """

    if k == -1 or n < k:
        cnk = 0
    elif n == k == 0 or n == k == 1:
        cnk = 1
    else:
        cnkrecu = cnk_chebyshev_T3
        cnk = 2*cnkrecu(n-1, k-1, signed)
        cnk += -cnkrecu(n-2, k, True) if signed else cnkrecu(n-2, k, False)
    return cnk

def cnk_chebyshev_U1(n, k, signed=True):
    """ Coefficients C^(n)_k to compute non-recursively the Chebyshev
    polynomials of the second kind U_n(x), i.e. U_n(x) = sum_k^n C^(n)_k x^k.
    Source: https://oeis.org/A053117. This algorithm is direct (non-recursive)
    and 30x faster than the recursive algorithm cnk_chebyshev_U2(). """

    if n >= k and (n + k) % 2 == 0:
        cnk = 2**k*int(binom((n+k)//2, k))
        if signed:
            cnk *= (-1)**((n+k)//2+k)
    else:
        cnk = 0
    return cnk

def cnk_chebyshev_U2(n, k, signed=True):
    """ Coefficients C^(n)_k to compute non-recursively the Chebyshev
    polynomials of the second kind U_n(x), i.e. U_n(x) = sum_k^n C^(n)_k x^k.
    Source: https://oeis.org/A053117. This algorithm generates the
    coefficients C^(n)_k recursively. """

    if k == -1 or n == -1:
        cnk = 0
    elif n == 0 and k == 0:
        cnk = 1
    elif n >= k and (n + k) % 2 == 0:
        cnkrecu = cnk_chebyshev_U2
        cnk = 2*cnkrecu(n-1, k-1, signed)
        cnk += -cnkrecu(n-2, k, True) if signed else cnkrecu(n-2, k, False)
    else:
        cnk = 0
    return cnk

def cnk_chebyshev_U3(n, k, signed=True):
    """ Coefficients C^(n)_k to compute non-recursively the Chebyshev
    polynomials of the second kind U_n(x), i.e. U_n(x) = sum_k^n C^(n)_k x^k.
    Source: https://oeis.org/A053117. This algorithm calculates the
    coefficients C^(n)_k via a summation. """

    if (n + k) % 2 == 0:
        cnk = sum(binomial((n+k)//2, i)*binomial((n+k)//2-i, (n-k)//2)
                  for i in range(k+1))
        if signed:
            cnk *= (-1)**((n-k)//2)
    else:
        cnk = 0
    return cnk
