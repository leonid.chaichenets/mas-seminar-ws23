""" Classes to construct polynomials of the classical Liouville operator """
import sympy as sp
import numpy as np
from generic import _float

q = sp.Symbol('q')
p = sp.Symbol('p')

class LiouvillePowers:
    """ Class for one-particle systems with 2D phase space (q, p) """
    def __init__(self, maxorder, hamiltonian=sp.Function('H')(q, p),
                 coordinate=q, momentum=p, function=sp.Function('f')(q, p),
                 h_mixed=True, f_mixed=True, scale=1, shift=0, nodes=None,
                 recursion='taylor', sign=None, fpprec='fixed'):
        """ nodes are Newton intepolation points if recursion='newton',
            sign is +1/-1 in recursion expression if recusion='chebyshev' """

        if recursion == 'chebyshev':
            assert sign is not None
        if recursion == 'newton':
            assert nodes is not None
        self.H = hamiltonian
        self.q = coordinate
        self.p = momentum
        self.f = function
        self.h_mixed = (sp.diff(self.H, self.q, self.p) != 0) if h_mixed is None else h_mixed
        self.f_mixed = (sp.diff(self.f, self.q, self.p) != 0) if f_mixed is None else f_mixed
        rules = []
        if not self.h_mixed:
            # H separable in q and p
            rules.append((sp.diff(self.H, self.q, self.p), 0))
        if not self.f_mixed:
            # f separable in q and p
            rules.append((sp.diff(self.f, self.q, self.p), 0))
        self.maxorder = maxorder

        A1 = sp.diff(self.H, self.q)
        B1 = sp.diff(self.H, self.p)
        self.Lf = [self.f]
        for n in range(1, self.maxorder):
            alf = sp.diff(self.Lf[n-1], self.q)
            blf = sp.diff(self.Lf[n-1], self.p)
            expr = scale*(B1*alf-A1*blf) + shift*self.Lf[n-1]
            if recursion == 'chebyshev' and n > 1:
                expr = 2*expr + sign*self.Lf[n-2]
            if recursion == 'newton':
                expr = expr - nodes[n-1]*self.Lf[n-1]
            self.Lf.append(expr.subs(rules).doit().expand())
        if fpprec == 'fixed':
            self.get_val_float = self.get_val_float64
        else:
            self.get_val_float = self.get_val_direct

    def get_expr(self, hamiltonian=None, function=None):
        """ return a list of the evaluated expressions """
        if hamiltonian or function:
            Lf_str = []
            for n in range(self.maxorder):
                ilnf = self.Lf[n]
                if hamiltonian:
                    ilnf = ilnf.subs(self.H, hamiltonian)
                if function:
                    ilnf = ilnf.subs(self.f, function)
                Lf_str.append(ilnf.doit())
        else:
            Lf_str = self.Lf
        return Lf_str

    def get_val_direct(self, qval, pval):
        """ evaluates (iL)^n f for given values of q and p with
            no float type casting """
        repl = {self.q: qval, self.p: pval}
        return [expr.xreplace(repl) for expr in self.Lf]

    def get_val_float64(self, qval, pval):
        """ evaluates (iL)^n f for given values of q and p with
            casting to float64 """
        repl = {self.q: qval, self.p: pval}
        return [float(expr.xreplace(repl)) for expr in self.Lf]

    def get_string(self):
        """ representation in terms of A_k and B_k """
        A = [sp.diff(self.H, self.q, n) for n in range(self.maxorder)]
        B = [sp.diff(self.H, self.p, n) for n in range(self.maxorder)]
        repl = [(sp.S.Zero, sp.Symbol('0'))]
        for n in reversed(range(self.maxorder)):
            repl.append((A[n], sp.Symbol('A'+str(n))))
            repl.append((B[n], sp.Symbol('B'+str(n))))
        if self.f != self.q and self.f != self.p:
            aa = [sp.diff(self.f, self.q, n) for n in range(self.maxorder)]
            bb = [sp.diff(self.f, self.p, n) for n in range(self.maxorder)]
            for n in reversed(range(1, self.maxorder)):
                repl.append((aa[n], sp.Symbol('a'+str(n))))
                repl.append((bb[n], sp.Symbol('b'+str(n))))
        return [expr.subs(repl) for expr in self.Lf]

    def __repr__(self):
        """ string representation of the object """
        return 'iL^n (f: %s): %s' % (self.f, self.get_string())


class LiouvillePowersQP:
    """ One-particle Liouville applied to phase space variables q and p """
    def __init__(self, nterms, system, **kwargs):
        self.qobj = LiouvillePowers(nterms, system.hamiltonian, system.q,
                                    system.p, function=system.q, **kwargs)
        self.pobj = LiouvillePowers(nterms, system.hamiltonian, system.q,
                                    system.p, function=system.p, **kwargs)

    def get_val_float(self, qv, pv):
        """ evaluates the expressions for given values of q and p """
        return (self.qobj.get_val_float(qv, pv), self.pobj.get_val_float(qv, pv))


class SPLP:
    """ Polynomials and monomials of single-particle Liouville operator L """

    def __init__(self, nterms, hamiltonian, coordinate, momentum, function,
                 scale=1, recursion='taylor', nodes=None, sign=None,
                 parity=None, fpprec='fixed'):
        """ * nterms: number of polynomials, the highest order (nterms-1)
            * hamiltonian: expression of the system Hamiltonian
            * coordinate: coordinate symbol as in the Hamiltonian expression
            * momentum: momentum symbol as in the Hamiltonian expression
            * function: a system variable to which iL is applied
            * scale: a scaling factor for L
            * nodes: a list of Newton interpolation points
            * sign: the sign in the Chebyshev polynomial recursion formula
               > sign = +1 for Chebyshev polynomials with imaginary argument
               > sign = -1 for Chebyshev polynomials with real argument
            * parity: parity of the Chebyshev polynomials: 0: even, 1: odd """

        self.nterms = nterms
        self.H = hamiltonian
        self.q = coordinate
        self.p = momentum
        self.f = function
        self.fpprec = fpprec
        self.sign = sign
        self.scale = scale
        self.nodes = nodes
        self.parity = parity
        self.A = sp.diff(self.H, self.q).doit()*self.scale
        self.B = sp.diff(self.H, self.p).doit()*self.scale
        getattr(self, recursion)()

    def poisson(self, phi):
        """ evaluate the Poisson bracket applied to variable phi, iL.phi """
        alf = sp.diff(phi, self.q).doit()
        blf = sp.diff(phi, self.p).doit()
        return self.B*alf - self.A*blf

    def chebyshev(self):
        """ evaluate the Chebyshev polynomials with argument iL """
        assert self.sign in (-1, 1)
        phi0 = self.f
        phi1 = self.poisson(self.f)
        if self.parity is None:
            self.Lf = [phi0, phi1]
            for k in range(2, self.nterms):
                phik = 2*self.poisson(self.Lf[k-1]) + self.sign*self.Lf[k-2]
                self.Lf.append(phik.expand())
        else:
            assert self.parity in (0, 1)
            assert (self.nterms-1)%2 == self.parity
            if self.parity == 0:
                phi2 = 2*self.poisson(phi1) + self.sign*self.f
                self.Lf = [phi0, phi2]
            else:
                phi3 = 4*self.poisson(self.poisson(phi1)) + self.sign*3*phi1
                self.Lf = [phi1, phi3]
            for k in range(2, (self.nterms-1)//2+1):
                sq_psi = self.poisson(self.poisson(self.Lf[k-1]))
                psik = 4*sq_psi + self.sign*2*self.Lf[k-1] - self.Lf[k-2]
                self.Lf.append(psik.expand())

    def newton(self):
        """ evaluate the Newton basis polynomials with argument iL """
        assert self.nodes is not None
        self.Lf = [self.f]
        for n in range(1, self.nterms):
            expr = self.poisson(self.Lf[n-1]) - self.nodes[n-1]*self.Lf[n-1]
            self.Lf.append(expr.expand())

    def taylor(self):
        """ evaluate the monomials (iL)^n.f for the Liouville operator L """
        self.Lf = [self.f]
        for n in range(1, self.nterms):
            self.Lf.append(self.poisson(self.Lf[n-1]).expand())

    def get_val_float(self, qval, pval):
        """ evaluate the symbolic expressions for given values of q and p """
        repl = {self.q: qval, self.p: pval}
        auxfunc = _float(self.fpprec, lambda x, y: x.xreplace(y))
        return [auxfunc(expr, repl) for expr in self.Lf]

    def __repr__(self):
        """ string representation of the object in terms of A_k and B_k """
        A = [sp.diff(self.H, self.q, n) for n in range(self.nterms)]
        B = [sp.diff(self.H, self.p, n) for n in range(self.nterms)]
        repl = [(sp.S.Zero, sp.Symbol('0'))]
        for n in reversed(range(self.nterms)):
            repl.append((A[n], sp.Symbol('A'+str(n))))
            repl.append((B[n], sp.Symbol('B'+str(n))))
        if self.f != self.q and self.f != self.p:
            aa = [sp.diff(self.f, self.q, n) for n in range(self.nterms)]
            bb = [sp.diff(self.f, self.p, n) for n in range(self.nterms)]
            for n in reversed(range(1, self.nterms)):
                repl.append((aa[n], sp.Symbol('a'+str(n))))
                repl.append((bb[n], sp.Symbol('b'+str(n))))
        return 'P_n (f: %s): %s' % (self.f, [e.subs(repl) for e in self.Lf])


class SPLPQP:
    """ Polynomials / monomials of SP Liouville operator applied to q and p """
    def __init__(self, nterms, system, **kwargs):
        self.qobj = SPLP(nterms, system.hamiltonian, system.q, system.p,
                         function=system.q, **kwargs)
        self.pobj = SPLP(nterms, system.hamiltonian, system.q, system.p,
                         function=system.p, **kwargs)
        self.ilnqcart = self.qobj.Lf
        self.ilnpcart = self.pobj.Lf

    def get_val_float(self, qval, pval):
        """ evaluate the symbolic expressions for given values of q and p """
        qvec = self.qobj.get_val_float(qval, pval)
        pvec = self.pobj.get_val_float(qval, pval)
        return (qvec, pvec)


class SPLPNR(SPLP):
    """ use the identity (iL)^n q == (iL)^{n-1} p which is valid if
        H(q, p) = p^2/2 + V(q) and non-recursive Chebyshev polynomial
        expansion or Newtonian polynomial interpolation """

    def __init__(self, nterms, system, recursion=None, **kwargs):
        """ the kinetic energy must be explicit and H(q, p) = p^2/2 + V(q) """
        from combinatorics import cnk_chebyshev_T2, cnk_newton_1

        assert recursion in ['chebyshev', 'newton']
        self.scale = kwargs.get('scale', 1)
        fpprec = kwargs.get('fpprec', 'fixed')
        prec = kwargs.get('prec', None)
        # current implementation only for float
        self.replf = _float(fpprec, lambda x, y: x.xreplace(y))

        if recursion == 'chebyshev':
            sign = kwargs.get('sign')
            assert sign is not None, 'need the sign convention of expansion'
            signed = True if sign == -1 else False
            vfunc = np.vectorize(cnk_chebyshev_T2)
            self.cnk = np.fromfunction(vfunc, (nterms, nterms), dtype=int,
                                       signed=signed)
        else:
            dtype = kwargs.get('dtype', float)
            nodes = kwargs.get('nodes')
            self.cnk = cnk_newton_1(nodes, nterms-1, dtype=dtype,
                                    fpprec=fpprec, prec=prec)

        super().__init__(nterms, system.hamiltonian, coordinate=system.q,
                         momentum=system.p, function=system.p,
                         recursion='taylor', **kwargs)
        self.ilnp = self.Lf
        self.ilnq = [self.q] + [e*self.scale for e in self.ilnp[:-1]]
        if recursion == 'chebyshev':
            self.ilnqexpr = np.inner(self.ilnq, self.cnk)
            self.ilnpexpr = np.inner(self.ilnp, self.cnk)
            self.get_val_float = self.get_val_float_nrcheb
        else:
            self.ilnqexpr = self.expand_newt_expr(self.ilnq)
            self.ilnpexpr = self.expand_newt_expr(self.ilnp)
            self.get_val_float = self.get_val_float_nrnewt

    def expand_newt_expr(self, iln):
        """ evaluate the Newton polynomial using the C^n_k coefficients """
        newt = []
        for num in range(len(iln)):
            ilnrev = reversed(iln[:num+1])
            newt.append(sum(self.cnk[num, k]*t for k, t in enumerate(ilnrev)))
        return np.array(newt)

    def get_val_float_direct(self, qval, pval):
        """ evaluate non-recursively Chebyshev or Newtonian polynomials
            numerically for given values of q and p """
        repl = {self.q: qval, self.p: pval}
        qvec = [self.replf(expr, repl) for expr in self.ilnqexpr]
        pvec = [self.replf(expr, repl) for expr in self.ilnpexpr]
        return (np.array(qvec), np.array(pvec))

    def get_val_float_nrcheb(self, qval, pval):
        """ evaluate Chebyshev polynomials non-recursively for given values
            of q and p using the identity (iL)^n q == (iL)^{n-1} p """
        repl = {self.q: qval, self.p: pval}
        ilnprec = [pval] + [self.replf(expr, repl) for expr in self.ilnp[1:]]
        ilnqrec = [qval] + [e*self.scale for e in ilnprec[:-1]]
        qvec = np.inner(np.array(ilnqrec), self.cnk)
        pvec = np.inner(np.array(ilnprec), self.cnk)
        return (qvec, pvec)

    def get_val_float_nrnewt(self, qval, pval):
        """ evaluate the Newton polynomials non-recursively for given values
            of q and p using the identity (iL)^n q == (iL)^{n-1} p """
        repl = {self.q: qval, self.p: pval}
        ilnprec = [pval] + [self.replf(expr, repl) for expr in self.ilnp[1:]]
        ilnqrec = [qval] + [e*self.scale for e in ilnprec[:-1]]
        qvec = self.expand_newt_expr(ilnqrec)
        pvec = self.expand_newt_expr(ilnprec)
        return (qvec, pvec)
