""" Classes to construct polynomials of the classical Liouville operator """
from itertools import chain, tee
import sympy as sp
import numpy as np
from generic import get_partial_derivs
from generic import lambdify_long as lambdify
from generic import _float, validate_numeric

def xrepl_expr(expr, repl, fpprec='fixed'):
    """ perform substitutions (repl) in liouville power expressions (expr) """
    auxfunc = _float(fpprec, lambda x, y: x.xreplace(y))
    return [[auxfunc(j, repl) for j in i] for i in expr]

def xrepl_repl(iterable, repl, doit=False):
    """ perform substitutions (repl) in a list of symbol-expression tuples """
    if doit:
        retval = {symb: val.xreplace(repl).doit() for symb, val in iterable}
    else:
        retval = {symb: val.xreplace(repl) for symb, val in iterable}
    return retval

def get_stats_repl(tlist, details=False):
    """ performance statistics for subsititution expressions """
    names = ['symbols', 'adds', 'multiplies']
    args = [sp.Symbol, sp.Add, sp.Mul]
    stats = {}
    for name, arg in zip(names, args):
        dct = {}
        numbs_tot = 0
        for tup in tlist:
            numbs = len(tup[1].atoms(arg))
            dct[str(tup[0])] = numbs
            numbs_tot += numbs
        if details:
            stats[name] = {'detailed': dct, 'total': numbs_tot}
        else:
            stats[name] = numbs_tot

    dct = {}
    numbs_tot = 0
    for tup in tlist:
        numbs = tup[1].count_ops()
        dct[str(tup[0])] = numbs
        numbs_tot += numbs
    if details:
        stats['operations'] = {'detailed': dct, 'total': numbs_tot}
    else:
        stats['operations'] = numbs_tot

    return stats


def get_stats_expr(expr, details=False):
    """ performance statistics for liouville power expressions """
    names = ['symbols', 'adds', 'multiplies']
    args = [sp.Symbol, sp.Add, sp.Mul]
    stats = {}
    for name, arg in zip(names, args):
        lst = []
        numbs_tot = 0
        for order, iln in enumerate(expr):
            numbs = [len(ilnf.atoms(arg)) for ilnf in iln]
            lst.append({
                'order': order,
                'numbers': numbs,
                'total': sum(numbs)
            })
            numbs_tot += sum(numbs)
        if details:
            stats[name] = {'total': numbs_tot, 'detailed': lst}
        else:
            stats[name] = numbs_tot

    lst = []
    numbs_tot = 0
    for order, iln in enumerate(expr):
        numbs = [ilnf.count_ops() for ilnf in iln]
        lst.append({
            'order': order,
            'numbers': numbs,
            'total': sum(numbs)
        })
        numbs_tot += sum(numbs)
    if details:
        stats['operations'] = {'total': numbs_tot, 'detailed': lst}
    else:
        stats['operations'] = numbs_tot

    return stats


class MPLP:
    """ Monomials and polynomials of iL for a many-particle system """

    def __init__(self, nterms, hamiltonian, coordinate, momentum, function,
                 scale=1, recursion='taylor', nodes=None, sign=None,
                 parity=None, fpprec='fixed'):
        """ * nterms: number of polynomials, the highest order (nterms-1)
            * hamiltonian: the two-tuple (potential, kinetic) (as expressions)
            * coordinate: an iterable with positions (as symbols)
            * momentum: an iterable with corresponding momenta (as symbols)
            * function: an iterable with system variables that iL is applied
            * scale: a scaling factor for L
            * nodes: a list of Newton interpolation points
            * sign: the sign in the Chebyshev polynomial recursion formula
               > sign = +1 for Chebyshev polynomials with imaginary argument
               > sign = -1 for Chebyshev polynomials with real argument
            * parity: parity of the Chebyshev polynomials: 0: even, 1: odd """
        assert nterms > 0
        assert len(coordinate) > 0 and len(momentum) > 0
        assert len(coordinate) == len(momentum)

        self.nterms = nterms
        self.Q = coordinate
        self.P = momentum
        self.F = function
        self.nodes = nodes
        self.sign = sign
        self.parity = parity
        self.fpprec = fpprec
        (self.V, self.T) = hamiltonian

        common_args = set(self.V.args) & set(self.T.args)
        if (len(common_args) > 0 and
                any(arg in (*self.Q, *self.P) for arg in common_args)):
            hama = hamb = sum(self.V, self.T)
        else:
            hama, hamb = (self.V, self.T)
        self.A = scale*sp.derive_by_array(hama, self.Q)
        self.B = scale*sp.derive_by_array(hamb, self.P)

        getattr(self, recursion)()

    def poisson(self, phi):
        """ evaluate the Poisson bracket applied to variable phi, iL.phi """
        alf = sp.derive_by_array(phi, self.Q)
        blf = sp.derive_by_array(phi, self.P)
        expr = sum(B*a-A*b for A, B, a, b in zip(self.A, self.B, alf, blf))
        return expr.doit()

    def chebyshev(self):
        """ evaluate the Chebyshev polynomials with argument iL """
        assert self.sign in (-1, 1)
        phi0 = list(self.F)
        phi1 = [self.poisson(lf) for lf in self.F]
        if self.parity is None:
            self.Lf = [phi0, phi1]
            for num in range(2, self.nterms):
                lfn = []
                for lf1, lf2 in zip(self.Lf[num-1], self.Lf[num-2]):
                    lfn.append(2*self.poisson(lf1)+self.sign*lf2)
                self.Lf.append(lfn)
        else:
            assert self.parity in (0, 1)
            assert (self.nterms-1)%2 == self.parity
            if self.parity == 0:
                phi2 = [2*self.poisson(lf1)+self.sign*lf0
                        for lf1, lf0 in zip(phi1, phi0)]
                self.Lf = [phi0, phi2]
            else:
                phi3 = [4*self.poisson(self.poisson(lf))+self.sign*3*lf
                        for lf in phi1]
                self.Lf = [phi1, phi3]
            for num in range(2, (self.nterms-1)//2+1):
                lfn = []
                for lf1, lf2 in zip(self.Lf[num-1], self.Lf[num-2]):
                    lfn.append(4*self.poisson(self.poisson(lf1))+
                               self.sign*2*lf1-lf2)
                self.Lf.append(lfn)

    def newton(self):
        """ evaluate the Newton basis polynomials with argument iL """
        assert self.nodes is not None
        self.Lf = [list(self.F)]
        for num in range(1, self.nterms):
            lfn = [self.poisson(lf)-self.nodes[num-1]*lf
                   for lf in self.Lf[num-1]]
            self.Lf.append(lfn)

    def taylor(self):
        """ evaluate the monomials (iL)^n.f for the Liouville operator L """
        self.Lf = [list(self.F)]
        for num in range(1, self.nterms):
            self.Lf.append([self.poisson(lf) for lf in self.Lf[num-1]])

    def get_val_float(self, qval, pval):
        """ evaluates (iL)^n f for given values of q and p, with float type
            casting """
        repl = dict(chain(zip(self.Q, qval), zip(self.P, pval)))
        return xrepl_expr(self.Lf, repl, self.fpprec)


class MPLPQP:
    """ aggregate two MPLP objects with functions Q and P respectively """

    numeric = {'tool': 'subs', 'modules': None}
    def __init__(self, nterms, system, numeric=None, **kwargs):
        if hasattr(system, 'pbc'):
            assert not system.pbc, 'PBCs are not supported'
        if numeric is not None:
            self.numeric = numeric
        elif hasattr(system, 'numeric'):
            self.numeric = system.numeric
        self.ilnqobj = MPLP(nterms, system.hamiltonian, system.q, system.p,
                            function=system.q, **kwargs)
        self.ilnpobj = MPLP(nterms, system.hamiltonian, system.q, system.p,
                            function=system.p, **kwargs)
        self.ilnqcart = self.ilnqobj.Lf
        self.ilnpcart = self.ilnpobj.Lf
        self.set_eval_func((*system.q, *system.p))

    def set_eval_func(self, symbs):
        """ compile functions to evaluate expressions numerically """

        if self.numeric['tool'] == 'subs':
            self.get_val_float = self.get_val_subs
        elif self.numeric['tool'] == 'lambdify':
            backend = self.numeric['modules']
            self.ilnqtf = [lambdify(symbs, i, backend) for i in self.ilnqcart]
            self.ilnptf = [lambdify(symbs, i, backend) for i in self.ilnpcart]
            self.get_val_float = self.get_val_lambdify
        elif self.numeric['tool'] == 'theano':
            from sympy.printing.theanocode import theano_function as tf
            tf_params = {'on_unused_input': 'ignore'}
            self.ilnqtf = [tf(symbs, i, **tf_params) for i in self.ilnqcart]
            self.ilnptf = [tf(symbs, i, **tf_params) for i in self.ilnpcart]
            self.get_val_float = self.get_val_theano
        else:
            raise ValueError('not supported numeric: '+self.numeric['tool'])

    def get_val_subs(self, qval, pval):
        """ evaluates (iL)^n q and (iL)^n p for given values of q and p """
        qvec = self.ilnqobj.get_val_float(qval, pval)
        pvec = self.ilnpobj.get_val_float(qval, pval)
        return (qvec, pvec)

    def get_val_lambdify(self, qval, pval):
        """ evaluates (iL)^n q and (iL)^n p for given values of q and p """
        replvec = (*qval, *pval)
        ilnqtv = [tf(replvec) for tf in self.ilnqtf]
        ilnptv = [tf(replvec) for tf in self.ilnptf]
        return (ilnqtv, ilnptv)

    def get_val_theano(self, qval, pval):
        """ evaluates (iL)^n q and (iL)^n p for given values of q and p """
        ilnqtv = [tf(*qval, *pval) for tf in self.ilnqtf]
        ilnptv = [tf(*qval, *pval) for tf in self.ilnptf]
        return (ilnqtv, ilnptv)


class MPLPAQ(MPLP):
    """ assume H(Q, P) = V(Q) + T(P) and functions Q and P. The iL^n f
        expressions include the derivatives of V(Q) w.r.t. the Cartesian
        coordinates Q, i.e. A1(Q), A2(Q), ... """

    numeric = {'tool': 'theano', 'modules': None}
    def __init__(self, maxorder, system, numeric=None, fpprec='fixed',
                 **kwargs):
        """ modules supported by tool 'lambdify': math, mpmath, numpy,
            numexpr, tensorflow, sympy """

        self.fpprec = fpprec
        # general symbolic potential and kinetic energies
        self.Vs = sp.Function('V', real=True)(*system.q)
        self.Ts = sp.Function('T', real=True)(*system.p)
        # specific symbolic potential and kinetic energies
        self.Ve, self.Te = system.hamiltonian
        if hasattr(system, 'pbc'):
            assert not system.pbc, 'PBCs are not supported'
        if numeric is not None:
            self.numeric = numeric
        elif hasattr(system, 'numeric'):
            self.numeric = system.numeric
        validate_numeric(self.fpprec, self.numeric)

        super().__init__(maxorder, (self.Vs, self.Ts), system.q, system.p,
                         system.q, **kwargs)
        A = [self.V]
        B = [self.T]
        for j in range(1, maxorder):
            # tensor of j-th order partial derivatives
            A.append(sp.Array(set(sp.derive_by_array(A[j-1], self.Q))))
            B.append(sp.Array(set(sp.derive_by_array(B[j-1], self.P))))

        self.repla = []
        self.replb = []
        for symb, ders, repl in [('A', A, self.repla), ('B', B, self.replb)]:
            for der in (i for j in ders[1:] for i in j):
                dvars = der.variables
                dstr = symb+str(len(dvars))+'_'+''.join([str(v) for v in dvars])
                repl.append((dstr, der))
        replar = reversed(self.repla)
        replbr = reversed(self.replb)
        replas = ((i.replace('q', 'p').replace('A', 'B'), j) for i, j in replar)
        replbs = ((i.replace('p', 'q').replace('B', 'A'), j) for i, j in replbr)
        replar = reversed(self.repla)
        replbr = reversed(self.replb)
        replqg = ((j, sp.Symbol(i, real=True)) for t in zip(replar, replbr) for i, j in t)
        replpg = ((j, sp.Symbol(i, real=True)) for t in zip(replas, replbs) for i, j in t)

        # symbolic expressions for (iL)^n Q and (iL)^n P
        replq = dict(replqg)
        replp = dict(chain(replpg, zip(self.Q, self.P)))
        ilnq = ((j.xreplace(replq) for j in i) for i in self.Lf)
        ilnp = (((-1)**k*j.xreplace(replp) for j in i) for k, i in enumerate(self.Lf))

        # actual expressions for potential and kinetic energies
        replve = {self.Vs: self.Ve}
        replte = {self.Ts: self.Te}
        replae = [(sp.Symbol(i, real=True), j.xreplace(replve).doit()) for i, j in self.repla]
        replbe = {sp.Symbol(i, real=True): j.xreplace(replte).doit() for i, j in self.replb}
        replbe.update({i: j for i, j in replae if j == 0}) # add vanishing derivatives

        if self.numeric['tool'] == 'subs':
            self.repl = replae
            self.ilnqexpr = [[j.xreplace(replbe) for j in i] for i in ilnq]
            self.ilnpexpr = [[j.xreplace(replbe) for j in i] for i in ilnp]
            self.get_val_float = self.get_val_float_subs
        else:
            replaf = (j for i, j in replae if j != 0) # omit vanishing derivatives
            syflat = tuple(i for i, j in replae if j != 0) + (*self.Q, *self.P)
            ilnqbe = ([j.xreplace(replbe) for j in i] for i in ilnq)
            ilnpbe = ([j.xreplace(replbe) for j in i] for i in ilnp)
            self.set_func(replaf, syflat, ilnqbe, ilnpbe)

    def set_func(self, replaf, syflat, ilnqbe, ilnpbe):
        """ compile functions to evaluate expressions numerically """

        if self.numeric['tool'] == 'lambdify':
            backend = self.numeric['modules']
            self.replat = lambdify(self.Q, replaf, backend)
            self.ilnqtf = [lambdify(syflat, i, backend) for i in ilnqbe]
            self.ilnptf = [lambdify(syflat, i, backend) for i in ilnpbe]
            self.get_val_float = self.get_val_lambdify
        elif self.numeric['tool'] == 'theano':
            from sympy.printing.theanocode import theano_function as tf
            self.replat = tf((*self.Q,), replaf)
            self.ilnqtf = [tf(syflat, i, on_unused_input='ignore') for i in ilnqbe]
            self.ilnptf = [tf(syflat, i, on_unused_input='ignore') for i in ilnpbe]
            self.get_val_float = self.get_val_theano
        else:
            raise ValueError('not supported numeric: '+self.numeric['tool'])

    def get_val_float_subs(self, qval, pval):
        """ evaluates (iL)^n q and (iL)^n p for given values of q and p """
        replqv = dict(zip(self.Q, qval))
        repl_full = dict(zip(self.P, pval))
        repl_full.update(xrepl_repl(self.repl, replqv))
        ilnpabv = xrepl_expr(self.ilnpexpr, repl_full, self.fpprec)
        repl_full.update(replqv)
        ilnqabv = xrepl_expr(self.ilnqexpr, repl_full, self.fpprec)
        return (ilnqabv, ilnpabv)

    def get_val_theano(self, qval, pval):
        """ evaluates (iL)^n q and (iL)^n p for given values of q and p """
        replvec = self.replat(*qval) + [*qval, *pval]
        ilnqtv = [tf(*replvec) for tf in self.ilnqtf]
        ilnptv = [tf(*replvec) for tf in self.ilnptf]
        return (ilnqtv, ilnptv)

    def get_val_lambdify(self, qval, pval):
        """ evaluates (iL)^n q and (iL)^n p for given values of q and p """
        replvec = self.replat(qval) + [*qval, *pval]
        ilnqtv = [tf(replvec) for tf in self.ilnqtf]
        ilnptv = [tf(replvec) for tf in self.ilnptf]
        return (ilnqtv, ilnptv)


class MPLPVQ:
    """ assume H(Q, P) = V(Q) + T(P) and functions Q and P and partition the
        total potential into pair potentials. The iL^n f expressions include
        the derivatives of pair potentials V_i(Q) w.r.t. the Cartesian
        coordinates Q """

    numeric = {'tool': 'theano', 'modules': None}
    def __init__(self, maxorder, system, numeric=None, fpprec='fixed',
                 **kwargs):
        """ modules supported by tool 'lambdify': math, mpmath, numpy,
            numexpr, tensorflow, sympy """

        self.fpprec = fpprec
        self.Q = system.q
        self.P = system.p
        self.ff = system.ff
        self.maxorder = maxorder-1
        if hasattr(system, 'pbc'):
            assert not system.pbc, 'PBCs are not supported'
        if numeric is not None:
            self.numeric = numeric
        elif hasattr(system, 'numeric'):
            self.numeric = system.numeric
        validate_numeric(self.fpprec, self.numeric)

        Vs = self.ff.get_symbolic_potential('cartesian')
        Ts = sp.Function('T', real=True)(*self.P)
        ilnq = MPLP(maxorder, (Vs, Ts), self.Q, self.P, self.Q, **kwargs).Lf
        ilnp = MPLP(maxorder, (Vs, Ts), self.Q, self.P, self.P, **kwargs).Lf

        replb = zip(*tee(get_partial_derivs(Ts, self.P, self.maxorder), 2))
        replbe = xrepl_repl(replb, {Ts: system.kinetic_energy}, doit=True)
        ilnqab = ((j.xreplace(replbe) for j in i) for i in ilnq)
        ilnpab = ((j.xreplace(replbe) for j in i) for i in ilnp)
        symbder = self.ff.get_symbolic_derivs(self.maxorder, 'cartesian')
        implder = self.ff.get_implicit_derivs(self.maxorder, 'cartesian')
        repla = dict(zip(implder, symbder))
        ilnqbe = ((j.xreplace(repla) for j in i) for i in ilnqab)
        ilnpbe = ((j.xreplace(repla) for j in i) for i in ilnpab)

        if self.numeric['tool'] == 'subs':
            # use sympy.xreplace() to evaluate expressions numerically
            self.ilnqexpr = [list(i) for i in ilnqbe]
            self.ilnpexpr = [list(i) for i in ilnpbe]
            symbder = self.ff.get_symbolic_derivs(self.maxorder, 'cartesian')
            explder = self.ff.get_explicit_derivs(self.maxorder, 'cartesian')
            self.repl = list(zip(symbder, explder))
            self.get_val_float = self.get_val_float_subs
        elif self.numeric['tool'] == 'theano':
            # use theano to evaluate expressions numerically
            self.set_theano(ilnqbe, ilnpbe)
            self.get_val_float = self.get_val_float_theano
        else:
            raise ValueError('not supported numeric: '+self.numeric['tool'])

    def set_theano(self, ilnqbe, ilnpbe):
        """ compile theano functions for numerical expression evaluation """
        from sympy.printing.theanocode import theano_function as tf
        replaf = list(self.ff.get_explicit_derivs(self.maxorder, 'cartesian'))
        self.replat = tf((*self.Q,), replaf)
        syflat = list(self.ff.get_symbolic_derivs(self.maxorder, 'cartesian'))
        syflat += [*self.Q, *self.P]
        self.ilnqtf = [tf(syflat, i, on_unused_input='ignore') for i in ilnqbe]
        self.ilnptf = [tf(syflat, i, on_unused_input='ignore') for i in ilnpbe]

    def get_val_float_subs(self, qval, pval):
        """ evaluates (iL)^n q and (iL)^n p for given values of q and p """
        replqv = dict(zip(self.Q, qval))
        repl_full = dict(zip(self.P, pval))
        repl_full.update(xrepl_repl(self.repl, replqv))
        ilnpabv = xrepl_expr(self.ilnpexpr, repl_full, self.fpprec)
        repl_full.update(replqv)
        ilnqabv = xrepl_expr(self.ilnqexpr, repl_full, self.fpprec)
        return (ilnqabv, ilnpabv)

    def get_val_float_theano(self, qval, pval):
        """ evaluates (iL)^n q and (iL)^n p for given values of q and p """
        replvec = self.replat(*qval) + [*qval, *pval]
        ilnqtv = [tf(*replvec) for tf in self.ilnqtf]
        ilnptv = [tf(*replvec) for tf in self.ilnptf]
        return (ilnqtv, ilnptv)


class MPLPDR:
    """ assume H(Q, P) = V(Q) + T(P) and functions Q and P, partition the
        total potential into pair potentials as function of distance. The
        iL^n f expressions include the derivatives of the distances R(Q)
        w.r.t. the Cartesian coordinates Q (variant='dr') or the distances
        R(Q) (variant='r') """

    numeric = {'tool': 'theano', 'modules': None}
    def __init__(self, maxorder, system, numeric=None, variant='dr',
                 fpprec='fixed', **kwargs):
        """ modules supported by tool 'lambdify': math, mpmath, numpy,
            numexpr, tensorflow, sympy """

        self.fpprec = fpprec
        self.Q = system.q
        self.P = system.p
        self.ff = system.ff
        self.maxorder = maxorder-1
        if hasattr(system, 'pbc'):
            assert not system.pbc, 'PBCs are not supported'
        if numeric is not None:
            self.numeric = numeric
        elif hasattr(system, 'numeric'):
            self.numeric = system.numeric
        validate_numeric(self.fpprec, self.numeric)

        Vs = sum(self.ff.get_explicit_potentials('internal'))
        Ts = system.kinetic_energy
        ilnqbv = MPLP(maxorder, (Vs, Ts), self.Q, self.P, self.Q, **kwargs).Lf
        ilnpbv = MPLP(maxorder, (Vs, Ts), self.Q, self.P, self.P, **kwargs).Lf

        # substitute distance derivative functions with symbols
        derig = [f.get_dist_derivs(self.maxorder, mixed=False) for f in self.ff]
        symders = self.ff.get_symbolic_derivs(self.maxorder)
        symders1, symders2 = tee(symders, 2)
        repld = dict(zip(chain(*derig), symders1))

        # substitute distance functions with r symbols
        distl, distr = tee((f.r for f in self.ff))
        symdist = [sp.Symbol(str(dist.func), real=True) for dist in distr]
        repld.update(dict(zip(distl, symdist)))
        ilnqbe = ((j.xreplace(repld) for j in i) for i in ilnqbv)
        ilnpbe = ((j.xreplace(repld) for j in i) for i in ilnpbv)

        # explicit expressions of distance derivatives in r, Q symbols
        repldr = dict(zip((f.r for f in self.ff), symdist))
        derig = [f.get_dist_derivs(self.maxorder) for f in self.ff]
        expders = (der.xreplace(repldr) for der in chain(*derig))

        if self.numeric['tool'] == 'subs':
            repldis = list(zip(symdist, (f.r_cart for f in self.ff)))
            if variant == 'dr':
                self.repldis = repldis
                self.replder = list(zip(symders2, expders))
                self.repl = self.repldis + self.replder
                self.ilnqexpr = [list(i) for i in ilnqbe]
                self.ilnpexpr = [list(i) for i in ilnpbe]
                self.get_val_float = self.get_val_subs_dr
            else:
                self.repl = repldis
                replder = dict(zip(symders2, expders))
                self.ilnqexpr = xrepl_expr(ilnqbe, replder, 'multi')
                self.ilnpexpr = xrepl_expr(ilnpbe, replder, 'multi')
                self.get_val_float = self.get_val_subs_r
        elif self.numeric['tool'] == 'theano':
            if variant == 'dr':
                self.set_theano_dr(symdist, symders2, expders, ilnqbe, ilnpbe)
                # handle the unlikely case of only one pair
                if len(symdist) == 1:
                    self.get_val_float = self.get_val_theano_dr_r1d
                else:
                    self.get_val_float = self.get_val_theano_dr
            else:
                self.set_theano_r(symdist, symders2, expders, ilnqbe, ilnpbe)
                if len(symdist) == 1:
                    self.get_val_float = self.get_val_theano_r_r1d
                else:
                    self.get_val_float = self.get_val_theano_r
        else:
            raise ValueError('not supported numeric: '+self.numeric['tool'])

    def set_theano_dr(self, symdist, symders, expders, ilnqbe, ilnpbe):
        """ evaluate r', r'', ... and then substitute in iL^n expr;
            this is suitable for applying PBC and r^n cutt-offs """
        from sympy.printing.theanocode import theano_function as tf
        self.repldis = tf((*self.Q,), (f.r_cart for f in self.ff))
        self.replder = tf((*self.Q, *symdist), expders)
        symbs = (*self.P, *self.Q, *symdist, *symders)
        self.ilnqtf = [tf(symbs, i, on_unused_input='ignore') for i in ilnqbe]
        self.ilnptf = [tf(symbs, i, on_unused_input='ignore') for i in ilnpbe]

    def set_theano_r(self, symdist, symders, expders, ilnqbe, ilnpbe):
        """ resolve r', r'', ... in iL^n expr to r and q """
        from sympy.printing.theanocode import theano_function as tf
        self.repldis = tf((*self.Q,), (f.r_cart for f in self.ff))
        replder = dict(zip(symders, expders))
        ilnqr = ((j.xreplace(replder) for j in i) for i in ilnqbe)
        ilnpr = ((j.xreplace(replder) for j in i) for i in ilnpbe)
        symbs = (*self.P, *self.Q, *symdist)
        self.ilnqtf = [tf(symbs, i, on_unused_input='ignore') for i in ilnqr]
        self.ilnptf = [tf(symbs, i, on_unused_input='ignore') for i in ilnpr]

    def get_val_subs_dr(self, qval, pval):
        """ evaluates (iL)^n Q and (iL)^n P for given values of Q and P """
        replqv = dict(zip(self.Q, qval))
        replpv = dict(zip(self.P, pval))
        repldisv = xrepl_repl(self.repldis, replqv)
        repldisv.update(replqv)
        replderv = xrepl_repl(self.replder, repldisv)
        replderv.update(repldisv)
        replderv.update(replpv)
        ilnqabv = xrepl_expr(self.ilnqexpr, replderv, self.fpprec)
        ilnpabv = xrepl_expr(self.ilnpexpr, replderv, self.fpprec)
        return (ilnqabv, ilnpabv)

    def get_val_subs_r(self, qval, pval):
        """ evaluates (iL)^n Q and (iL)^n P for given values of Q and P """
        replqv = dict(zip(self.Q, qval))
        replpv = dict(zip(self.P, pval))
        repl_all = xrepl_repl(self.repl, replqv)
        repl_all.update(replpv)
        repl_all.update(replqv)
        ilnqabv = xrepl_expr(self.ilnqexpr, repl_all, self.fpprec)
        ilnpabv = xrepl_expr(self.ilnpexpr, repl_all, self.fpprec)
        return (ilnqabv, ilnpabv)

    def get_val_theano_dr_r1d(self, qval, pval):
        """ evaluates (iL)^n q and (iL)^n p for given values of q and p """
        replvec1 = self.repldis(*qval)
        replvec2 = self.replder(*qval, replvec1)
        ilnqtv = [tf(*pval, *qval, replvec1, *replvec2) for tf in self.ilnqtf]
        ilnptv = [tf(*pval, *qval, replvec1, *replvec2) for tf in self.ilnptf]
        return (ilnqtv, ilnptv)

    def get_val_theano_dr(self, qval, pval):
        """ evaluates (iL)^n q and (iL)^n p for given values of q and p """
        replvec1 = self.repldis(*qval)
        replvec2 = self.replder(*qval, *replvec1)
        ilnqtv = [tf(*pval, *qval, *replvec1, *replvec2) for tf in self.ilnqtf]
        ilnptv = [tf(*pval, *qval, *replvec1, *replvec2) for tf in self.ilnptf]
        return (ilnqtv, ilnptv)

    def get_val_theano_r_r1d(self, qval, pval):
        """ evaluates (iL)^n q and (iL)^n p for given values of q and p """
        replvec = self.repldis(*qval)
        ilnqtv = [tf(*pval, *qval, replvec) for tf in self.ilnqtf]
        ilnptv = [tf(*pval, *qval, replvec) for tf in self.ilnptf]
        return (ilnqtv, ilnptv)

    def get_val_theano_r(self, qval, pval):
        """ evaluates (iL)^n q and (iL)^n p for given values of q and p """
        replvec = self.repldis(*qval)
        ilnqtv = [tf(*pval, *qval, *replvec) for tf in self.ilnqtf]
        ilnptv = [tf(*pval, *qval, *replvec) for tf in self.ilnptf]
        return (ilnqtv, ilnptv)


class MPLPDV:
    """ assume H(Q, P) = V(Q) + T(P) and functions Q and P, partition the
        total potential into pair potentials as function of distance; The
        iL^n f expressions include the derivatives of V_i(Rx, Ry, Rz) w.r.t.
        the cartesian components of the distances (variant='dv') or cartesian
        components of the distances (variant='r'); PBC are supported in this
        implementation; V(r) based cut-offs can be implemented. """

    numeric = {'tool': 'theano', 'modules': None}
    def __init__(self, maxorder, system, numeric=None, variant='dv',
                 fpprec='fixed', **kwargs):
        """ modules supported by tool 'lambdify': math, mpmath, numpy,
            numexpr, tensorflow, sympy """

        from forcefield import PairFF

        self.fpprec = fpprec
        self.Q = system.q
        self.P = system.p
        self.ff = system.ff
        self.maxorder = maxorder-1
        self.system = system
        if numeric is not None:
            self.numeric = numeric
        elif hasattr(system, 'numeric'):
            self.numeric = system.numeric
        self.variant = variant
        validate_numeric(self.fpprec, self.numeric)

        assert (isinstance(self.ff, list) and
                all(isinstance(f, PairFF) for f in self.ff))
        Vs = self.ff.get_symbolic_potential('cartesian')
        Ts = system.kinetic_energy
        ilnqbv = MPLP(maxorder, (Vs, Ts), self.Q, self.P, self.Q, **kwargs).Lf
        ilnpbv = MPLP(maxorder, (Vs, Ts), self.Q, self.P, self.P, **kwargs).Lf
        explder = (f.get_full_derivs(self.maxorder) for f in self.ff)
        implder = (f.get_cart_derivs(self.maxorder) for f in self.ff)
        dsymbs = list(self.system.get_distance_symbols())
        if self.numeric['tool'] == 'theano':
            self.set_theano(explder, implder, dsymbs, ilnqbv, ilnpbv)
            self.get_val_float = (self.get_val_theano_r if self.variant == 'r'
                                  else self.get_val_theano_dv)
        elif self.numeric['tool'] == 'lambdify':
            self.set_lambdify(explder, implder, dsymbs, ilnqbv, ilnpbv)
            self.get_val_float = (self.get_val_lambdify_r if self.variant == 'r'
                                  else self.get_val_lambdify_dv)
        elif self.numeric['tool'] == 'subs':
            self.set_subs(explder, implder, dsymbs, ilnqbv, ilnpbv)
            self.get_val_float = (self.get_val_subs_r if self.variant == 'r'
                                  else self.get_val_subs_dv)

    def set_subs(self, explder, implder, dsymbs, ilnqbv, ilnpbv):
        """ construct final iL^n expressions for evaluation with xreplace """
        if self.variant == 'dv':
            symbder = self.ff.get_symbolic_derivs(self.maxorder)
            symbder1, symbder2 = tee(symbder, 2)
            repldi = dict(zip(chain(*implder), symbder1))
            self.repl = list(zip(symbder2, chain(*explder)))
        elif self.variant == 'r':
            repldi = dict(zip(chain(*implder), chain(*explder)))
        self.dists = dsymbs
        self.ilnqexpr = [[j.xreplace(repldi) for j in i] for i in ilnqbv]
        self.ilnpexpr = [[j.xreplace(repldi) for j in i] for i in ilnpbv]

    def set_theano(self, explder, implder, dsymbs, ilnqbv, ilnpbv):
        """ compile theano function for numerical evaluation of iL^n{q, p} """
        from sympy.printing.theanocode import theano_function as tf
        if self.variant == 'dv':
            symbder = self.ff.get_symbolic_derivs(self.maxorder)
            symbder1, symbder2 = tee(symbder, 2)
            repldi = dict(zip(chain(*implder), symbder1))
            self.replder = tf(dsymbs, chain(*explder))
            symbs = (*self.P, *self.Q, *symbder2)
        elif self.variant == 'r':
            repldi = dict(zip(chain(*implder), chain(*explder)))
            symbs = (*self.P, *self.Q, *dsymbs)

        ilnqbe = ((j.xreplace(repldi) for j in i) for i in ilnqbv)
        ilnpbe = ((j.xreplace(repldi) for j in i) for i in ilnpbv)
        self.ilnqtf = [tf(symbs, i, on_unused_input='ignore') for i in ilnqbe]
        self.ilnptf = [tf(symbs, i, on_unused_input='ignore') for i in ilnpbe]

    def set_lambdify(self, explder, implder, dsymbs, ilnqbv, ilnpbv):
        """ compile lambda function for numerical evaluation of iL^n{q, p} """
        backend = self.numeric['modules']
        if self.variant == 'dv':
            symbder = self.ff.get_symbolic_derivs(self.maxorder)
            symbder1, symbder2 = tee(symbder, 2)
            repldi = dict(zip(chain(*implder), symbder1))
            self.replder = lambdify(dsymbs, chain(*explder), backend)
            symbs = (*self.P, *self.Q, *symbder2)
        elif self.variant == 'r':
            repldi = dict(zip(chain(*implder), chain(*explder)))
            symbs = (*self.P, *self.Q, *dsymbs)

        ilnqbe = ((j.xreplace(repldi) for j in i) for i in ilnqbv)
        ilnpbe = ((j.xreplace(repldi) for j in i) for i in ilnpbv)
        self.ilnqtf = [lambdify(symbs, i, backend) for i in ilnqbe]
        self.ilnptf = [lambdify(symbs, i, backend) for i in ilnpbe]

    def get_val_theano_dv(self, qval, pval):
        """ evaluate (iL)^n q and (iL)^n p for given values of q and p """
        distv = self.system.get_distance_vectors(qval).flatten()
        derivv = self.replder(*distv)
        ilnqtv = [tf(*pval, *qval, *derivv) for tf in self.ilnqtf]
        ilnptv = [tf(*pval, *qval, *derivv) for tf in self.ilnptf]
        return (ilnqtv, ilnptv)

    def get_val_theano_r(self, qval, pval):
        """ evaluate (iL)^n q and (iL)^n p for given values of q and p """
        distv = self.system.get_distance_vectors(qval).flatten()
        ilnqtv = [tf(*pval, *qval, *distv) for tf in self.ilnqtf]
        ilnptv = [tf(*pval, *qval, *distv) for tf in self.ilnptf]
        return (ilnqtv, ilnptv)

    def get_val_lambdify_dv(self, qval, pval):
        """ evaluate (iL)^n q and (iL)^n p for given values of q and p """
        distv = self.system.get_distance_vectors(qval).flatten()
        derivv = self.replder(distv)
        ilnqtv = [tf((*pval, *qval, *derivv)) for tf in self.ilnqtf]
        ilnptv = [tf((*pval, *qval, *derivv)) for tf in self.ilnptf]
        return (ilnqtv, ilnptv)

    def get_val_lambdify_r(self, qval, pval):
        """ evaluate (iL)^n q and (iL)^n p for given values of q and p """
        distv = self.system.get_distance_vectors(qval).flatten()
        ilnqtv = [tf((*pval, *qval, *distv)) for tf in self.ilnqtf]
        ilnptv = [tf((*pval, *qval, *distv)) for tf in self.ilnptf]
        return (ilnqtv, ilnptv)

    def get_val_subs_dv(self, qval, pval):
        """ evaluate (iL)^n q and (iL)^n p for given values of q and p """
        distv = self.system.get_distance_vectors(qval).flatten()
        replv = xrepl_repl(self.repl, dict(zip(self.dists, distv)))
        replv.update(dict(zip(self.P, pval)))
        replv.update(dict(zip(self.Q, qval)))
        ilnqabv = xrepl_expr(self.ilnqexpr, replv, self.fpprec)
        ilnpabv = xrepl_expr(self.ilnpexpr, replv, self.fpprec)
        return (ilnqabv, ilnpabv)

    def get_val_subs_r(self, qval, pval):
        """ evaluate (iL)^n q and (iL)^n p for given values of q and p """
        distv = self.system.get_distance_vectors(qval).flatten()
        replv = dict(zip(self.dists, distv))
        replv.update(dict(zip(self.P, pval)))
        replv.update(dict(zip(self.Q, qval)))
        ilnqabv = xrepl_expr(self.ilnqexpr, replv, self.fpprec)
        ilnpabv = xrepl_expr(self.ilnpexpr, replv, self.fpprec)
        return (ilnqabv, ilnpabv)


class MPLPNR:
    """ assume H(Q, P) = V(Q) + T(P) and functions Q and P, partition the
        total potential into pair potentials as function of distance; The
        iL^n f expressions include the derivatives of V_i(Rx, Ry, Rz) w.r.t.
        the cartesian components of the distances (variant='dv') or cartesian
        components of the distances (variant='r'); PBC are fully supported in
        this implementation; the identity (iL)^n q == (iL)^{n-1} p is used in
        combination with non-recursive Chebyshev polynomial expansion or
        Newtonian polynomial interpolation; V(r) based cut-offs can be (but
        currently not) implemented. """

    numeric = {'tool': 'theano', 'modules': None}
    def __init__(self, nterms, system, numeric=None, recursion=None,
                 variant='dv', fpprec='fixed', **kwargs):
        """ modules supported by tool 'lambdify': math, mpmath, numpy,
            numexpr, tensorflow, sympy """

        from forcefield import PairFF
        from combinatorics import cnk_chebyshev_T2, cnk_newton_1

        self.fpprec = fpprec
        self.maxorder = nterms-1
        assert recursion in ['chebyshev', 'newton']
        self.scale = kwargs.get('scale', 1)
#        fpprec = kwargs.get('fpprec', 'fixed')
        prec = kwargs.get('prec', None)
        if numeric is not None:
            self.numeric = numeric
        elif hasattr(system, 'numeric'):
            self.numeric = system.numeric
        validate_numeric(self.fpprec, self.numeric)

        if recursion == 'chebyshev':
            sign = kwargs.get('sign')
            assert sign is not None
            signed = True if sign == -1 else False
            vfunc = np.vectorize(cnk_chebyshev_T2)
            self.cnk = np.fromfunction(vfunc, (nterms, nterms), dtype=int,
                                       signed=signed)
            self._nr_poly = self._nr_poly_cheb
        else:
            dtype = kwargs.get('dtype', float)
            nodes = kwargs.get('nodes')
            self.cnk = cnk_newton_1(nodes, self.maxorder, dtype=dtype,
                                    fpprec=fpprec, prec=prec)
            self._nr_poly = self._nr_poly_newt

        self.system = system
        self.Q = system.q
        self.P = system.p
        self.ff = system.ff
        self.variant = variant

        assert (isinstance(self.ff, list) and
                all(isinstance(f, PairFF) for f in self.ff))
        Vs = self.ff.get_symbolic_potential('cartesian')
        Ts = system.kinetic_energy
        ilnpbv = MPLP(nterms, (Vs, Ts), self.Q, self.P, self.P,
                      recursion='taylor', **kwargs).Lf
        explder = (f.get_full_derivs(self.maxorder) for f in self.ff)
        implder = (f.get_cart_derivs(self.maxorder) for f in self.ff)
        dsymbs = list(self.system.get_distance_symbols())
        if self.numeric['tool'] in ['theano', 'lambdify']:
            self.set_func(explder, implder, dsymbs, ilnpbv)
        elif self.numeric['tool'] == 'subs':
            self.set_subs(explder, implder, dsymbs, ilnpbv)

    def set_subs(self, explder, implder, dsymbs, ilnpbv):
        """ construct final iL^n expressions for evaluation with xreplace """
        if self.variant == 'dv':
            symbder = self.ff.get_symbolic_derivs(self.maxorder)
            symbder1, symbder2 = tee(symbder, 2)
            repldi = dict(zip(chain(*implder), symbder1))
            self.repl = list(zip(symbder2, chain(*explder)))
            self.get_val_float = self.get_val_subs_dv
        elif self.variant == 'r':
            repldi = dict(zip(chain(*implder), chain(*explder)))
            self.get_val_float = self.get_val_subs_r
        self.dists = dsymbs
        self.ilnpexpr = [[j.xreplace(repldi) for j in i] for i in ilnpbv]
        self.ilnqexpr = [[]]

    def set_func(self, explder, implder, dsymbs, ilnpbv):
        """ compile a function for numerical evaluation of iL^n{q, p} """
        from sympy.printing.theanocode import theano_function as tf
        if self.variant == 'dv':
            symbder = self.ff.get_symbolic_derivs(self.maxorder)
            symbder1, symbder2 = tee(symbder, 2)
            repldi = dict(zip(chain(*implder), symbder1))
            if self.numeric['tool'] == 'theano':
                self.replder = tf(dsymbs, chain(*explder))
                self.get_val_float = self.get_val_theano_dv
            else:
                self.replder = lambdify(dsymbs, chain(*explder),
                                        self.numeric['modules'])
                self.get_val_float = self.get_val_lambdify_dv
            symbs = (*self.P, *symbder2)
        elif self.variant == 'r':
            repldi = dict(zip(chain(*implder), chain(*explder)))
            symbs = (*self.P, *dsymbs)
            self.get_val_float = (self.get_val_theano_r
                                  if self.numeric['tool'] == 'theano' else
                                  self.get_val_lambdify_r)
        ilnpbe = ((j.xreplace(repldi) for j in i) for i in ilnpbv)
        if self.numeric['tool'] == 'theano':
            self.ilnptf = [tf(symbs, i, on_unused_input='ignore')
                           for i in ilnpbe]
        else:
            self.ilnptf = [lambdify(symbs, i, self.numeric['modules'])
                           for i in ilnpbe]

    def expand_newt_expr(self, iln):
        newt = []
        for n in range(len(iln)):
            iln_rev = reversed(iln[:n+1])
            terms = [self.cnk[n, k]*t for k, t in enumerate(iln_rev)]
            newt.append(np.sum(terms, axis=0))
        return np.array(newt)

    def _nr_poly_cheb(self, qval, ilnprec):
        ilnqrec = np.array([qval] + [e*self.scale for e in ilnprec[:-1]])
        qvec = np.tensordot(self.cnk, ilnqrec, axes=1)
        pvec = np.tensordot(self.cnk, ilnprec, axes=1)
        return (qvec, pvec)

    def _nr_poly_newt(self, qval, ilnprec):
        ilnqrec = np.array([qval] + [e*self.scale for e in ilnprec[:-1]])
        qvec = self.expand_newt_expr(ilnqrec)
        pvec = self.expand_newt_expr(ilnprec)
        return (qvec, pvec)

    def _theano_all(self, qval, pval, distv):
        ilnprec = np.array([tf(*pval, *distv) for tf in self.ilnptf])
        return self._nr_poly(qval, ilnprec)

    def get_val_theano_dv(self, qval, pval):
        """ evaluate (iL)^n q and (iL)^n p for given values of q and p """
        distv = self.system.get_distance_vectors(qval).flatten()
        return self._theano_all(qval, pval, self.replder(*distv))

    def get_val_theano_r(self, qval, pval):
        """ evaluate (iL)^n q and (iL)^n p for given values of q and p """
        distv = self.system.get_distance_vectors(qval).flatten()
        return self._theano_all(qval, pval, distv)

    def _lambdify_all(self, qval, auxvec):
        ilnprec = np.array([tf(auxvec) for tf in self.ilnptf])
        return self._nr_poly(qval, ilnprec)

    def get_val_lambdify_dv(self, qval, pval):
        """ evaluate (iL)^n q and (iL)^n p for given values of q and p """
        distv = self.system.get_distance_vectors(qval).flatten()
        return self._lambdify_all(qval, (*pval, *self.replder(distv)))

    def get_val_lambdify_r(self, qval, pval):
        """ evaluate (iL)^n q and (iL)^n p for given values of q and p """
        distv = self.system.get_distance_vectors(qval).flatten()
        return self._lambdify_all(qval, (*pval, *distv))

    def _subs_all(self, replv, qval):
        ilnprec = np.array(xrepl_expr(self.ilnpexpr, replv, self.fpprec))
        return self._nr_poly(qval, ilnprec)

    def get_val_subs_dv(self, qval, pval):
        """ evaluate (iL)^n q and (iL)^n p for given values of q and p """
        distv = self.system.get_distance_vectors(qval).flatten()
        replv = xrepl_repl(self.repl, dict(zip(self.dists, distv)))
        replv.update(dict(zip(self.P, pval)))
        return self._subs_all(replv, qval)

    def get_val_subs_r(self, qval, pval):
        """ evaluate (iL)^n q and (iL)^n p for given values of q and p """
        distv = self.system.get_distance_vectors(qval).flatten()
        replv = dict(zip(self.dists, distv))
        replv.update(dict(zip(self.P, pval)))
        return self._subs_all(replv, qval)
