""" Propagators for classical particle dynamics """

import mpmath as mp
import numpy as np
import sympy as sp
from collections.abc import Iterable
from liouville import SPLPQP

class Propagator:
    """ Base class for all propagators for classical particle dynamics """
    name = 'generic'
    def __init__(self, syst, tstart, tend, tstep, substep=None, pbc=False,
                 tqdm=False):
        self.q = syst.q0
        self.p = syst.p0
        self.q0 = syst.q0
        self.p0 = syst.p0
        self.syst = syst
        self.tstart = tstart
        self.tend = tend
        self.tstep = tstep
        self.substep = substep
        self.en0 = self.syst.energy(self.q0, self.p0)
        assert self.tstart < self.tend
        nsteps = int(np.floor((self.tend-self.tstart)/self.tstep))+1
        # self.time = np.linspace(self.tstart, self.tend, nsteps)
        self.time = [tstart+(tend-tstart)/(nsteps-1)*s for s in range(nsteps)]
        self.sq = [self.q]
        self.sp = [self.p]
        self.pbc = pbc
        self.step = self.step_pbc if self.pbc else self.step_nopbc
        self.tqdm = tqdm

        if self.substep is not None:
            assert self.substep < self.tstep
            assert np.isclose(self.tstep % self.substep, 0)
            nsubsteps = int(np.floor(self.tstep/self.substep))
            self.subtime = np.linspace(self.substep, self.tstep, nsubsteps)

    def propagate(self):
        """ perform propagation of coordinate and momentum in time """
        from tqdm import tqdm
        (q, p) = (self.q, self.p)
        if self.substep is not None:
            its = tqdm(self.time[:-1]) if self.tqdm else self.time[:-1]
            for time in its:
                (qt, pt) = self.step(time, q, p)
                self.sq.extend(qt)
                self.sp.extend(pt)
                q = qt[-1]
                p = pt[-1]
            self.time = [t+st for t in self.time[:-1] for st in self.subtime]
            self.time.insert(0, 0)
        else:
            its = tqdm(self.time[1:], miniters=10) if self.tqdm else self.time[1:]
            for time in its:
                (q, p) = self.step(time, q, p)
                self.sq.append(q)
                self.sp.append(p)
    '''
    def step(self, time, coordinate, momentum):
        """ perform a time step - stub method """
        return (coordinate, momentum)
    '''
    def get_trajectory(self, step=1, dtype=np.float):
        """ return a trajectory - a time series of coordinate and momentum """
        time = np.array(self.time, dtype=dtype)[::step]
        coordinate = np.array(self.sq, dtype=dtype)[::step]
        momentum = np.array(self.sp, dtype=dtype)[::step]
        return time, coordinate, momentum

    def get_trajectory_3d(self, step=1):
        """ return a trajectory of coordinates grouped in 3-tuples """
        ti, qt, pt = self.get_trajectory(step)
        qt.shape = (len(qt), -1, 3)
        pt.shape = (len(pt), -1, 3)
        return ti, qt, pt

    def analyse(self, step=1, p='err'):
        """ calculate relative energy error or relative energy difference
            (p = 1, 2, 3, ..., inf), see refs. https://doi.org/10.2307/2683905
            https://en.wikipedia.org/wiki/Relative_change_and_difference """
        from itertools import islice
        if not hasattr(self, 'en'):
            qptup = islice(zip(self.sq, self.sp), 0, None, step)
            self.en = np.array([self.syst.energy(*t) for t in qptup])
        if p == 'inf':
            func = lambda x, y: (x-y)/np.maximum(abs(x), abs(y))
        elif p == 'err':
            func = lambda x, y: (x-y)/y
        elif isinstance(p, int) and p > 0:
            func = lambda x, y: (x-y)/(abs(x)**p+abs(y)**p)**(1/p)
        else:
            raise ValueError('invalid value for p: '+repr(p))
        self.er = np.array(func(self.en, self.en0), dtype=float)


class PolynomialPropagator(Propagator):
    """ Base class for polynomial classical propagators """

    name = 'polynomial'
    coeffs = None
    iln = None

    def __init__(self, liouville=SPLPQP, liou_params={}, restart=False,
                 restart_file='propag.pkl', fpprec='fixed', prec=None,
                 nterms=None, delta=1., **kwargs):
        super().__init__(**kwargs)
        self.liouville = liouville
        self.liou_params = liou_params
        self.restart = restart
        self.restart_file = restart_file
        self.fpprec = fpprec
        self.prec = prec
        assert nterms is not None and nterms > 0
        self.nterms = nterms
        assert delta > 0.
        self.delta = delta
        self.alpha = self.delta*self.tstep/2.
        self.efficiency = self.alpha/self.nterms

    def set_polynomial(self, **kwargs):
        """ initialize the polynomial (iln) object """
        if self.restart:
            self.iln = self.load(self.restart_file)
        else:
            params = {'fpprec': self.fpprec, **kwargs, **self.liou_params}
            self.iln = self.liouville(self.nterms, **params)

    def step_nopbc(self, time, qval, pval):
        """ perform a time step with initial coordinate and momentum """
        qvec, pvec = self.iln.get_val_float(qval, pval)
        qt = np.dot(self.coeffs, qvec)
        pt = np.dot(self.coeffs, pvec)
        return (qt, pt)

    def step_pbc(self, *args, **kwargs):
        """ perform a time step with periodic boundary conditions """
        wrap = lambda x, y: (self.syst.wrap_q(x), y)
        return wrap(*self.step_nopbc(*args, **kwargs))

    def load(self, filename='propag.pkl'):
        """ deserialize the propagator object and extract the iln object """
        import cloudpickle as pkl
        with open(filename, 'rb') as fh:
            that = pkl.load(fh)
        params = ['__class__', 'name', 'nterms', 'delta', 'tstep', 'liouville',
                  'liou_params', 'fpprec', 'prec']
        for key in params:
            attr = getattr(that, key)
            errstr = key + ': ' + str(getattr(self, key)) + ' != ' + str(attr)
            assert getattr(self, key) == attr, errstr
        assert self.syst.hamiltonian == that.syst.hamiltonian
        return that.iln

    def dump(self, filename='propag.pkl'):
        """ serialize and save the propagator object """
        import cloudpickle as pkl
        with open(filename, 'wb') as fh:
            pkl.dump(self, fh)


class TwoStepPolynomialPropagator(PolynomialPropagator):
    """ Two-step time-symmetric polynomial propagators """

    name = 'two-step polynomial'
    coeffs = None
    iln = None

    def __init__(self, q_1=None, p_1=None, **kwargs):
        """ the starter propagator takes a backward time step """
        super().__init__(**kwargs)
        self.parity = (self.nterms-1)%2
        self.prefact = 2*self.parity-1
        if q_1 is None or p_1 is None:
            starter = self.starter(**kwargs)
            starter.coeffs = [c*(-1)**i for i, c in enumerate(starter.coeffs)]
            self.q_1, self.p_1 = starter.step(None, self.q0, self.p0)
        else:
            self.q_1, self.p_1 = q_1, p_1

    def step_nopbc(self, time, qval, pval):
        """ perform a time step with initial coordinate and momentum """
        qvec, pvec = self.iln.get_val_float(qval, pval)
        qt = np.dot(self.coeffs, qvec) + self.prefact*self.q_1
        pt = np.dot(self.coeffs, pvec) + self.prefact*self.p_1
        self.q_1 = qval
        self.p_1 = pval
        return (qt, pt)


class SymmetricPolynomialPropagator(PolynomialPropagator):
    """ Self-starting one-step time-symmetric polynomial propagators """

    name = 'symmetric polynomial propagator'
    numeric = {'tool': 'subs', 'modules': None}
    maxiter = 4

    def __init__(self, numeric=None, **kwargs):
        """
        -------------------------------------------------------------------
        fpprec  dtype       tool        modules     linalg      method
        -------------------------------------------------------------------
        fixed   np.float    subs        None        sympy       step_sympy
        fixed   np.float    theano      None        numpy       step_numpy
        fixed   np.float    lambdify    numpy       numpy       step_numpy
        multi   mp.Float    subs        None        sympy       step_sympy
        multi   mp.Float    lambdify    mpmath      mpmath      step_mpmath
        -------------------------------------------------------------------
        NOTABENE: for numeric tool lambdify modules 'tensorflow' and 'math'
        are currently not supported.
        """

        from generic import validate_numeric

        if numeric is not None:
            self.numeric = numeric
        elif hasattr(self.syst, 'numeric'):
            self.numeric = self.syst.numeric

        tool = self.numeric['tool']
        assert tool in ['subs', 'lambdify', 'theano']
        if tool == 'subs':
            self.linalg = 'sympy'
        if tool == 'lambdify':
            if self.numeric['modules'] == 'mpmath':
                self.linalg = 'mpmath'
            else:
                self.linalg = 'numpy'
        if tool == 'theano':
            self.linalg = 'numpy'

        super().__init__(**kwargs)

        validate_numeric(self.fpprec, self.numeric)

        if self.fpprec == 'fixed':
            self.prec = np.finfo(float).precision

        if self.linalg == 'sympy':
            self.matrix = sp.Matrix
            self.matmul = lambda x, y: x*y
            self.matinv = lambda x: x.inv()
            self.normf = lambda x: x.norm()
        elif self.linalg == 'mpmath':
            self.matrix = mp.matrix
            self.matmul = lambda x, y: x*y
            self.matinv = lambda x: x**(-1)
            self.normf = lambda x: mp.mnorm(x, p='f')
        else:
            self.matrix = np.array
            self.matmul = np.matmul
            self.matinv = np.linalg.inv
            self.normf = np.linalg.norm

        if isinstance(self.q, Iterable):
            assert isinstance(self.p, Iterable)
            assert isinstance(self.syst.q, Iterable)
            assert isinstance(self.syst.p, Iterable)
            assert len(self.q) == len(self.p)
            assert len(self.q) == len(self.syst.q)
            assert len(self.syst.q) == len(self.syst.p)
            self.concat = lambda a, b: (*a, *b)
            self.split = lambda x: (x[:len(x)//2], x[len(x)//2:])
        else:
            self.concat = lambda a, b: (a, b)
            self.split = lambda x: (x[0], x[1])

    def set_gvar_jacobian(self):
        """ initialize the G_var vector function and the Jacobian matrix """

        if self.fpprec == 'multi':
            self.coeffs = sp.Matrix(self.coeffs)
            self.reverse_coeffs = sp.Matrix(self.reverse_coeffs)
        ugq =self.reverse_coeffs.dot(self.iln.ilnqcart)
        ugp = self.reverse_coeffs.dot(self.iln.ilnpcart)
        if isinstance(self.syst.q, Iterable):
            gq = sp.Matrix(self.syst.q) + sp.Matrix(ugq)
            gp = sp.Matrix(self.syst.p) + sp.Matrix(ugp)
        else:
            gq = self.syst.q + ugq
            gp = self.syst.p + ugp
        self.z = sp.Matrix(self.concat(self.syst.q, self.syst.p))
        jacobian = sp.Matrix(self.concat(gq, gp)).jacobian(self.z)
        self.matrix_shape = jacobian.shape
        self.vector_shape = (self.z.shape[0],)
        if self.numeric['tool'] == 'lambdify':
            modules = self.numeric['modules']
            if self.numeric['modules'] == 'mpmath':
                gvar = sp.Matrix(self.concat(gq, gp))
            else:
                gvar = sp.Array(self.concat(gq, gp))
            self.jacobian = sp.lambdify(self.z, jacobian, modules)
            self.gvar = sp.lambdify(self.z, gvar, modules)
        elif self.numeric['tool'] == 'theano':
            from sympy.printing.theanocode import theano_function as tf
            gvar = sp.Matrix(self.concat(gq, gp))
            self.jacobian = tf(self.z, jacobian, on_unused_input='ignore')
            self.gvar = tf(self.z, gvar, on_unused_input='ignore')
        else:
            self.jacobian = jacobian
            self.gvar = sp.Matrix(self.concat(gq, gp))

    def array_eval(self, array, shape, zval):
        """ evaluate the elements of a matrix that are sympy expressions """
        if self.numeric['tool'] == 'subs':
            retval = array.xreplace(dict(zip(self.z, zval))).evalf()
        else:
            retval = array(*zval)
            if self.linalg == 'numpy':
                retval = np.array(retval)
                retval.shape = shape
        return retval

    def step_nopbc(self, time, qval, pval):
        """ implicit time stepping procedure using Newton iteration """
        qvec, pvec = self.iln.get_val_float(qval, pval)
        qt = self.coeffs.dot(qvec)
        pt = self.coeffs.dot(pvec)
        zval = self.matrix(self.concat(qt, pt))
        gcon = self.matrix(self.concat(qval, pval)) + zval
        residue = 1.
        numiter = 0
        while residue > 10**(-self.prec/2):
            if numiter > self.maxiter:
                raise RuntimeError('maximum number of iterations exceeded')
            jacval = self.array_eval(self.jacobian, self.matrix_shape, zval)
            gvar = self.array_eval(self.gvar, self.vector_shape, zval)
            correction = self.matmul(self.matinv(jacval), gvar-gcon)
            zval = zval - correction
            residue = self.normf(correction)
            numiter += 1
        return self.split(zval)
