""" Force field classes """
from itertools import chain
import sympy as sp
import numpy as np
from generic import get_partial_derivs, get_all_partial_derivs

class PairFF:
    """ Base class for all pair interaction potentials """

    pot_cart = None
    pot_inte = None

    def __init__(self, pair, distance=None, **kwargs):
        assert len(pair) == 2
        assert pair[0] != pair[1]

        print(pair)
        self.q = (*pair[0], *pair[1])
        print("q", self.q)
        self.r_cart = sp.sqrt(sum((i1-i2)**2 for i1, i2 in zip(pair[0], pair[1])))
        print("r_cart", self.r_cart)
        # euclidean distance of the particles

        rstring = 'r' + kwargs.get('label', '')
        print("rstring", rstring)
        vstring = 'V' + kwargs.get('label', '')

        self.r = sp.Function(rstring, real=True)(*self.q)
        print("r", self.r)
        self.Vq = sp.Function(vstring, real=True)(*self.q)
        self.Vr = sp.Function(vstring, real=True)(self.r)
        print("Vr", self.Vr)

        self.r0 = distance
        print("r0", self.r0)

        self.rc_stri = [str(i)+'_'+str(j) for i, j in zip(pair[0], pair[1])]
        self.rc_symb = [sp.Symbol(sym, real=True) for sym in self.rc_stri]
        tuples = zip(self.rc_stri, ((i, j) for i, j in zip(pair[0], pair[1])))
        self.rc_func = [sp.Function(s, real=True)(*p) for s, p in tuples]
        self.rc_valu = [i-j for i, j in zip(pair[0], pair[1])]
        self.rc_cart = sp.sqrt(sum(f**2 for f in self.rc_func))

    def get_potential_energy(self, ctype='cartesian'):
        """ Potential energy expression """
        return self.pot_cart if ctype == 'cartesian' else self.pot_inte

    def get_cart_derivs(self, n, explicit=False):
        """ the first n cartesian partial derivatives of the potential """
        ders = get_partial_derivs(self.Vq, self.q, n)
        repl = (self.Vq, self.pot_cart)
        return (d.subs(*repl).doit() for d in ders) if explicit else ders

    def get_inte_derivs(self, n, explicit=False):
        """ the first n distance partial derivatives of the potential """
        func = self.pot_inte if explicit else self.Vr
        ders = [sp.diff(func, self.r)]
        for j in range(1, n):
            ders.append(sp.diff(ders[j-1], self.r))
        return ders

    def get_dist_derivs(self, n, explicit=False, mixed=True):
        """ Evaluate the first n Cartesian derivatives of the distance. For
        numerical evaluation the values are necessary for i) [r, *Q] (seven
        values) n case of mixed form; ii) [*Q] (six values) in case of
        implicit form """
        dersi = get_partial_derivs(self.r, self.q, n)
        if explicit or mixed:
            reple = (self.r, self.r_cart)
            derse = (d.subs(*reple).doit() for d in dersi)
        if explicit:
            return derse
        elif mixed:
            replm = (self.r_cart, self.r)
            dersm = (d.subs(*replm) for d in derse)
            return dersm
        else:
            return dersi

    def get_explicit_dists_repl(self):
        return dict(zip(self.rc_valu, self.rc_symb))

    def get_full_derivs(self, n, full=False):
        """ Evaluate the first n full derivatives of the potential in terms of
        the components of the distance vectors, for example
        r01_vec = (q0x_q1x, q0y_q1y, q0z_q1z) """

        gpd = get_all_partial_derivs if full else get_partial_derivs
        der0 = gpd(self.Vr, self.q, n)
        reple = (self.Vr, self.pot_inte)
        der1 = (d.subs(*reple).doit().subs(*reple).doit() for d in der0)
        replc = (self.r, self.r_cart)
        der2 = (d.subs(*replc).doit() for d in der1)
        repld = self.get_explicit_dists_repl()
        return (d.subs(repld).doit().simplify() for d in der2)


class FFZero(PairFF):
    """ Zero interaction potential """

    def __init__(self, pair, **kwargs):
        super().__init__(pair, **kwargs)
        self.pot_inte = sp.S.Zero
        self.pot_cart = sp.S.Zero
        print("pot_inte", self.pot_inte)
        print("pot_cart", self.pot_cart)

class FFNewtonian(PairFF):
    """ Newton potential for a pair of particles """

    def __init__(self, pair, masses, **kwargs):
        super().__init__(pair, **kwargs)
        assert len(pair) == len(masses)
        self.masses = masses
        print(masses)
        self.pot_inte = self.masses[0]*self.masses[1]/self.r
        # print("r:::", -self.r.is_positive)
        print("r", self.r)
        self.pot_cart = self.pot_inte.subs(self.r, self.r_cart)
        print(type(self.pot_inte))
        print(type(self.r0))
        print(type(self.masses[0]))
        print("pot_inte", self.pot_inte)
        print("pot_cart", self.pot_cart)

class FFCoulomb(PairFF):
    """ Coulomb potential for a pair of particles """

    def __init__(self, pair, charge, **kwargs):
        super().__init__(pair, **kwargs)
        assert len(pair) == len(charge)
        self.charge = charge
        self.pot_inte = (self.charge[0]*self.charge[1])/self.r
        self.pot_cart = self.pot_inte.subs(self.r, self.r_cart)
        print(type(self.pot_inte))
        print("r:::", type(self.r))
        print("r0:::", type(self.r0))
        print(type(self.charge[0]))
        print("pot_inte", self.pot_inte)
        print("pot_cart", self.pot_cart)


class FFHarmonic(PairFF):
    """ Bond stretching described by a harmonic potential """

    def __init__(self, pair, distance, kappa=1.0, **kwargs):
        super().__init__(pair, distance, **kwargs)
        self.pot_inte = kappa*(self.r-self.r0)**2
        self.pot_cart = self.pot_inte.subs(self.r, self.r_cart)
        print("pot_inte", self.pot_inte)
        print("pot_cart", self.pot_cart)

class FFMorse(PairFF):
    """ Bond stretching described by a Morse potential """

    def __init__(self, pair, distance, kappa=1.0, d=1.0, **kwargs):
        super().__init__(pair, distance, **kwargs)
        self.pot_inte = (d*(sp.exp(-2*kappa*(self.r-self.r0))
                         -2*sp.exp(-kappa*(self.r-self.r0))))
        self.pot_cart = self.pot_inte.subs(self.r, self.r_cart)
        print("pot_inte", self.pot_inte)
        print("pot_cart", self.pot_cart)

class FFLennardJones(PairFF):
    """ Lennard-Jones potential for a pair of particles """

    def __init__(self, pair, sigma=1, epsilon=1, **kwargs):
        super().__init__(pair, **kwargs)
        self.pot_inte = 4*epsilon*((sigma/self.r)**12-(sigma/self.r)**6)
        self.pot_cart = self.pot_inte.subs(self.r, self.r_cart)
        print("pot_inte", self.pot_inte)
        print("pot_cart", self.pot_cart)

class FFList(list):
    """ Force fields aggregation class """

    def __init__(self, ffparams, Q):
        """ ffparams: [{'class': str, 'pair': (), 'params': {}}] """
        namespace = __import__(__name__)
        for subpackage in __name__.split('.')[1:]:
            namespace = getattr(namespace, subpackage)
        for index, ffpar in enumerate(ffparams):
            params = ffpar.get('params', {})
            params['label'] = str(index)
            ffclass = getattr(namespace, ffpar['class'])
            ffqpar = tuple(tuple(Q[i] for i in t) for t in ffpar['pair'])
            self.append(ffclass(ffqpar, **params))

    def get_potential_energy(self, ctype='cartesian'):
        """ Explicit expression of the potential energy  """
        return sum(self.get_explicit_potentials(ctype))

    def get_symbolic_potential(self, ctype='cartesian'):
        """ Implicit expression of the total potential energy """
        return sum(self.get_symbolic_potentials(ctype))

    def get_symbolic_potentials(self, ctype='cartesian'):
        """ Implicit expressions of the potential energies """
        return (f.Vq for f in self) if ctype == 'cartesian' else (f.Vr for f in self)

    def get_explicit_potentials(self, ctype='cartesian'):
        """ Explicit expressions of the potential energies """
        return (ff.get_potential_energy(ctype) for ff in self)

    def get_symbolic_derivs(self, maxorder, ctype='cartesian'):
        """ Expressions of the potential derivatives in symbolic form """
        for der in self.get_implicit_derivs(maxorder, ctype):
            symb = str(der.expr.func)+''.join([str(v) for v in der.variables])
            yield sp.Symbol(symb, real=True)

    def get_implicit_derivs(self, maxorder, ctype='cartesian'):
        """ Expressions of the potential derivatives in implicit form """
        if ctype == 'cartesian':
            gens = [f.get_cart_derivs(maxorder, explicit=False) for f in self]
        else:
            gens = [f.get_inte_derivs(maxorder, explicit=False) for f in self]
        return chain(*gens)

    def get_explicit_derivs(self, maxorder, ctype='cartesian'):
        """ Expressions of the potential derivatives in explicit form """
        if ctype == 'cartesian':
            gens = [f.get_cart_derivs(maxorder, explicit=True) for f in self]
        else:
            gens = [f.get_inte_derivs(maxorder, explicit=True) for f in self]
        return chain(*gens)
