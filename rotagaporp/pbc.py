""" methods for periodic boundary conditions, orthorhombic system only """
import numpy as np

def get_distance_matrix_lc(pos, cell):
    """ calculate the distance vector for all positions in pos with periodic
    boundary conditions w.r.t. cell; list comprehension used
    original code from: http://blog.lostinmyterminal.com/physics/2015/04/09/
    periodic-boundary-subtraction.html """
    distvec = np.array([np.subtract.outer(rd, rd) for rd in pos.T]).T
    return np.remainder(distvec + cell/2., cell) - cell/2.

def get_distance_matrix_bc(pos, cell):
    """ calculate the distance vector for all positions in pos with periodic
    boundary conditions w.r.t. cell; numpy broadcasting used
    original code from: http://blog.lostinmyterminal.com/physics/2015/04/09/
    periodic-boundary-subtraction.html """
    distvec = pos.T[np.newaxis, ...] - pos[..., np.newaxis]
    distvec = np.rollaxis(distvec, -1, -2)
    return np.remainder(distvec + cell/2., cell) - cell/2.

def wrap_positions(pos, cell):
    """ wrap the positions into cell """
    return np.mod(pos, cell)
