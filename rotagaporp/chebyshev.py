""" Chebyshev propagator for classical particle dynamics """

import numpy as np
import sympy as sp
from scipy.special import jn, iv
from propagators import PolynomialPropagator
from propagators import TwoStepPolynomialPropagator
from propagators import SymmetricPolynomialPropagator


def cheb_coef(bf, nterms, alpha):
    """ chebyshev expansion coefficients for the function exp(z*t) """
    coef = bf(range(nterms), np.full(nterms, alpha))
    coef[1:] = coef[1:]*2.
    return coef


def besself(fpprec, signed, prec=None):
    """ select the proper bessel function according to requested precision
        and selected axis: if signed is True [-1, +1] otherwise [-i, +i] """
    assert fpprec in ['fixed', 'multi']
    if fpprec == 'fixed':
        bessel = iv if signed else jn
    elif fpprec == 'multi':
        fargs = [] if prec is None else [prec]
        bess = sp.besseli if signed else sp.besselj
        def bessel(orders, args):
            bfun = [bess(o, a).evalf(*fargs) for o, a in zip(orders, args)]
            return np.array(bfun)
    return bessel


def chebyshev_scalar(z, t, delta=None, lambda_min=None, nterms=None,
                     signed=False, fpprec='fixed', prec=None):
    """ Chebyshev polynomial expansion of the scalar function exp(z*t) """
    assert isinstance(z, np.ndarray)
    delta = max(abs(z)) if delta is None else delta
    alpha = delta*t/2.
    lambda_min = -delta/2. if lambda_min is None else lambda_min
    nterms = int(np.ceil(abs(alpha)))+1 if nterms is None else nterms
    assert nterms > 1
    scale = 2./delta
    shift = -(2.*lambda_min/delta+1.)
    sign = -1 if signed else 1
    z = z*scale+shift
    exp = np.exp if fpprec == 'fixed' else sp.exp
    scaling = exp((lambda_min+delta/2.)*t)
    ch_coef = cheb_coef(besself(fpprec, signed, prec), nterms, alpha)*scaling
    ch_poly = np.empty((nterms, len(z)), dtype=z.dtype)
    ch_poly[0, :] = 1.
    ch_poly[1, :] = z
    for order in range(2, nterms):
        ch_poly[order, :] = 2.*z*ch_poly[order-1, :]+sign*ch_poly[order-2, :]
    return np.dot(ch_coef, ch_poly)


class Chebyshev(PolynomialPropagator):
    """ Chebyshev propagator for classical particle dynamics """
    name = 'chebyshev'
    def __init__(self, signed=False, **kwargs):
        super().__init__(**kwargs)

        bf = besself(self.fpprec, signed, prec=self.prec)
        assert isinstance(signed, bool)
        sign = -1 if signed else 1

        if self.substep is not None:
            self.coeffs = []
            for stime in self.subtime:
                alpha = self.delta*stime/2.
                self.coeffs.append(cheb_coef(bf, self.nterms, alpha))
        else:
            self.coeffs = cheb_coef(bf, self.nterms, self.alpha)
        self.set_polynomial(system=self.syst, scale=2./self.delta, sign=sign,
                            recursion='chebyshev')


class TwoStepChebyshev(TwoStepPolynomialPropagator):
    """ Two-step time-symmetric Chebyshev propagator """
    name = 'two-step chebyshev'
    starter = Chebyshev
    def __init__(self, signed=False, **kwargs):
        super().__init__(**kwargs)

        bf = besself(self.fpprec, signed, prec=self.prec)
        assert isinstance(signed, bool)
        sign = -1 if signed else 1
        assert self.substep is None, 'substep not implemented'
        self.coeffs = 2*cheb_coef(bf, self.nterms, self.alpha)[self.parity::2]
        self.set_polynomial(system=self.syst, scale=2./self.delta, sign=sign,
                            recursion='chebyshev', parity=self.parity)


class SymmetricChebyshev(SymmetricPolynomialPropagator):
    """ Self-starting time-symmetric Chebyshev propagator """

    name = 'symmetric chebyshev'

    def __init__(self, signed=False, **kwargs):
        super().__init__(**kwargs)

        bf = besself(self.fpprec, signed, prec=self.prec)
        assert isinstance(signed, bool)
        sign = -1 if signed else 1
        assert self.substep is None, 'substep not implemented'
        self.coeffs = cheb_coef(bf, self.nterms, self.alpha)
        self.reverse_coeffs = cheb_coef(bf, self.nterms, -self.alpha)
        self.set_polynomial(system=self.syst, scale=2./self.delta, sign=sign,
                            recursion='chebyshev')
        self.set_gvar_jacobian()
