""" Generic operations on symbolic expresions and data structures """
import sympy as sp

def _float(fpprec, f):
    """ function wrapper for conditional casting to python float """
    if fpprec == 'fixed':
        def newfunc(*args, **kwargs):
            return float(f(*args, **kwargs))
    else:
        newfunc = f
    return newfunc

def spfloat(f, prec):
    """ creates a sympy float with specified precision from a python float """
    if isinstance(f, float):
        from decimal import Decimal
        from fractions import Fraction
        # only in Python 3.6 or later
        # x, y = Decimal(repr(f)).as_integer_ratio()
        # ret = sp.N(x, prec)/sp.N(y, prec)
        frac = Fraction.from_decimal(Decimal(repr(f)))
        ret = sp.N(frac.numerator, prec)/sp.N(frac.denominator, prec)
    else:
        ret = f
    return ret

def flatten(x):
    """ Return a flat list from a deeply nested iterables """
    from collections import Iterable
    return (a for i in x for a in flatten(i)) if isinstance(x, Iterable) else [x]

def uniq(seq):
    """ filter out duplicated elements and return the unique ones """
    seen = set()
    seen_add = seen.add
    return [x for x in seq if not (x in seen or seen_add(x))]

def get_partial_derivs(function, variables, n, flatlist=True):
    """ Evaluate the first n partial derivatives of a function with respect to
        specified variables and return a list of unique derivatives """

    ders = [uniq(sp.derive_by_array(function, variables))]
    for j in range(1, n):
        ders.append(uniq(sp.derive_by_array(ders[j-1], variables)))
    return flatten(ders) if flatlist else ders

def get_all_partial_derivs(function, variables, n, flatlist=True):
    """ Evaluate the first n partial derivatives of a function with respect to
        specified variables and return a flat iterable or a list of tensors """
    ders = [sp.derive_by_array(function, variables)]
    for j in range(1, n):
        ders.append(sp.derive_by_array(ders[j-1], variables))
    return flatten(ders) if flatlist else ders

def lambdify_long(symbs, expr, *args, **kwargs):
    """ symbs: an iterable of symbols, expr: expression with these symbols """
    from types import GeneratorType
    from collections import Iterable
    from itertools import chain
    dvec = sp.DeferredVector('x')
    repl = {q: dvec[i] for i, q in enumerate(symbs)}
    if isinstance(expr, (Iterable, GeneratorType, chain)):
        sexpr = [e.xreplace(repl) for e in expr]
    else:
        sexpr = expr.xreplace(repl)
    return sp.lambdify(dvec, sexpr, *args, **kwargs)

def validate_numeric(fpprec, numeric):
    """ validate consistency between fpprec and numeric parameters """
    if fpprec != 'fixed':
        assert numeric['tool'] in ['subs', 'lambdify']
        if numeric['tool'] == 'lambdify':
            mods = numeric['modules']
            if isinstance(mods, list):
                assert all(f in ['mpmath', 'sympy'] for f in mods)
            else:
                assert mods in ['mpmath', 'sympy']
