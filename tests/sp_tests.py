""" Basic short tests of one-particle and auxiliary classes and functions """
import unittest
import numpy as np
import sympy as sp
from rotagaporp.liouville import q, p
from rotagaporp.liouville import LiouvillePowers
from rotagaporp.liouville import SPLP, SPLPQP, SPLPNR
from rotagaporp.symplectic import Symplectic
from rotagaporp.chebyshev import Chebyshev, TwoStepChebyshev
from rotagaporp.newtonian import Newtonian
from rotagaporp.newtonian import leja_points_ga, leja_points_jb
from rotagaporp.newtonian import chebyshev_points
from rotagaporp.newtonian import newton_scalar
from rotagaporp.systems import Harmonic, Anharmonic, Ballistic
from rotagaporp.systems import Morse, LennardJones
from rotagaporp.systems import ManyParticlePhysicalSystem
from rotagaporp.forcefield import PairFF, FFHarmonic
from rotagaporp.forcefield import FFLennardJones, FFMorse, FFCoulomb
from rotagaporp.mpliouville import MPLPQP
from rotagaporp.pbc import get_distance_matrix_lc as dists1
from rotagaporp.pbc import get_distance_matrix_bc as dists2
from rotagaporp.pbc import wrap_positions as wrap
from rotagaporp.combinatorics import cnk_chebyshev_T1, cnk_chebyshev_T2
from rotagaporp.combinatorics import cnk_chebyshev_T3
from rotagaporp.combinatorics import cnk_chebyshev_U1, cnk_chebyshev_U2
from rotagaporp.combinatorics import cnk_chebyshev_U3

atol32 = np.finfo(np.float32).resolution

class LiouvillePowersTest(unittest.TestCase):
    """ Test of the one-particle Liouville powers class """

    def test_liouvillepower_expr(self):
        """ expression tests """
        ilnp = LiouvillePowers(4, function=p, h_mixed=False)
        iln_expr = ilnp.get_expr(hamiltonian=(q**2+p**2)/2)
        self.assertEqual(iln_expr, [p, -q, -p, q])
        ilnq = LiouvillePowers(4, function=q, h_mixed=False)
        iln_expr = ilnq.get_expr(hamiltonian=(q**2+p**2)/2)
        self.assertEqual(iln_expr, [q, p, -q, -p])
        ilnf = LiouvillePowers(4)
        iln_expr = ilnf.get_expr(hamiltonian=(q**2+p**2)/2, function=p)
        self.assertEqual(iln_expr, [p, -q, -p, q])
        ilnf = LiouvillePowers(2, hamiltonian=(q**2+p**2)/2)
        self.assertEqual(ilnf.get_expr(function=q), [q, p])

    def test_liouvillepower_eval(self):
        """ numeric evaluation tests """
        ilnp = LiouvillePowers(4, hamiltonian=(q**2+p**2)/2, function=p)
        self.assertEqual(ilnp.get_val_direct(1, 1), [1, -1, -1, 1])
        self.assertEqual(ilnp.get_val_float(1, 1), [1, -1, -1, 1])
        hamiltonian = (p**2 + q**4)/2 - q**2
        ilnq = LiouvillePowers(4, hamiltonian=hamiltonian, function=q)
        ref_expr = [q, p, -2*q**3 + 2*q, -6*p*q**2 + 2*p]
        ref_val = [1, 1, 0, -4]
        self.assertEqual(ilnq.get_expr(), ref_expr)
        self.assertEqual(ilnq.get_val_direct(1, 1), ref_val)
        self.assertEqual(ilnq.get_val_float(1, 1), ref_val)

    def test_liouvillepower_repr_qp(self):
        """ symbols output tests """
        ilnq = LiouvillePowers(6, function=q, h_mixed=False)
        ilnp = LiouvillePowers(6, function=p, h_mixed=False)
        ilnq_ref = (r'iL^n (f: q): [q, B1, -A1*B2, A1**2*B3 - A2*B1*B2, '
                    r'-A1**3*B4 + 3*A1*A2*B1*B3 + A1*A2*B2**2 - A3*B1**2*B2, '
                    r'A1**4*B5 - 6*A1**2*A2*B1*B4 - 5*A1**2*A2*B2*B3 + '
                    r'4*A1*A3*B1**2*B3 + 3*A1*A3*B1*B2**2 + 3*A2**2*B1**2*B3 '
                    r'+ A2**2*B1*B2**2 - A4*B1**3*B2]')
        ilnp_ref = (r'iL^n (f: p): [p, -A1, -A2*B1, A1*A2*B2 - A3*B1**2, '
                    r'-A1**2*A2*B3 + 3*A1*A3*B1*B2 + A2**2*B1*B2 - A4*B1**3, '
                    r'A1**3*A2*B4 - 4*A1**2*A3*B1*B3 - 3*A1**2*A3*B2**2 - '
                    r'3*A1*A2**2*B1*B3 - A1*A2**2*B2**2 + 6*A1*A4*B1**2*B2 + '
                    r'5*A2*A3*B1**2*B2 - A5*B1**4]')
        self.assertEqual(repr(ilnq), ilnq_ref)
        self.assertEqual(repr(ilnp), ilnp_ref)

    def test_liouvillepower_repr_f(self):
        ilnf = LiouvillePowers(5, h_mixed=False, f_mixed=False)
        ilnf_ref = (r'iL^n (f: f(q, p)): [f(q, p), -A1*b1 + B1*a1, A1**2*b2 -'
                    r' A1*B2*a1 - A2*B1*b1 + B1**2*a2, -A1**3*b3 + A1**2*B3*a'
                    r'1 + 3*A1*A2*B1*b2 + A1*A2*B2*b1 - 3*A1*B1*B2*a2 - A2*B1'
                    r'*B2*a1 - A3*B1**2*b1 + B1**3*a3, A1**4*b4 - A1**3*B4*a1'
                    r' - 6*A1**2*A2*B1*b3 - 4*A1**2*A2*B2*b2 - A1**2*A2*B3*b1'
                    r' + 4*A1**2*B1*B3*a2 + 3*A1**2*B2**2*a2 + 3*A1*A2*B1*B3*'
                    r'a1 + A1*A2*B2**2*a1 + 4*A1*A3*B1**2*b2 + 3*A1*A3*B1*B2*'
                    r'b1 - 6*A1*B1**2*B2*a3 + 3*A2**2*B1**2*b2 + A2**2*B1*B2*'
                    r'b1 - 4*A2*B1**2*B2*a2 - A3*B1**2*B2*a1 - A4*B1**3*b1 + '
                    r'B1**4*a4]')
        self.assertEqual(repr(ilnf), ilnf_ref)

    def test_liouvillepower_symb(self):
        """ numeric evaluation tests """
        qexpr, pexpr = sp.symbols('q p')
        ilnq = LiouvillePowers(4, hamiltonian=(qexpr**2+pexpr**2)/2,
                               function=qexpr)
        self.assertEqual(ilnq.get_val_direct(1, 1), [1, 1, -1, -1])
        self.assertEqual(ilnq.get_val_float(1, 1), [1, 1, -1, -1])

    def test_qp_symmetry(self):
        """ test iL^n q == iL^(n-1) p when kinetic energy is explicit """

        nterms = 5
        # symmetry if kinetic energy explicitly defined, T(p) = p^2/2
        H = p**2/2 + sp.Function('V', real=True)(q)
        lpq_t = LiouvillePowers(nterms, hamiltonian=H, function=q,
                                recursion='taylor')
        lpp_t = LiouvillePowers(nterms, hamiltonian=H, function=p,
                                recursion='taylor')
        for n in range(1, nterms):
            self.assertEqual(lpq_t.Lf[n], lpp_t.Lf[n-1])

        # no symmetry if kinetic energy not explicitly defined, T(p)
        H = sp.Function('V', real=True)(q) + sp.Function('T', real=True)(p)
        lpq_t = LiouvillePowers(nterms, hamiltonian=H, function=q,
                                recursion='taylor')
        lpp_t = LiouvillePowers(nterms, hamiltonian=H, function=p,
                                recursion='taylor')
        for n in range(1, nterms):
            self.assertNotEqual(lpq_t.Lf[n], lpp_t.Lf[n-1])

    def test_direct_recursive(self):
        """ check that expressions with Chebyshev recursion
            T_n(iL) f = 2 iL T_{n-1}(iL) f -/+ T_{n-2}(iL) f and the expansion
            T_n(iL) f = sum_{k=0}^n C^(n)_k (iL)^k f are equal """

        nterms = 5
        lpq_t = LiouvillePowers(nterms, recursion='taylor')
        for signed in (True, False):
            sign = -1 if signed else 1
            lpq_c = LiouvillePowers(nterms, recursion='chebyshev', sign=sign)
            for n in range(nterms):
                cnk = [cnk_chebyshev_T2(n, k, signed=signed) for k in range(n+1)]
                direct = sum(lpq_t.Lf[k]*cnk[k] for k in range(n+1))
                recurs = lpq_c.Lf[n]
                assert direct == recurs


class SPLPNRTest(unittest.TestCase):
    """ Test the SPLPNR class """

    def test_nrcheb_symbolic(self):
        """ recursive and non-recursive Chebyshev expansions: symbolic """
        nterms = 8
        class system:
            q = q
            p = p
            hamiltonian = p**2/2 + sp.Function('V', real=True)(q)
        syst = system()
        lp_nr = SPLPNR(nterms, syst, recursion='chebyshev', sign=1)
        lp_tr = SPLPQP(nterms, syst, recursion='taylor')
        self.assertListEqual(lp_tr.qobj.Lf, lp_nr.ilnq)
        self.assertListEqual(lp_tr.pobj.Lf, lp_nr.ilnp)
        lp_cr = SPLPQP(nterms, syst, recursion='chebyshev', sign=1)
        self.assertListEqual(lp_cr.qobj.Lf, lp_nr.ilnqexpr.tolist())
        self.assertListEqual(lp_cr.pobj.Lf, lp_nr.ilnpexpr.tolist())

    def test_nrcheb_numeric(self):
        """ recursive and non-recursive Chebyshev expansions: numeric """
        nterms = 12
        syst = Harmonic()
        lp_nr = SPLPNR(nterms, syst, recursion='chebyshev', sign=1)
        lp_cr = SPLPQP(nterms, syst, recursion='chebyshev', sign=1)
        qptest = (1., -1.)
        qvec_nr, pvec_nr = lp_nr.get_val_float_direct(*qptest)
        qvec_cr, pvec_cr = lp_cr.get_val_float(*qptest)
        self.assertListEqual(qvec_nr.tolist(), qvec_cr)
        self.assertListEqual(pvec_nr.tolist(), pvec_cr)
        qvec_nr, pvec_nr = lp_nr.get_val_float_nrcheb(*qptest)
        self.assertListEqual(qvec_nr.tolist(), qvec_cr)
        self.assertListEqual(pvec_nr.tolist(), pvec_cr)

    def test_nrnewt_symbolic(self):
        """ recursive and non-recursive Newton expansions: symbolic """
        nterms = 4
        class system:
            q = q
            p = p
            hamiltonian = p**2/2 + sp.Function('V', real=True)(q)
        syst = system()
        lambdas, scaling = leja_points_ga(nterms)
        lp_nr = SPLPNR(nterms, syst, recursion='newton', scale=1./scaling,
                       nodes=lambdas)
        lp_tr = SPLPQP(nterms, syst, recursion='taylor', scale=1./scaling)
        self.assertListEqual(lp_tr.qobj.Lf, lp_nr.ilnq)
        self.assertListEqual(lp_tr.pobj.Lf, lp_nr.ilnp)
        lp_cr = SPLPQP(nterms, syst, recursion='newton', scale=1./scaling,
                       nodes=lambdas)
        qchck = all(n.equals(r) for n, r in zip(lp_nr.ilnqexpr, lp_cr.qobj.Lf))
        self.assertTrue(qchck)
        pchck = all(n.equals(r) for n, r in zip(lp_nr.ilnpexpr, lp_cr.pobj.Lf))
        self.assertTrue(pchck)

    def test_nrnewt_numeric(self):
        """ recursive and non-recursive Newton expansions: numeric """
        nterms = 12
        syst = LennardJones()
        lambdas, scaling = leja_points_ga(nterms)
        lp_nr = SPLPNR(nterms, syst, recursion='newton', scale=1./scaling,
                       nodes=lambdas)
        lp_cr = SPLPQP(nterms, syst, recursion='newton', scale=1./scaling,
                       nodes=lambdas)
        qptest = (5., -1.)
        qvec_nr, pvec_nr = lp_nr.get_val_float_direct(*qptest)
        qvec_cr, pvec_cr = lp_cr.get_val_float(*qptest)
        self.assertTrue(np.allclose(qvec_nr, qvec_cr))
        self.assertTrue(np.allclose(pvec_nr, pvec_cr))
        qvec_nr, pvec_nr = lp_nr.get_val_float_nrnewt(*qptest)
        self.assertTrue(np.allclose(qvec_nr, qvec_cr))
        self.assertTrue(np.allclose(pvec_nr, pvec_cr))


class SPLPTest(unittest.TestCase):
    """ Tests for the SPLP class """

    hamsplit = sp.Function('V', real=True)(q)+sp.Function('T', real=True)(p)
    hamgener = sp.Function('H', real=True)(q, p)
    funsplit = sp.Function('fq', real=True)(q)+sp.Function('fp', real=True)(p)
    fungener = sp.Function('f', real=True)(q, p)

    def test_liouvillepower_expr(self):
        """ expression tests """
        ilnp = SPLP(4, hamiltonian=(q**2+p**2)/2, coordinate=q, momentum=p,
                    function=p)
        self.assertEqual(ilnp.Lf, [p, -q, -p, q])
        ilnq = SPLP(4, hamiltonian=(q**2+p**2)/2, coordinate=q, momentum=p,
                    function=q)
        self.assertEqual(ilnq.Lf, [q, p, -q, -p])
        ilnf = SPLP(4, hamiltonian=(q**2+p**2)/2, coordinate=q, momentum=p,
                    function=p)
        self.assertEqual(ilnf.Lf, [p, -q, -p, q])
        ilnf = SPLP(2, hamiltonian=(q**2+p**2)/2, coordinate=q, momentum=p,
                    function=q)
        self.assertEqual(ilnf.Lf, [q, p])

    def test_liouvillepower_eval(self):
        """ numeric evaluation tests """
        ilnp = SPLP(4, hamiltonian=(q**2+p**2)/2, coordinate=q, momentum=p,
                    function=p)
        self.assertEqual(ilnp.get_val_float(1, 1), [1, -1, -1, 1])
        hamiltonian = (p**2 + q**4)/2 - q**2
        ilnq = SPLP(4, hamiltonian=hamiltonian, coordinate=q, momentum=p,
                    function=q)
        ref_expr = [q, p, -2*q**3 + 2*q, -6*p*q**2 + 2*p]
        ref_val = [1, 1, 0, -4]
        self.assertEqual(ilnq.Lf, ref_expr)
        self.assertEqual(ilnq.get_val_float(1, 1), ref_val)

    def test_liouvillepower_repr_qp(self):
        """ symbols output tests for iL applied to variables q and p """
        ilnq = SPLP(6, self.hamsplit, coordinate=q, momentum=p, function=q)
        ilnp = SPLP(6, self.hamsplit, coordinate=q, momentum=p, function=p)
        ilnq_ref = (r'P_n (f: q): [q, B1, -A1*B2, A1**2*B3 - A2*B1*B2, '
                    r'-A1**3*B4 + 3*A1*A2*B1*B3 + A1*A2*B2**2 - A3*B1**2*B2, '
                    r'A1**4*B5 - 6*A1**2*A2*B1*B4 - 5*A1**2*A2*B2*B3 + '
                    r'4*A1*A3*B1**2*B3 + 3*A1*A3*B1*B2**2 + 3*A2**2*B1**2*B3 '
                    r'+ A2**2*B1*B2**2 - A4*B1**3*B2]')
        ilnp_ref = (r'P_n (f: p): [p, -A1, -A2*B1, A1*A2*B2 - A3*B1**2, '
                    r'-A1**2*A2*B3 + 3*A1*A3*B1*B2 + A2**2*B1*B2 - A4*B1**3, '
                    r'A1**3*A2*B4 - 4*A1**2*A3*B1*B3 - 3*A1**2*A3*B2**2 - '
                    r'3*A1*A2**2*B1*B3 - A1*A2**2*B2**2 + 6*A1*A4*B1**2*B2 + '
                    r'5*A2*A3*B1**2*B2 - A5*B1**4]')
        self.assertEqual(repr(ilnq), ilnq_ref)
        self.assertEqual(repr(ilnp), ilnp_ref)

    def test_liouvillepower_repr_f(self):
        """ symbols output tests for iL applied to f = fq(q) + fp(p) """
        ilnf = SPLP(5, self.hamsplit, coordinate=q, momentum=p,
                    function=self.funsplit)
        ilnf_ref = (r'P_n (f: fp(p) + fq(q)): [fp(p) + fq(q), -A1*b1 + B1*a1'
                    r', A1**2*b2 - A1*B2*a1 - A2*B1*b1 + B1**2*a2, -A1**3*b3 '
                    r'+ A1**2*B3*a1 + 3*A1*A2*B1*b2 + A1*A2*B2*b1 - 3*A1*B1*B'
                    r'2*a2 - A2*B1*B2*a1 - A3*B1**2*b1 + B1**3*a3, A1**4*b4 -'
                    r' A1**3*B4*a1 - 6*A1**2*A2*B1*b3 - 4*A1**2*A2*B2*b2 - A1'
                    r'**2*A2*B3*b1 + 4*A1**2*B1*B3*a2 + 3*A1**2*B2**2*a2 + 3*'
                    r'A1*A2*B1*B3*a1 + A1*A2*B2**2*a1 + 4*A1*A3*B1**2*b2 + 3*'
                    r'A1*A3*B1*B2*b1 - 6*A1*B1**2*B2*a3 + 3*A2**2*B1**2*b2 + '
                    r'A2**2*B1*B2*b1 - 4*A2*B1**2*B2*a2 - A3*B1**2*B2*a1 - A4'
                    r'*B1**3*b1 + B1**4*a4]')
        self.assertEqual(repr(ilnf), ilnf_ref)

    def test_liouvillepower_symb(self):
        """ numeric evaluation tests """
        ilnq = SPLP(4, hamiltonian=(q**2+p**2)/2, coordinate=q, momentum=p,
                    function=q)
        self.assertEqual(ilnq.get_val_float(1, 1), [1, 1, -1, -1])

    def test_qp_symmetry(self):
        """ test iL^n q == iL^(n-1) p when kinetic energy is T(p) = p^2/2 """

        nterms = 5
        ham = p**2/2 + sp.Function('V', real=True)(q)
        lpq_t = SPLP(nterms, ham, coordinate=q, momentum=p, function=q,
                     recursion='taylor')
        lpp_t = SPLP(nterms, ham, coordinate=q, momentum=p, function=p,
                     recursion='taylor')
        for n in range(1, nterms):
            self.assertEqual(lpq_t.Lf[n], lpp_t.Lf[n-1])

        # no symmetry if kinetic energy not explicitly defined, T(p)
        lpq_t = SPLP(nterms, self.hamsplit, coordinate=q, momentum=p,
                     function=q, recursion='taylor')
        lpp_t = SPLP(nterms, self.hamsplit, coordinate=q, momentum=p,
                     function=p, recursion='taylor')
        for n in range(1, nterms):
            self.assertNotEqual(lpq_t.Lf[n], lpp_t.Lf[n-1])

    def test_direct_recursive(self):
        """ check that the Chebyshev recurrence relations
            T_n(iL) f = 2 iL T_{n-1}(iL) f -/+ T_{n-2}(iL) f and the expansion
            T_n(iL) f = sum_{k=0}^n C^(n,-/+)_k (iL)^k f are equal """

        nterms = 5
        cnk = cnk_chebyshev_T2
        ilnt = SPLP(nterms, self.hamgener, coordinate=q, momentum=p,
                    function=self.fungener, recursion='taylor')
        for signed in (True, False):
            sign = -1 if signed else 1
            ilnc = SPLP(nterms, self.hamgener, coordinate=q, momentum=p,
                        function=self.fungener, recursion='chebyshev',
                        sign=sign)
            for n in range(nterms):
                direct = sum(ilnt.Lf[k]*cnk(n, k, signed) for k in range(n+1))
                self.assertEqual(direct, ilnc.Lf[n])

    def test_chebyshev_parity(self):
        """ compare Chebyshev polynomials of same parity with and without
            parity parameter for different sign, valid for any H and f """

        nterms = 7
        # 102 seconds for nterms=5
        # hamiltonian, function = self.hamgener, self.fungener
        # 11 seconds for nterms=5
        # hamiltonian, function = self.hamsplit, self.funsplit
        # 3 seconds for nterms=7
        hamiltonian, function = self.hamsplit, q
        for sign in (-1, 1):
            ilnall = SPLP(nterms, hamiltonian, q, p, function,
                          recursion='chebyshev', sign=sign, parity=None)
            ilneven = SPLP(nterms, hamiltonian, q, p, function,
                           recursion='chebyshev', sign=sign, parity=0)
            self.assertEqual(ilnall.Lf[0].factor(), ilneven.Lf[0].factor())
            self.assertEqual(ilnall.Lf[2].factor(), ilneven.Lf[1].factor())
            self.assertEqual(ilnall.Lf[4].factor(), ilneven.Lf[2].factor())
            self.assertEqual(ilnall.Lf[6].factor(), ilneven.Lf[3].factor())
            ilnodd = SPLP(nterms-1, hamiltonian, q, p, function,
                          recursion='chebyshev', sign=sign, parity=1)
            self.assertEqual(ilnall.Lf[1].factor(), ilnodd.Lf[0].factor())
            self.assertEqual(ilnall.Lf[3].factor(), ilnodd.Lf[1].factor())
            self.assertEqual(ilnall.Lf[5].factor(), ilnodd.Lf[2].factor())


class ChebyshevPropagatorTest(unittest.TestCase):
    """ Tests for the Chebyshev polynomial propagator """

    params = {'tstart': 0.0, 'tend': 5.0, 'tstep': 0.2, 'nterms': 8}
    syspar = {'q0': 1.1, 'p0': 0.0}

    def chebyshev_propagator_substep_test(self):
        """ test the sub-stepping option """
        params = self.params.copy()
        params['tstep'] = 0.1
        prop = Chebyshev(syst=Morse(**self.syspar), **params)
        prop.propagate()
        (ti1, sq1, sp1) = prop.get_trajectory()
        params['substep'] = 0.1
        params['tstep'] = 0.2
        prop = Chebyshev(syst=Morse(**self.syspar), **params)
        prop.propagate()
        (ti2, sq2, sp2) = prop.get_trajectory()
        self.assertTrue(np.allclose(ti1, ti2))
        self.assertTrue(np.allclose(sq1, sq2))
        self.assertTrue(np.allclose(sp1, sp2))

    def chebyshev_propagator_fpprec_test(self):
        """ test the fpprec=multi option """
        params = self.params.copy()
        syspar = self.syspar.copy()
        params['fpprec'] = 'multi'
        params['prec'] = 30
        syspar['fpprec'] = 'multi'
        syspar['prec'] = 30
        system = Morse(**syspar)
        prop = Chebyshev(syst=system, **params)
        prop.propagate()
        times, qapr, papr = prop.get_trajectory(dtype=np.object)
        qref, pref = system.solution(times=times)
        npqapr = np.array(qapr, dtype=np.float)
        npqref = np.array(qref, dtype=np.float)
        self.assertTrue(np.allclose(npqapr, npqref))
        nppapr = np.array(papr, dtype=np.float)
        nppref = np.array(pref, dtype=np.float)
        self.assertTrue(np.allclose(nppapr, nppref))

    def chebyshev_propagator_harmonic_test(self):
        """ test with harmonic oscillator """
        syst = Harmonic(**self.syspar)
        prop = Chebyshev(syst=syst, **self.params)
        prop.propagate()
        prop.analyse()
        self.assertLess(np.max(np.fabs(prop.er)), 1e-5)
        (ti, qt, pt) = prop.get_trajectory()
        (qr, pr) = syst.solution(self.syspar['q0'], self.syspar['p0'], ti)
        self.assertTrue(np.allclose(qt, qr, rtol=1e-7))
        self.assertTrue(np.allclose(pt, pr, rtol=1e-7))

    def chebyshev_propagator_ballistic_test(self):
        """ test with ballistic motion """
        syst = Ballistic(**self.syspar)
        prop = Chebyshev(syst=syst, **self.params)
        prop.propagate()
        prop.analyse()
        self.assertLess(np.max(np.fabs(prop.er)), 1e-5)
        (ti, qt, pt) = prop.get_trajectory()
        (qr, pr) = syst.solution(self.syspar['q0'], self.syspar['p0'], ti)
        self.assertTrue(np.allclose(qt, qr, rtol=1e-7))
        self.assertTrue(np.allclose(pt, pr, rtol=1e-7))

    def chebyshev_propagator_morse_test(self):
        """ test with Morse oscillator """
        syst = Morse(**self.syspar)
        prop = Chebyshev(syst=syst, **self.params)
        prop.propagate()
        prop.analyse()
        self.assertLess(np.max(np.fabs(prop.er)), 1e-5)
        (ti, qt, pt) = prop.get_trajectory()
        (qr, pr) = syst.solution(self.syspar['q0'], self.syspar['p0'], ti)
        self.assertTrue(np.allclose(qt, qr, rtol=1e-7))
        self.assertTrue(np.allclose(pt, pr, atol=atol32))

    def chebyshev_propagator_signed_test(self):
        """ test the signed and unsigned variants with harmonic oscillator """
        syst = Harmonic(**self.syspar)
        params = self.params.copy()
        params['signed'] = False
        prop_u = Chebyshev(syst=syst, **params)
        prop_u.propagate()
        params['signed'] = True
        prop_s = Chebyshev(syst=syst, **params)
        prop_s.propagate()

        (ti, qts, pts) = prop_s.get_trajectory()
        (ti, qtu, ptu) = prop_u.get_trajectory()
        (qr, pr) = syst.solution(self.syspar['q0'], self.syspar['p0'], ti)
        self.assertTrue(np.allclose(qtu, qr, rtol=1e-7))
        self.assertTrue(np.allclose(qts, qr, rtol=1e-7))
        self.assertTrue(np.allclose(ptu, pr, rtol=1e-7))
        self.assertTrue(np.allclose(pts, pr, rtol=1e-7))
        self.assertTrue(np.allclose(qtu, qts, rtol=1e-7))
        self.assertTrue(np.allclose(ptu, pts, rtol=1e-7))

    def chebyshev_propagator_nonrecursive_test(self):
        """ test Chebyshev propagator with non-recursive SPLPNR """
        syst = Harmonic(**self.syspar)
        params = self.params.copy()
        params['liouville'] = SPLPNR
        prop_nr = Chebyshev(syst=syst, **params)
        prop_nr.propagate()
        prop_nr.analyse()
        self.assertLess(np.max(np.fabs(prop_nr.er)), 1e-5)
        (ti, qt, pt) = prop_nr.get_trajectory()
        (qr, pr) = syst.solution(self.syspar['q0'], self.syspar['p0'], ti)
        self.assertTrue(np.allclose(qt, qr, rtol=1e-7))
        self.assertTrue(np.allclose(pt, pr, rtol=1e-7))


class TwoStepChebyshevPropagatorTest(unittest.TestCase):
    """ Tests for the two-step Chebyshev polynomial propagator """

    params = {'tstart': 0.0, 'tend': 5.0, 'tstep': 0.2, 'nterms': 7,
              'liouville': SPLPQP}
    syspar = {'q0': 1.1, 'p0': 0.0}

    def chebyshev_propagator_ballistic_and_harmonic_test(self):
        """ test with ballistic motion and harmonic oscillator """
        for potential in (Harmonic, Ballistic):
            sys = potential(**self.syspar)
            t_1 = self.params['tstart'] - self.params['tstep']
            q_1, p_1 = sys.solution(self.syspar['q0'], self.syspar['p0'], t_1)
            prop = TwoStepChebyshev(syst=sys, q_1=q_1, p_1=p_1, **self.params)
            prop.propagate()
            prop.analyse()
            self.assertLess(np.max(np.fabs(prop.er)), 1e-8)
            (ti, qt, pt) = prop.get_trajectory()
            (qr, pr) = sys.solution(self.syspar['q0'], self.syspar['p0'], ti)
            self.assertTrue(np.allclose(qt, qr, rtol=1e-8))
            self.assertTrue(np.allclose(pt, pr, rtol=1e-8))

    def chebyshev_propagator_morse_test(self):
        """ test with Morse oscillator """
        syst = Morse(**self.syspar)
        init_params = self.params.copy()
        init_params['tend'] = self.params['tstart'] + self.params['tstep']
        init_times = [init_params['tstart'], init_params['tend']]
        qinit, pinit = syst.solution(times=init_times)
        syst = Morse(q0=qinit[1], p0=pinit[1])
        params = self.params.copy()
        params['q_1'] = qinit[0]
        params['p_1'] = pinit[0]
        for nterms in (7, 8):
            for signed in (True, False):
                params['nterms'] = nterms
                params['signed'] = signed
                prop = TwoStepChebyshev(syst=syst, **params)
                prop.propagate()
                prop.analyse()
                self.assertLess(np.max(np.fabs(prop.er)), 1e-8)
                ti, qt, pt = prop.get_trajectory()
                qr, pr = syst.solution(times=ti)
                self.assertTrue(np.allclose(qt, qr, rtol=1e-8))
                self.assertTrue(np.allclose(pt, pr, atol=atol32))

    def chebyshev_propagator_lj_test(self):
        """ test with a particle in Lennard-Jones potential """
        syspar = {'p0': -1.0, 'q0': 2}
        params = {'tstart': 0.0, 'tend': 1.8, 'tstep': 0.02, 'nterms': 5}
        syst = LennardJones(**syspar)
        ref_prop = Chebyshev(syst=syst, **params)
        ref_prop.propagate()
        time_ref, q_ref, p_ref = ref_prop.get_trajectory()
        syst = LennardJones(q0=q_ref[1], p0=p_ref[1])
        params['q_1'] = q_ref[0]
        params['p_1'] = p_ref[0]
        params['tstart'] = params['tstart'] + params['tstep']
        params['liouville'] = SPLPQP
        for nterms in (5, 6):
            for signed in (False, True):
                params['nterms'] = nterms
                params['signed'] = signed
                prop = TwoStepChebyshev(syst=syst, **params)
                prop.propagate()
                prop.analyse()
                self.assertLess(np.max(np.fabs(prop.er)), 1e-3)
                time, qt, pt = prop.get_trajectory()
                assert len(time) == len(time_ref[1:])
                self.assertTrue(np.allclose(qt, q_ref[1:], rtol=1e-3))
                self.assertTrue(np.allclose(pt, p_ref[1:], rtol=1e-2))


class PropagatorRestartTest(unittest.TestCase):
    """ Tests with restarting polynomial propagators """

    params = {'tstart': 0.0, 'tend': 5.0, 'tstep': 0.2, 'nterms': 8}
    syspar = {'q0': 1.1, 'p0': 0.0}

    def setUp(self):
        import uuid
        self.testfile = str(uuid.uuid4())

    def chebyshev_propagator_restart_test(self):
        """ test the propagator restart option with Chebyshev propagator """
        system = Morse(**self.syspar)
        prop1 = Chebyshev(syst=system, **self.params)
        prop1.dump(self.testfile)
        prop1.propagate()
        (ti1, sq1, sp1) = prop1.get_trajectory()
        self.assertRaises(AssertionError, Newtonian, syst=system,
                          restart_file=self.testfile, restart=True,
                          **self.params)
        prop2 = Chebyshev(syst=system, restart_file=self.testfile,
                          restart=True, **self.params)
        prop2.propagate()
        (ti2, sq2, sp2) = prop2.get_trajectory()
        self.assertTrue(np.allclose(ti1, ti2))
        self.assertTrue(np.allclose(sq1, sq2))
        self.assertTrue(np.allclose(sp1, sp2))

    def tearDown(self):
        import os
        os.remove(self.testfile)


class PointsTest(unittest.TestCase):
    """ Tests of interpolation points construction methods """

    def chebyshev_points_exponent_test(self):
        """ Chebyshev points interpolation of exponential function """
        vals = np.arange(-1, 1, 0.1)
        func = lambda x: 2*np.exp(2*x)
        lambdas = chebyshev_points(20)
        pol = newton_scalar(lambdas, func, vals)
        self.assertTrue(np.allclose(pol, func(vals), rtol=1e-8))

    def chebyshev_points_exponent_test_affine(self):
        """ Chebyshev points interp. of exp. function with shift and scale """
        (left, right) = (-2, 5)
        vals = np.arange(left, right, 0.1)
        func = lambda x: 2*np.exp(2*x)
        lambdas = chebyshev_points(20)
        pol = newton_scalar(lambdas, func, vals, scale=(right-left)/2,
                            shift=(left+right)/2)
        self.assertTrue(np.allclose(pol, func(vals), rtol=1e-4))

    def chebyshev_points_runge_test(self):
        """ Chebyshev points interpolation of Runge function """
        (left, right) = (-0.8, 0.9)
        vals = np.arange(left, right, 0.1)
        func = lambda x: 1./(1 + 25*x*x)
        lambdas = chebyshev_points(60)
        pol = newton_scalar(lambdas, func, vals)
        self.assertTrue(np.allclose(pol, func(vals), rtol=1e-3))

    def leja_points_exponent_test(self):
        """ Leja points interpolation of exponential function """
        vals = np.arange(-1, 1, 0.1)
        func = lambda x: 2*np.exp(3*x)
        lambdas, scaling = leja_points_ga(15)
        pol = newton_scalar(lambdas, func, vals, scale=scaling)
        self.assertTrue(np.allclose(pol, func(vals), rtol=1e-8))
        lambdas, scaling = leja_points_jb(15)
        pol = newton_scalar(lambdas, func, vals, scale=scaling)
        self.assertTrue(np.allclose(pol, func(vals), rtol=1e-8))

    def leja_points_runge_test(self):
        """ Leja points interpolation of Runge function """
        vals = np.arange(-1, 1, 0.1)
        func = lambda x: 1./(1 + 25*x*x)
        lambdas, scaling = leja_points_ga(60)
        pol = newton_scalar(lambdas, func, vals, scale=scaling)
        self.assertTrue(np.allclose(pol, func(vals), rtol=1e-4))
        lambdas, scaling = leja_points_jb(60)
        pol = newton_scalar(lambdas, func, vals, scale=scaling)
        self.assertTrue(np.allclose(pol, func(vals), rtol=1e-4))

    def leja_points_runge_test_affine(self):
        """ Leja points interpolation of Runge funct. with scale and shift """
        (left, right) = (-2, 1)
        vals = np.arange(left, right, 0.1)
        func = lambda x: 1./(1 + 25*x*x)
        lambdas, scaling = leja_points_ga(60)
        pol = newton_scalar(lambdas, func, vals, scale=scaling*(right-left)/2,
                            shift=(left+right)/2)
        self.assertTrue(np.allclose(pol, func(vals), atol=1e-3))
        lambdas_jb, scaling = leja_points_jb(60)
        pol = newton_scalar(lambdas_jb, func, vals, scale=(right-left)/2,
                            shift=(left+right)/2)
        self.assertTrue(np.allclose(pol, func(vals), atol=1e-3))


class DividedDifferenceTest(unittest.TestCase):
    """ Tests for finite difference functions """

    def divided_difference_test(self):
        """ compare two algorithms for divided differences """
        from rotagaporp.newtonian import divdiff_1, divdiff_2
        nodes = np.arange(-1, 1, 0.1)
        funcvals = lambda x: 2*np.exp(3*x)
        diffs1 = divdiff_1(nodes, funcvals(nodes))
        diffs2 = divdiff_2(nodes, funcvals(nodes))
        self.assertTrue(np.allclose(diffs1, diffs2))


class NewtonianPropagatorTest(unittest.TestCase):
    """ Tests for the Newtonian polynomial propagator """

    params = {'tstart': 0.0, 'tend': 5.0, 'tstep': 0.2, 'nterms': 8,
              'delta': 2.0}
    syspar = {'q0': 1.1, 'p0': 0.0}

    def newtonian_propagator_substep_test(self):
        """ test the sub-stepping option """
        params = self.params.copy()
        params['tstep'] = 0.1
        prop = Newtonian(syst=Morse(**self.syspar), **params)
        prop.propagate()
        (ti1, sq1, sp1) = prop.get_trajectory()
        params['substep'] = 0.1
        params['tstep'] = 0.2
        prop = Newtonian(syst=Morse(**self.syspar), **params)
        prop.propagate()
        (ti2, sq2, sp2) = prop.get_trajectory()
        self.assertTrue(np.allclose(ti1, ti2))
        self.assertTrue(np.allclose(sq1, sq2))
        self.assertTrue(np.allclose(sp1, sp2))

    def newtonian_propagator_fpprec_test(self):
        """ test the fpprec=multi option """
        params = self.params.copy()
        syspar = self.syspar.copy()
        params['fpprec'] = 'multi'
        params['prec'] = 30
        syspar['fpprec'] = 'multi'
        syspar['prec'] = 30
        system = Morse(**syspar)
        prop = Newtonian(syst=system, **params)
        prop.propagate()
        times, qapr, papr = prop.get_trajectory(dtype=np.object)
        qref, pref = system.solution(times=times)
        npqapr = np.array(qapr, dtype=np.float)
        npqref = np.array(qref, dtype=np.float)
        self.assertTrue(np.allclose(npqapr, npqref))
        nppapr = np.array(papr, dtype=np.float)
        nppref = np.array(pref, dtype=np.float)
        self.assertTrue(np.allclose(nppapr, nppref))

    def newtonian_propagator_harmonic_test(self):
        """ test with harmonic oscillator """
        syst = Harmonic(**self.syspar)
        prop = Newtonian(syst=syst, **self.params)
        prop.propagate()
        prop.analyse()
        self.assertLess(np.max(np.fabs(prop.er)), 1e-5)
        (ti, qt, pt) = prop.get_trajectory()
        (qr, pr) = syst.solution(self.syspar['q0'], self.syspar['p0'], ti)
        self.assertTrue(np.allclose(qt, qr, rtol=1e-7))
        self.assertTrue(np.allclose(pt, pr, rtol=1e-7))

    def newtonian_propagator_morse_test(self):
        """ test with Morse oscillator """
        syst = Morse(**self.syspar)
        prop = Newtonian(syst=syst, **self.params)
        prop.propagate()
        prop.analyse()
        self.assertLess(np.max(np.fabs(prop.er)), 1e-5)
        (ti, qt, pt) = prop.get_trajectory()
        qr, pr = syst.solution(self.syspar['q0'], self.syspar['p0'], ti)
        self.assertTrue(np.allclose(qt, qr, rtol=1e-7))
        self.assertTrue(np.allclose(pt, pr, atol=atol32))


class SymplecticPropagatorTest(unittest.TestCase):
    """ Tests for the symplectic (velocity Verlet) propagator """

    params = {'tstart': 0.0, 'tend': 5.0, 'tstep': 0.01}
    syspar = {'q0': 1.1, 'p0': 0.0}

    def symplectic_propagator_harmonic_test(self):
        """ test with harmonic oscillator """
        syst = Harmonic(**self.syspar)
        prop = Symplectic(syst=syst, **self.params)
        prop.propagate()
        prop.analyse()
        self.assertLess(np.max(np.fabs(prop.er)), 1e-4)
        (ti, qt, pt) = prop.get_trajectory()
        (qr, pr) = syst.solution(self.syspar['q0'], self.syspar['p0'], ti)
        self.assertTrue(np.allclose(qt, qr, rtol=1e-2))
        self.assertTrue(np.allclose(pt, pr, rtol=1e-2))

    def symplectic_propagator_ballistic_test(self):
        """ test with ballistic system """
        syst = Ballistic(**self.syspar)
        prop = Symplectic(syst=syst, **self.params)
        prop.propagate()
        prop.analyse()
        self.assertLess(np.max(np.fabs(prop.er)), 1e-4)
        (ti, qt, pt) = prop.get_trajectory()
        (qr, pr) = syst.solution(self.syspar['q0'], self.syspar['p0'], ti)
        self.assertTrue(np.allclose(qt, qr, rtol=1e-2))
        self.assertTrue(np.allclose(pt, pr, rtol=1e-2))

    def symplectic_propagator_morse_test(self):
        """ test with Morse oscillator """
        syst = Morse(**self.syspar)
        params = self.params.copy()
        params['tstep'] /= 10
        params['tend'] /= 10
        prop = Symplectic(syst=syst, **params)
        prop.propagate()
        prop.analyse()
        self.assertLess(np.max(np.fabs(prop.er)), 1e-4)
        (ti, qt, pt) = prop.get_trajectory()
        qr, pr = syst.solution(self.syspar['q0'], self.syspar['p0'], ti)
        self.assertTrue(np.allclose(qt, qr, rtol=1e-2))
        self.assertTrue(np.allclose(pt, pr, atol=atol32))


class ChebyshevAssessmentTest(unittest.TestCase):
    """ Compare Chebyshev to velocity Verlet and analytical solution """

    def chebyshev_scalar_test(self):
        """ chebyshev expansion of the scalar function exp(z*t) """
        from rotagaporp.chebyshev import chebyshev_scalar
        from itertools import product

        sample_length = 10
        lambdas = (np.random.rand((sample_length))-0.5)*6*np.random.rand()
        pars = [(False, True), (1., 1.j), ('fixed', 'multi')]
        for sign, fact, fpprec in product(*pars):
            values = lambdas*fact
            apr = chebyshev_scalar(values, 1.1, nterms=24, signed=sign,
                                   fpprec=fpprec)
            ref = np.exp(values*1.1)
            self.assertTrue(np.allclose(np.array(apr, dtype=ref.dtype), ref))

    def chebyshev_symplectic_1p_test(self):
        """ with one-particle system: anharmonic oscillator """
        params = {
            'tstart': 0.0,
            'tend': 5.0,
            'tstep': 0.2,
            'delta': 2.0,
            'nterms': 12,
            'substep': 0.01
        }
        syspar = {
            'q0': 1.1,
            'p0': 0.0
        }
        prop1 = Chebyshev(syst=Anharmonic(**syspar), **params)
        prop1.propagate()
        (ti1, sq1, sp1) = prop1.get_trajectory()

        params = {
            'tstart': 0.0,
            'tend': 5.0,
            'tstep': 0.01
        }
        prop2 = Symplectic(syst=Anharmonic(**syspar), **params)
        prop2.propagate()
        (ti2, sq2, sp2) = prop2.get_trajectory()

        self.assertTrue(np.allclose(ti1, ti2))
        self.assertTrue(np.allclose(sq1, sq2, rtol=1e-4))
        self.assertLess(np.max(np.fabs(sp1-sp2)), 1e-4)

    def chebyshev_symplectic_2p_ho_test(self):
        """ two particles in harmonic potential """

        M = 2*[2]
        qsymbs = ['q'+str(index) for index in list(range(len(M)))]
        Q = sp.Array(sp.symbols(' '.join(qsymbs), real=True))
        psymbs = ['p'+str(index) for index in list(range(len(M)))]
        P = sp.Array(sp.symbols(' '.join(psymbs), real=True))
        r0 = 1.
        kappa = sp.Rational(1, 2)
        ff = FFHarmonic(([Q[0]], [Q[1]]), r0, kappa)
        syspar = {'q0': [0, 2], 'p0': [0, 0], 'mass': M}
        syst = ManyParticlePhysicalSystem(ff, Q, P, **syspar)

        # solution with Chebyshev expansion
        params = {'tstart': 0.0, 'tend': 4.0, 'tstep': 0.05, 'nterms': 4,
                  'liouville': MPLPQP}
        prop = Chebyshev(syst=syst, **params)
        prop.propagate()
        prop.analyse()
        (ti, qt, pt) = prop.get_trajectory()

        # reference numerical solution
        params = {'tstart': 0.0, 'tend': 4.0, 'tstep': 0.05}
        prop_ref = Symplectic(syst=syst, **params)
        prop_ref.propagate()
        prop_ref.analyse()
        (tir, qtr, ptr) = prop_ref.get_trajectory()

        # reference analytical solution
        sys_1p_par = {
            'q0': np.abs(syspar['q0'][0]-syspar['q0'][1]) - r0,
            'p0': np.sqrt((syspar['p0'][0]**2*M[0] +
                           syspar['p0'][1]**2*M[1])/np.sum(M))
        }
        sys_1p = Harmonic(**sys_1p_par)
        qrta, prta = sys_1p.solution(sys_1p_par['q0'], sys_1p_par['p0'], tir)
        enea = np.full(len(ti), sys_1p.energy(sys_1p_par['q0'], sys_1p_par['p0']))

        # 2p -> 1p reduced chebyshev solution
        qrtr = np.abs(qt[:, 0]-qt[:, 1]) - r0
        prtr = (pt[:, 1]*M[0]-pt[:, 0]*M[1])/np.sum(M)

        self.assertTrue(np.allclose(ti, tir))
        self.assertTrue(np.allclose(qrtr, qrta, atol=1e-5))
        self.assertTrue(np.allclose(prtr, prta, atol=1e-5))
        self.assertTrue(np.allclose(prop.en, enea, atol=1e-4))
        self.assertTrue(np.allclose(qt, qtr, atol=1e-3))
        self.assertTrue(np.allclose(pt, ptr, atol=1e-3))
        self.assertLess(np.max(np.fabs(prop.er)), 1e-3)

    def chebyshev_symplectic_2p_lj_test(self):
        """ two particles in Lennard-Jones potential """

        M = np.full(2, 1.0)
        qsymbs = ['q'+str(index) for index in list(range(len(M)))]
        Q = sp.Array(sp.symbols(' '.join(qsymbs), real=True))
        psymbs = ['p'+str(index) for index in list(range(len(M)))]
        P = sp.Array(sp.symbols(' '.join(psymbs), real=True))

        ff = FFLennardJones(([Q[0]], [Q[1]]))
        syspar = {'q0': [0, 2], 'p0': [0, -1], 'mass': M}
        syst = ManyParticlePhysicalSystem(ff, Q, P, **syspar)
        params = {'tstart': 0.0, 'tend': 0.8, 'tstep': 0.01, 'nterms': 4,
                  'liouville': MPLPQP}
        prop = Chebyshev(syst=syst, **params)
        prop.propagate()
        prop.analyse()
        (ti, qt, pt) = prop.get_trajectory()
        enea = np.full(len(ti), syst.energy(syspar['q0'], syspar['p0']))

        params = {'tstart': 0.0, 'tend': 0.8, 'tstep': 0.01}
        prop_ref = Symplectic(syst=syst, **params)
        prop_ref.propagate()
        prop_ref.analyse()
        (tir, qtr, ptr) = prop_ref.get_trajectory()

        self.assertTrue(np.allclose(ti, tir))
        self.assertTrue(np.allclose(qt, qtr, atol=1e-3))
        self.assertTrue(np.allclose(pt, ptr, atol=1e-2))
        self.assertTrue(np.allclose(prop.en, enea, atol=1e-2))
        self.assertLess(np.max(np.fabs(prop.er)), 1e-2)


class MorseTest(unittest.TestCase):
    """ Test of Morse oscillator """

    def morse_test_solution_initial_val(self):
        """ compare initial values to analytical solution """
        initial_conditions = [
            [1.0, 0.0, [0]],
            [1.0, 0.5, [0]],
            [1.0, -0.5, [0]],
            [1.0, 3.0, [0]],
            [1.0, -3.0, [0]],
            [0.1, 0.0, [0]],
            [0.1, 0.3, [0]],
            [0.1, -0.3, [0]],
            [0.3068528194400546, 0.0, [0]],
            [0.3068528194400546, 0.3, [0]],
            [0.3068528194400546, -0.3, [0]],
            [1.5, 0.0, [0]],
            [1.5, 0.5, [0]],
            [1.5, -0.5, [0]],
            [0.4, 0.0, [0]],
            [0.4, 0.5, [0]],
            [0.4, -0.5, [0]],
            [2.5, 0.0, [0]],
            [2.5, 0.5, [0]],
            [2.5, -0.5, [0]],
            [6.0, 0.0, [0]],
            [6.0, 1.0, [0]],
            [6.0, -1.0, [0]],
        ]
        morse = Morse()
        for ics in initial_conditions:
            (qt, pt) = morse.solution(*ics)
            self.assertAlmostEqual(qt[0], ics[0], places=6)
            self.assertAlmostEqual(pt[0], ics[1], places=6)


class ForceFieldTest(unittest.TestCase):
    """ Test for pair-interaction force field classes """

    def pair_ff_2p_1d_test(self):
        """ One-dimensional two-particle potentials """
        Q = [sp.Array((sp.Symbol('q0', real=True), )),
             sp.Array((sp.Symbol('q1', real=True), ))]
        subs = [(Q[0][0], 1.), (Q[1][0], 1.)]

        self.assertRaises(AssertionError, PairFF, [Q[0], Q[0]])
        ff = PairFF(Q)
        self.assertAlmostEqual(ff.r_cart.subs(subs), 0.)

        hf = FFHarmonic(Q, 1., sp.Rational(1, 2))
        self.assertAlmostEqual(hf.r_cart.subs(subs), 0.)
        self.assertAlmostEqual(hf.pot_cart.subs(subs), 0.5)
        self.assertAlmostEqual(hf.pot_inte.subs(hf.r, 0.), 0.5)

        subs = [(Q[0][0], 0.), (Q[1][0], 1.)]

        mf = FFMorse(Q, 1.)
        self.assertAlmostEqual(mf.pot_cart.subs(subs), -1.)
        self.assertAlmostEqual(mf.pot_inte.subs(mf.r, 1.), -1.)

        lj = FFLennardJones(Q)
        self.assertAlmostEqual(lj.pot_cart.subs(subs), 0.)
        self.assertAlmostEqual(lj.pot_inte.subs(lj.r, 1.), 0.)

        cf = FFCoulomb(Q, [1., -1.])
        self.assertAlmostEqual(cf.pot_cart.subs(subs), -1.)
        self.assertAlmostEqual(cf.pot_inte.subs(cf.r, 1.), -1.)

    def pair_ff_2p_3d_test(self):
        """ Three-dimensional two-particle potentials """
        from itertools import chain
        qsym = [sp.Array(sp.symbols('x0 y0 z0', real=True)),
                sp.Array(sp.symbols('x1 y1 z1', real=True))]
        ff = PairFF(qsym)
        qval = [[1, 0, 1], [0, 1, 0]]
        subs = list(chain(*[list(zip(qs, qv)) for qs, qv in zip(qsym, qval)]))
        self.assertEqual(ff.r_cart.subs(subs), sp.sqrt(3))


class PBCTest(unittest.TestCase):
    """ Test functions for treating periodic boundary conditions """

    def test_position_small(self):
        """ wrap positions with short-range shifts, small data set """
        cell = np.array([3.0, 4.0, 5.0])
        posvec = np.array([[3.5, 2.0, 2.5], [0.0, -1.0, 4.0]])
        res = wrap(posvec, cell)
        ref = np.array([[0.5, 2.0, 2.5], [0.0, 3.0, 4.0]])
        self.assertTrue(np.allclose(res, ref))

    def test_position_far(self):
        """ wrap positions with many random long-range shifts """
        cell = np.array([1.0, 2.0, 3.0])
        posvec = np.array([[0.5, 0.5, 0.5], [0.7, 0.7, 0.7]])
        res1 = wrap(posvec, cell)
        posvec += np.random.randint(-10, 10, (2, 3))*cell
        res2 = wrap(posvec, cell)
        self.assertTrue(np.allclose(res1, res2))

    def test_distance_small(self):
        """ test mic distances for a small data set """
        cell = np.array([3.0, 4.0, 5.0])
        posvec = np.array([[1.5, 2.0, 2.5], [2.7, 1.5, 4.3], [1.2, 0.3, 4.2]])
        res1 = np.linalg.norm(dists1(posvec, cell), axis=2)
        res2 = np.linalg.norm(dists2(posvec, cell), axis=2)
        self.assertAlmostEqual(res1[0, 1], 2.22036033)
        self.assertAlmostEqual(res1[0, 2], 2.42280829)
        self.assertAlmostEqual(res1[1, 2], 1.92353841)
        self.assertTrue(np.allclose(res1, res2))

    def test_distance_big(self):
        """ test mic distances, big set of random positions, small offsets """
        cell = np.random.rand(3)
        posvec = np.random.rand(100, 3)
        ful1 = dists1(posvec, cell)
        ful2 = dists2(posvec, cell)
        self.assertTrue(np.allclose(ful1, ful2))
        res1 = np.linalg.norm(ful1, axis=2)
        res2 = np.linalg.norm(ful2, axis=2)
        self.assertTrue(np.allclose(res1, res2))

    def test_distance_far(self):
        """ test mic distances, big set of random positions, large offsets """
        cells = [np.array([1.0, 1.1, 1.2]), np.array([1.0, 2.0, 3.0])]
        posvecs = [np.array([[0.8, 0.8, 0.8], [0.1, 0.1, 0.1]]),
                   np.array([[0.5, 0.5, 0.5], [0.7, 0.7, 0.7]])]
        refs = [np.array([[[0.0, 0.0, 0.0], [0.3, 0.4, 0.5]],
                          [[-0.3, -0.4, -0.5], [0.0, 0.0, 0.0]]]),
                np.array([[[0.0, 0.0, 0.0], [0.2, 0.2, 0.2]],
                          [[-0.2, -0.2, -0.2], [0.0, 0.0, 0.0]]])]
        for cell, posvec, ref in zip(cells, posvecs, refs):
            res11 = dists1(posvec, cell)
            res21 = dists2(posvec, cell)
            self.assertTrue(np.allclose(res11, ref))
            self.assertTrue(np.allclose(res21, ref))
            posvec += np.random.randint(-10, 10, (2, 3))*cell
            res12 = dists1(posvec, cell)
            res22 = dists2(posvec, cell)
            self.assertTrue(np.allclose(res11, res22))
            self.assertTrue(np.allclose(res11, res12))
            self.assertTrue(np.allclose(res21, res22))


class CombinatoricsTest(unittest.TestCase):
    """ Test auxiliary functions for generating recursion coefficients """

    ref_N = [[1, 0, 0, 0, 0], [1, -1, 0, 0, 0], [1, -3, 2, 0, 0],
             [1, -6, 11, -6, 0], [1, -10, 35, -50, 24]]

    # source: http://oeis.org/A053120/list
    ref_T = np.array(
        [1, 0, 1, -1, 0, 2, 0, -3, 0, 4, 1, 0, -8, 0, 8, 0, 5, 0, -20, 0, 16,
         -1, 0, 18, 0, -48, 0, 32, 0, -7, 0, 56, 0, -112, 0, 64, 1, 0, -32,
         0, 160, 0, -256, 0, 128, 0, 9, 0, -120, 0, 432, 0, -576, 0, 256,
         -1, 0, 50, 0, -400, 0, 1120, 0, -1280, 0, 512, 0, -11, 0, 220, 0,
         -1232, 0, 2816, 0, -2816]
    )

    # source: https://oeis.org/A053117/list
    ref_U = np.array(
        [1, 0, 2, -1, 0, 4, 0, -4, 0, 8, 1, 0, -12, 0, 16, 0, 6, 0, -32, 0,
         32, -1, 0, 24, 0, -80, 0, 64, 0, -8, 0, 80, 0, -192, 0, 128, 1, 0,
         -40, 0, 240, 0, -448, 0, 256, 0, 10, 0, -160, 0, 672, 0, -1024,
         0, 512, -1, 0, 60, 0, -560, 0, 1792, 0, -2304, 0, 1024, 0, -12,
         0, 280, 0, -1792, 0, 4608, 0, -5120, 0, 2048, 1, 0, -84, 0,
         1120, 0, -5376, 0, 11520]
    )

    def test_newton_cnk_int(self):
        """ test Newton expansion coefficients for integer interpolation
            nodes """
        from rotagaporp.combinatorics import cnk_newton_1, cnk_newton_2
        lambdas = [1, 2, 3, 4]
        cnk1 = cnk_newton_1(lambdas, 4, dtype=int).tolist()
        cnk2 = cnk_newton_2(lambdas, 4, dtype=int).tolist()
        self.assertListEqual(cnk1, cnk2)
        self.assertListEqual(cnk1, self.ref_N)

    def test_newton_cnk_float(self):
        """ test Newton expansion coefficients for real interpolation
            nodes """
        from rotagaporp.combinatorics import cnk_newton_1, cnk_newton_2
        lambdas = np.random.rand(8)
        cnk1 = cnk_newton_1(lambdas, 6, dtype=float)
        cnk2 = cnk_newton_2(lambdas, 6, dtype=float)
        self.assertTrue(np.allclose(cnk1, cnk2))

    def test_newton_cnk_complex(self):
        """ test Newton expansion coefficients for complex interpolation
            nodes """
        from rotagaporp.combinatorics import cnk_newton_1, cnk_newton_2
        reals = np.random.rand(8)
        imags = np.random.rand(8)
        lambdas = reals + 1j * imags
        cnk1 = cnk_newton_1(lambdas, 6, dtype=complex)
        cnk2 = cnk_newton_2(lambdas, 6, dtype=complex)
        self.assertTrue(np.allclose(cnk1, cnk2))

    def test_chebyshev_recursion_short(self):
        """ test Chebyshev expansion coefficients C^(n)_k by comparing to
            reference number sequences """

        kinds = [(self.ref_T, cnk_chebyshev_T1),
                 (self.ref_U, cnk_chebyshev_U1)]
        for ref, fun in kinds:
            for signed in (True, False):
                ref_s = ref if signed else np.abs(ref)
                seq = []
                for n in range(0, 12):
                    for k in range(0, n+1):
                        seq.append(fun(n, k, signed))
                self.assertListEqual(seq[:76], ref_s.tolist()[:76])

    def test_chebyshev_recursion_long(self):
        """ test Chebyshev expansion coefficients C^(n)_k by comparing
            different algorithms """

        for signed in (True, False):
            for n in range(0, 26):
                for k in range(0, n+1):
                    message = 'n, k: ' + str(n) + ' ' + str(k)
                    tcnk1 = cnk_chebyshev_T1(n, k, signed)
                    tcnk2 = cnk_chebyshev_T2(n, k, signed)
                    tcnk3 = cnk_chebyshev_T3(n, k, signed)
                    self.assertEqual(tcnk1, tcnk2, msg=message)
                    self.assertEqual(tcnk1, tcnk3, msg=message)
                    ucnk1 = cnk_chebyshev_U1(n, k, signed)
                    ucnk2 = cnk_chebyshev_U2(n, k, signed)
                    ucnk3 = cnk_chebyshev_U3(n, k, signed)
                    self.assertEqual(ucnk1, ucnk2, msg=message)
                    self.assertEqual(ucnk1, ucnk3, msg=message)

class GenericFunctionsTests(unittest.TestCase):
    """ Tests for generic functions """

    def lambdify_long_test(self):
        """ test the lambdify wrapper used for more than 255 arguments """
        from rotagaporp.generic import lambdify_long
        import sympy as sp

        dim = 64
        symbs = [str(i)+'x:z' for i in range(dim)]
        arr = sp.Array(sp.symbols(' '.join(['q'+s for s in symbs]), real=True))
        lfunc = lambdify_long(arr, sum(arr))
        ind = sp.Symbol('i', integer=True, positive=True)
        ref = sp.Sum(ind, (ind, 0, 3*dim-1)).doit()
        self.assertEqual(lfunc(range(3*dim)), ref)
        lfunc = lambdify_long(arr, (i*sum(arr) for i in range(2)))
        self.assertEqual(lfunc(range(3*dim)), [0, ref])
        lfunc = lambdify_long(arr, tuple(i*sum(arr) for i in range(2)))
        self.assertEqual(lfunc(range(3*dim)), [0, ref])
