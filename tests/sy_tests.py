""" Tests for the self-starting symmetric polynomial propagator """
import unittest
import numpy as np
from rotagaporp.generic import spfloat
from rotagaporp.chebyshev import SymmetricChebyshev
from rotagaporp.newtonian import SymmetricNewtonian
from rotagaporp.systems import Morse, MPPSMD
from rotagaporp.mpliouville import MPLPQP

class TestSymmetricPolynomialPropagator(unittest.TestCase):
    """ Test the self-starting symmetric polynomial propagator """

    def compare(self, prop, params, syst, atol):
        """ compare trajectories with analytic solution """
        sy = prop(syst=syst, **params)
        sy.propagate()
        sy.analyse()
        (tsy, qsy, psy) = sy.get_trajectory()
        qan, pan = syst.solution(times=tsy)
        qanfl = np.array(qan, dtype=float)
        panfl = np.array(pan, dtype=float)
        self.assertTrue(np.allclose(qsy, qanfl, atol=atol))
        self.assertTrue(np.allclose(psy, panfl, atol=atol))
        self.assertTrue(np.allclose(sy.er, 0, atol=atol))

    def test_cheb_subs_fixed(self):
        """ Chebyshev with fixed precision / subs """
        numeric = {'tool': 'subs'}
        params = {'tstart': 0., 'tend': 20., 'tstep': 0.2, 'nterms': 6,
                  'signed': False, 'numeric': numeric, 'fpprec': 'fixed'}
        syspar = {'q0': 3., 'p0': 0., 'fpprec': 'fixed'}
        system = Morse(**syspar)
        self.compare(SymmetricChebyshev, params, system, 1e-2)

    def test_newt_subs_fixed(self):
        """ Newtonian with fixed precision / subs """
        numeric = {'tool': 'subs'}
        params = {'tstart': 0., 'tend': 20., 'tstep': 0.2, 'nterms': 6,
                  'numeric': numeric, 'fpprec': 'fixed'}
        syspar = {'q0': 3., 'p0': 0., 'fpprec': 'fixed'}
        system = Morse(**syspar)
        self.compare(SymmetricNewtonian, params, system, 1e-2)

    def test_cheb_lambdify_fixed(self):
        """ Chebyshev with fixed precision / lambdify """
        numeric = {'tool': 'lambdify', 'modules': 'numpy'}
        params = {'tstart': 0., 'tend': 20., 'tstep': 0.2, 'nterms': 6,
                  'signed': False, 'numeric': numeric, 'fpprec': 'fixed'}
        syspar = {'q0': 3., 'p0': 0., 'fpprec': 'fixed'}
        system = Morse(**syspar)
        self.compare(SymmetricChebyshev, params, system, 1e-2)

    def test_newt_lambdify_fixed(self):
        """ Newtonain with fixed precision / lambdify """
        numeric = {'tool': 'lambdify', 'modules': 'numpy'}
        params = {'tstart': 0., 'tend': 20., 'tstep': 0.2, 'nterms': 6,
                  'numeric': numeric, 'fpprec': 'fixed'}
        syspar = {'q0': 3., 'p0': 0., 'fpprec': 'fixed'}
        system = Morse(**syspar)
        self.compare(SymmetricNewtonian, params, system, 1e-2)

    def test_cheb_theano_fixed(self):
        """ Chebyshev with fixed precision / theano """
        numeric = {'tool': 'theano'}
        params = {'tstart': 0., 'tend': 20., 'tstep': 0.2, 'nterms': 6,
                  'signed': False, 'numeric': numeric, 'fpprec': 'fixed'}
        syspar = {'q0': 3., 'p0': 0., 'fpprec': 'fixed'}
        system = Morse(**syspar)
        self.compare(SymmetricChebyshev, params, system, 1e-2)

    def test_newt_theano_fixed(self):
        """ Newtonian with fixed precision / theano """
        numeric = {'tool': 'theano'}
        params = {'tstart': 0., 'tend': 20., 'tstep': 0.2, 'nterms': 6,
                  'numeric': numeric, 'fpprec': 'fixed'}
        syspar = {'q0': 3., 'p0': 0., 'fpprec': 'fixed'}
        system = Morse(**syspar)
        self.compare(SymmetricNewtonian, params, system, 1e-2)

    def test_cheb_subs_prec15(self):
        """ Chebyshev with multi precision 15 decimal digits / subs """
        prec = 15
        numeric = {'tool': 'subs'}
        params = {'tstart': spfloat(0., prec), 'tend': spfloat(20., prec),
                  'tstep': spfloat(0.2, prec), 'nterms': 6, 'signed': False,
                  'numeric': numeric, 'fpprec': 'multi', 'prec': prec}
        syspar = {'q0': spfloat(3., prec), 'p0': spfloat(0., prec),
                  'fpprec': 'multi', 'prec': prec}
        system = Morse(**syspar)
        self.compare(SymmetricChebyshev, params, system, 1e-2)

    def test_newt_subs_prec15(self):
        """ Newtonian with multi precision 15 decimal digits / subs """
        prec = 15
        numeric = {'tool': 'subs'}
        params = {'tstart': spfloat(0., prec), 'tend': spfloat(20., prec),
                  'tstep': spfloat(0.2, prec), 'nterms': 6,
                  'numeric': numeric, 'fpprec': 'multi', 'prec': prec}
        syspar = {'q0': spfloat(3., prec), 'p0': spfloat(0., prec),
                  'fpprec': 'multi', 'prec': prec}
        system = Morse(**syspar)
        self.compare(SymmetricNewtonian, params, system, 1e-2)

    def test_cheb_lambdify_prec08(self):
        """ Chebyshev with multi precision 8 decimal digits / lambdify """
        prec = 8
        numeric = {'tool': 'lambdify', 'modules': 'mpmath'}
        params = {'tstart': spfloat(0., prec), 'tend': spfloat(20., prec),
                  'tstep': spfloat(0.2, prec), 'nterms': 6, 'signed': False,
                  'numeric': numeric, 'fpprec': 'multi', 'prec': prec}
        syspar = {'q0': spfloat(3., prec), 'p0': spfloat(0., prec),
                  'fpprec': 'multi', 'prec': prec}
        system = Morse(**syspar)
        self.compare(SymmetricChebyshev, params, system, 1e-2)

    def test_newt_lambdify_prec08(self):
        """ Newtonian with multi precision 8 decimal digits / lambdify """
        prec = 8
        numeric = {'tool': 'lambdify', 'modules': 'mpmath'}
        params = {'tstart': spfloat(0., prec), 'tend': spfloat(20., prec),
                  'tstep': spfloat(0.2, prec), 'nterms': 6,
                  'numeric': numeric, 'fpprec': 'multi', 'prec': prec}
        syspar = {'q0': spfloat(3., prec), 'p0': spfloat(0., prec),
                  'fpprec': 'multi', 'prec': prec}
        system = Morse(**syspar)
        self.compare(SymmetricNewtonian, params, system, 1e-2)

    def test_cheb_lambdify_prec30(self):
        """ Chebyshev with multi precision 30 decimal digits / lambdify """
        prec = 30
        numeric = {'tool': 'lambdify', 'modules': 'mpmath'}
        params = {'tstart': spfloat(0., prec), 'tend': spfloat(20., prec),
                  'tstep': spfloat(0.2, prec), 'nterms': 6, 'signed': False,
                  'numeric': numeric, 'fpprec': 'multi', 'prec': prec}
        syspar = {'q0': spfloat(3., prec), 'p0': spfloat(0., prec),
                  'fpprec': 'multi', 'prec': prec}
        system = Morse(**syspar)
        self.compare(SymmetricChebyshev, params, system, 1e-2)

    def test_newt_lambdify_prec30(self):
        """ Chebyshev with multi precision 30 decimal digits / lambdify """
        prec = 30
        numeric = {'tool': 'lambdify', 'modules': 'mpmath'}
        params = {'tstart': spfloat(0., prec), 'tend': spfloat(20., prec),
                  'tstep': spfloat(0.2, prec), 'nterms': 6,
                  'numeric': numeric, 'fpprec': 'multi', 'prec': prec}
        syspar = {'q0': spfloat(3., prec), 'p0': spfloat(0., prec),
                  'fpprec': 'multi', 'prec': prec}
        system = Morse(**syspar)
        self.compare(SymmetricNewtonian, params, system, 1e-2)


class TestSymmetricPolynomialPropagatorMP(unittest.TestCase):
    """ Self-starting symmetric polynomial propagator - many particles """

    def test_cheb_morse_2p_2d_lambdify(self):
        """ Chebyshev for two-particle 2D Morse oscillator with lambdify """

        M = [2.0, 2.0]
        numeric = {'tool': 'lambdify', 'modules': 'numpy'}
        params = {'tstart': 0.0, 'tend': 5., 'tstep': 0.1, 'nterms': 4,
                  'liouville': MPLPQP, 'numeric': numeric}
        syspar = {'symbols': ['0x:y', '1x:y'], 'q0': [[1, 2], [4, 2]],
                  'p0': [[0, 0], [0, 0]], 'masses': M, 'numeric': numeric}
        ffpars = [{'class': 'FFMorse', 'pair': ((0, 1), (2, 3)),
                   'params': {'distance': 1.}}]

        system = MPPSMD(ffpars, dim=2, **syspar)
        ts_prop = SymmetricChebyshev(syst=system, **params)
        ts_prop.propagate()
        time_ts, q_ts, p_ts = ts_prop.get_trajectory()
        q_ts.shape = (len(q_ts), -1, 2)
        p_ts.shape = (len(p_ts), -1, 2)
        r_ts = np.abs(q_ts[:, 1]-q_ts[:, 0])
        r_ts_1p = np.linalg.norm(r_ts, axis=1)
        pr_ts = (p_ts[:, 1]*M[0]-p_ts[:, 0]*M[1])/np.sum(M)
        pr_ts_1p = np.einsum('ij,ij->i', pr_ts, r_ts/r_ts_1p[:, np.newaxis])
        ts_prop.analyse()

        system_1d = Morse(q0=3., p0=0.)
        q_an, p_an = system_1d.solution(times=time_ts)

        self.assertTrue(np.allclose(r_ts_1p, q_an, atol=1e-2))
        self.assertTrue(np.allclose(pr_ts_1p, p_an, atol=1e-2))
        self.assertTrue(np.allclose(ts_prop.er, 0, atol=1e-2))
