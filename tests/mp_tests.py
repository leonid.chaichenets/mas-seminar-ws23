""" Diverse two-particle tests comparing Chebyshev and velocity Verlet """
import unittest
import numpy as np
import sympy as sp
from ase import Atoms
from rotagaporp.systems import ManyAtomsSystem, MPPS1D, MPPS3D
from rotagaporp.chebyshev import Chebyshev
from rotagaporp.newtonian import Newtonian
from rotagaporp.symplectic import Symplectic
from rotagaporp.mpliouville import MPLPQP, MPLPAQ, MPLPVQ
from rotagaporp.mpliouville import MPLPDR, MPLPDV, MPLPNR
from utilities.units import LJUnitsElement

class MPLPTest(unittest.TestCase):
    """ Comparison tests with different MPLP classes without dynamics """

    ffparams = [{'class': 'FFLennardJones', 'pair': ((0, 1, 2), (3, 4, 5))}]
    symbols = ['0x:z', '1x:z']
    masses = np.array([1, 1])

    def test_qvec_pvec_taylor(self):
        """ test with a pre-calculated reference with only subs numerics """
        qval = np.array([[1, 2, 3], [1, 1, 1]])
        pval = np.array([[0, 0, 0], [1, 1, 1]])
        qval_flat = qval.flatten()
        pval_flat = pval.flatten()
        numeric = {'tool': 'subs'}
        qref = [[1.0, 2.0, 3.0, 1.0, 1.0, 1.0], [0.0, 0.0, 0.0, 1.0, 1.0, 1.0],
                [0.0, -0.0377856, -0.0755712, 0.0, 0.0377856, 0.0755712]]
        pref = [[0.0, 0.0, 0.0, 1.0, 1.0, 1.0],
                [0.0, -0.0377856, -0.0755712, 0.0, 0.0377856, 0.0755712],
                [0.0377856, -0.14137344, -0.32053248, -0.0377856, 0.14137344,
                 0.32053248]]

        cases = ((MPLPQP, None), (MPLPAQ, None), (MPLPVQ, None), (MPLPDR, 'dr'),
                 (MPLPDR, 'r'), (MPLPDV, 'dv'), (MPLPDV, 'r'))
        system = MPPS3D(self.ffparams, self.symbols, self.masses, qval, pval,
                        pbc=False, numeric=numeric)

        for mplpcls, var in cases:
            kwargs = {} if not var else {'variant': var}
            mplpobj = mplpcls(3, system, numeric=numeric, recursion='taylor',
                              **kwargs)
            qvec, pvec = mplpobj.get_val_float(qval_flat, pval_flat)
            self.assertTrue(np.allclose(qvec, qref))
            self.assertTrue(np.allclose(pvec, pref))

    def test_qvec_pvec(self):
        """ cross-check all classes with all numerics and variants """
        from rotagaporp.newtonian import leja_points_ga

        qval = np.random.rand(2, 3)
        pval = np.random.rand(2, 3)
        qval_flat = qval.flatten()
        pval_flat = pval.flatten()

        cases = (
            (MPLPQP, None, {'tool': 'subs'}),
            (MPLPQP, None, {'tool': 'theano'}),
            (MPLPQP, None, {'tool': 'lambdify', 'modules': 'math'}),
            (MPLPAQ, None, {'tool': 'subs'}),
            (MPLPAQ, None, {'tool': 'theano'}),
            (MPLPAQ, None, {'tool': 'lambdify', 'modules': 'math'}),
            (MPLPVQ, None, {'tool': 'subs'}),
            (MPLPVQ, None, {'tool': 'theano'}),
            (MPLPDR, 'r', {'tool': 'subs'}),
            (MPLPDR, 'r', {'tool': 'theano'}),
            (MPLPDR, 'dr', {'tool': 'subs'}),
            (MPLPDR, 'dr', {'tool': 'theano'}),
            (MPLPDV, 'r', {'tool': 'subs'}),
            (MPLPDV, 'r', {'tool': 'theano'}),
            (MPLPDV, 'r', {'tool': 'lambdify', 'modules': 'math'}),
            (MPLPDV, 'dv', {'tool': 'subs'}),
            (MPLPDV, 'dv', {'tool': 'theano'}),
            (MPLPDV, 'dv', {'tool': 'lambdify', 'modules': 'math'}),
            (MPLPNR, 'r', {'tool': 'subs'}),
            (MPLPNR, 'r', {'tool': 'theano'}),
            (MPLPNR, 'r', {'tool': 'lambdify', 'modules': 'math'}),
            (MPLPNR, 'dv', {'tool': 'subs'}),
            (MPLPNR, 'dv', {'tool': 'theano'}),
            (MPLPNR, 'dv', {'tool': 'lambdify', 'modules': 'math'})
        )

        nterms = 3
        nodes, scale = leja_points_ga(nterms)
        pdicts = [{'recursion': 'newton', 'nodes': nodes, 'scale': 1./scale},
                  {'recursion': 'chebyshev', 'sign': 1}]

        for pdict in pdicts:
            qref = pref = None
            for mplpcls, variant, numeric in cases:
                system = MPPS3D(self.ffparams, self.symbols, self.masses,
                                qval, pval, numeric=numeric)
                kwargs = {} if not variant else {'variant': variant}
                mplpobj = mplpcls(nterms, system, numeric=numeric, **pdict,
                                  **kwargs)
                if qref and pref:
                    qvec, pvec = mplpobj.get_val_float(qval_flat, pval_flat)
                    relerr = (np.array(pref)-np.array(pvec))/np.array(pref)
                    msg = (repr(mplpcls.__name__)+repr(variant)+repr(numeric)+
                           repr(qval_flat)+repr(pval_flat)+repr(relerr))
                    self.assertTrue(np.allclose(qvec, qref, rtol=0.001), msg)
                    self.assertTrue(np.allclose(pvec, pref, rtol=0.001), msg)
                else:
                    qref, pref = mplpobj.get_val_float(qval_flat, pval_flat)


class PolynomialAndSymplecticTest(unittest.TestCase):
    """ Tests with different MPLP classes and numeric tools """

    def compare(self, propagator, dim, syst, chparams, vvparams, atols):
        """ Main test evaluation function """

        ch = propagator(syst=syst, **chparams)
        ch.propagate()
        ch.analyse()
        (qt, pt) = (ch.get_trajectory()[1:] if dim == 1
                    else ch.get_trajectory_3d()[1:])
        vv = Symplectic(syst=syst, **vvparams)
        vv.propagate()
        vv.analyse()
        (qtr, ptr) = (vv.get_trajectory()[1:] if dim == 1
                      else vv.get_trajectory_3d()[1:])
        rt = np.array([syst.get_distances(q)[0] for q in qt])
        rtr = np.array([syst.get_distances(q)[0] for q in qtr])
        self.assertTrue(np.allclose(rt, rtr, atol=atols['rt']))
        self.assertTrue(np.allclose(pt, ptr, atol=atols['pt']))
        self.assertTrue(np.allclose(ch.er, 0, atol=atols['ch']))
        self.assertTrue(np.allclose(vv.er, 0, atol=atols['vv']))

    def test_00_1d(self):
        """ Two non-interacting particles in one dimension """

        syspar = {'symbols': ['0', '1'], 'q0': [[1], [3]], 'p0': [[1], [-1]],
                  'masses': [1.0, 1.0], 'numeric': {'tool': 'subs'}}
        ffparams = [{'class': 'FFZero', 'pair': ((0,), (1,))}]
        syst = MPPS1D(ffparams, **syspar)
        chparams = {'tstart': 0.0, 'tend': 8.0, 'tstep': 0.01, 'nterms': 4,
                    'liouville': MPLPDV, 'pbc': False}
        vvparams = {'tstart': 0.0, 'tend': 8.0, 'tstep': 0.01, 'pbc': False}
        atols = {'rt': 1e-4, 'pt': 1e-5, 'ch': 1e-5, 'vv': 1e-5}
        dim = 1
        self.compare(Chebyshev, dim, syst, chparams, vvparams, atols)
        self.compare(Newtonian, dim, syst, chparams, vvparams, atols)

    def test_00_1d_pbc(self):
        """ Two non-interacting particles in one periodic dimension """

        syspar = {'symbols': ['0', '1'], 'q0': [[1], [3]], 'p0': [[1], [-1]],
                  'masses': [1.0, 1.0], 'pbc': True, 'cell': [5.0],
                  'numeric': {'tool': 'subs'}}
        ffparams = [{'class': 'FFZero', 'pair': ((0,), (1,))}]

        chparams = {'tstart': 0.0, 'tend': 8.0, 'tstep': 0.01, 'nterms': 4,
                    'liouville': MPLPDV, 'pbc': True}
        vvparams = {'tstart': 0.0, 'tend': 8.0, 'tstep': 0.01, 'pbc': True}
        syst = MPPS1D(ffparams, **syspar)
        atols = {'rt': 1e-4, 'pt': 1e-5, 'ch': 1e-5, 'vv': 1e-5}
        dim = 1
        self.compare(Chebyshev, dim, syst, chparams, vvparams, atols)
        self.compare(Newtonian, dim, syst, chparams, vvparams, atols)

    def test_00_3d_pbc(self):
        """ Two non-interacting particles in three periodic dimensions """

        argon_u = LJUnitsElement('Ar')
        atoms_dict = {
            'symbols': 'ArAr',
            'pbc': (True, True, True),
            'cell': np.array([5, 2, 2])*argon_u.sigmaA,
            'positions': np.array([[0, 0, 0], [3, 0, 0]])*argon_u.sigmaA,
            'momenta': np.array([[1, 0, 0], [-1, 0, 0]])*argon_u.momentumA
        }
        atoms = Atoms(**atoms_dict)
        syst = ManyAtomsSystem(atoms, 'FFZero', numeric={'tool': 'subs'})
        chparams = {'tstart': 0.0, 'tend': 8.0, 'tstep': 0.01, 'nterms': 4,
                    'liouville': MPLPDV, 'pbc': True}
        vvparams = {'tstart': 0.0, 'tend': 8.0, 'tstep': 0.01, 'pbc': True}
        atols = {'rt': 1e-6, 'pt': 1e-6, 'ch': 1e-6, 'vv': 1e-6}
        dim = 3
        self.compare(Chebyshev, dim, syst, chparams, vvparams, atols)
        self.compare(Newtonian, dim, syst, chparams, vvparams, atols)

    def test_ho_1d(self):
        """ Two particles with harmonic coupling in one dimension """

        M = np.full(2, 2.0)
        r0 = 3.5
        kappa = sp.Rational(1, 2)
        syspar = {'symbols': ['0', '1'], 'q0': [[0], [1]], 'p0': [[1], [0]],
                  'masses': M, 'numeric': {'tool': 'subs'}}
        ffpar = [{'class': 'FFHarmonic', 'pair': ((0,), (1,)),
                  'params': {'distance': r0, 'kappa': kappa}}]
        syst = MPPS1D(ffpar, **syspar)
        chparams = {'tstart': 0.0, 'tend': 4.0, 'tstep': 0.005, 'nterms': 4,
                    'liouville': MPLPQP, 'pbc': False}
        vvparams = {'tstart': 0.0, 'tend': 4.0, 'tstep': 0.005, 'pbc': False}
        atols = {'rt': 1e-5, 'pt': 1e-5, 'ch': 1e-6, 'vv': 1e-5}
        dim = 1
        self.compare(Chebyshev, dim, syst, chparams, vvparams, atols)
        self.compare(Newtonian, dim, syst, chparams, vvparams, atols)

    def test_ho_3d(self):
        """ Two particles with harmonic coupling in three dimensions """

        M = np.full(2, 2.0)
        r0 = 3.5
        kappa = sp.Rational(1, 2)
        syspar = {'symbols': ['0x:z', '1x:z'],
                  'q0': [[1, 2, 3], [1.7071, 2.7071, 3]],
                  'p0': [[0.7071, 0.7071, 0], [0, 0, 0]], 'masses': M,
                  'numeric': {'tool': 'subs'}}
        ffpar = [{'class': 'FFHarmonic', 'pair': ((0, 1, 2), (3, 4, 5)),
                  'params': {'distance': r0, 'kappa': kappa}}]
        syst = MPPS3D(ffpar, **syspar)
        chparams = {'tstart': 0.0, 'tend': 1.0, 'tstep': 0.005, 'nterms': 4,
                    'liouville': MPLPQP, 'pbc': False}
        vvparams = {'tstart': 0.0, 'tend': 1.0, 'tstep': 0.005, 'pbc': False}
        atols = {'rt': 1e-6, 'pt': 1e-5, 'ch': 1e-6, 'vv': 1e-5}
        dim = 3
        self.compare(Chebyshev, dim, syst, chparams, vvparams, atols)
        self.compare(Newtonian, dim, syst, chparams, vvparams, atols)

    def test_lj_1d(self):
        """ Two particles with LJ interaction in one dimension """

        syspar = {'symbols': ['0', '1'], 'q0': [[0], [2]], 'p0': [[0], [-1]],
                  'masses': [1.0, 1.0]}
        ffpars = [{'class': 'FFLennardJones', 'pair': ((0,), (1,))}]
        syst = MPPS1D(ffpars, **syspar)

        chparams = {'tstart': 0.0, 'tend': 2.0, 'tstep': 0.01, 'nterms': 4,
                    'liouville': MPLPQP, 'pbc': False}
        vvparams = {'tstart': 0.0, 'tend': 2.0, 'tstep': 0.01, 'pbc': False}
        atols = {'rt': 1e-2, 'pt': 1e-2, 'ch': 0.1, 'vv': 0.1}
        dim = 1
        self.compare(Chebyshev, dim, syst, chparams, vvparams, atols)
        self.compare(Newtonian, dim, syst, chparams, vvparams, atols)

    def test_lj_1d_pbc(self):
        """ Two particles with LJ interaction in one periodic dimension """

        syspar = {'symbols': ['0', '1'], 'q0': [[1], [3]], 'p0': [[0], [-1]],
                  'masses': [1.0, 1.0], 'pbc': True, 'cell': [15.],
                  'numeric': {'tool': 'subs'}}
        ffpars = [{'class': 'FFLennardJones', 'pair': ((0,), (1,))}]
        syst = MPPS1D(ffpars, **syspar)

        chparams = {'tstart': 0.0, 'tend': 4.0, 'tstep': 0.005, 'nterms': 4,
                    'liouville': MPLPDV, 'pbc': True}
        vvparams = {'tstart': 0.0, 'tend': 4.0, 'tstep': 0.005, 'pbc': True}
        atols = {'rt': 1e-2, 'pt': 1e-2, 'ch': 0.1, 'vv': 0.1}
        dim = 1
        self.compare(Chebyshev, dim, syst, chparams, vvparams, atols)
        self.compare(Newtonian, dim, syst, chparams, vvparams, atols)

    def test_lj_3d_mplpqp_subs(self):
        """ Two particles with LJ interaction in three dimensions """

        numeric = {'tool': 'subs'}
        argon_u = LJUnitsElement('Ar')
        symbols = 'ArAr'
        positions = np.array([[1, 2, 3], [3, 2, 3]])*argon_u.sigmaA
        momenta = np.array([[0, 0, 0], [-1, 0, 0]])*argon_u.momentumA
        atoms = Atoms(symbols, positions=positions, momenta=momenta)
        atoms_aux = atoms.copy()
        atoms_aux.set_positions(momenta)
        atoms.euler_rotate(phi=45, theta=30, psi=10)
        atoms_aux.euler_rotate(phi=45, theta=30, psi=10)
        atoms.set_momenta(atoms_aux.get_positions())
        ffield = 'FFLennardJones'
        syst = ManyAtomsSystem(atoms, ffield, numeric=numeric)
        chparams = {'tstart': 0.0, 'tend': 4.0, 'tstep': 0.005, 'nterms': 4,
                    'liouville': MPLPQP, 'pbc': False}
        vvparams = {'tstart': 0.0, 'tend': 4.0, 'tstep': 0.005, 'pbc': False}
        atols = {'rt': 1e-2, 'pt': 1e-2, 'ch': 0.1, 'vv': 0.1}
        dim = 3
        self.compare(Chebyshev, dim, syst, chparams, vvparams, atols)
        self.compare(Newtonian, dim, syst, chparams, vvparams, atols)

    def test_lj_3d_mplpaq_subs(self):
        """ Two particles with LJ interaction in three dimensions """

        numeric = {'tool': 'subs'}
        argon_u = LJUnitsElement('Ar')
        symbols = 'ArAr'
        positions = np.array([[1, 2, 3], [3, 2, 3]])*argon_u.sigmaA
        momenta = np.array([[0, 0, 0], [-1, 0, 0]])*argon_u.momentumA
        atoms = Atoms(symbols, positions=positions, momenta=momenta)
        atoms_aux = atoms.copy()
        atoms_aux.set_positions(momenta)
        atoms.euler_rotate(phi=45, theta=30, psi=10)
        atoms_aux.euler_rotate(phi=45, theta=30, psi=10)
        atoms.set_momenta(atoms_aux.get_positions())
        ffield = 'FFLennardJones'
        syst = ManyAtomsSystem(atoms, ffield, numeric=numeric)
        chparams = {'tstart': 0.0, 'tend': 4.0, 'tstep': 0.005, 'nterms': 4,
                    'liouville': MPLPAQ, 'pbc': False}
        vvparams = {'tstart': 0.0, 'tend': 4.0, 'tstep': 0.005, 'pbc': False}
        atols = {'rt': 1e-2, 'pt': 1e-2, 'ch': 0.1, 'vv': 0.1}
        dim = 3
        self.compare(Chebyshev, dim, syst, chparams, vvparams, atols)
        # MPLPAQ class has an accuracy problem with Newtonian
        # self.compare(Newtonian, dim, syst, chparams, vvparams, atols)

    def test_lj_3d_mplpaq_lambdify(self):
        """ Two particles with LJ interaction in three dimensions """

        numeric = {'tool': 'lambdify', 'modules': ['math', 'numpy']}
        argon_u = LJUnitsElement('Ar')
        symbols = 'ArAr'
        positions = np.array([[1, 2, 3], [3, 2, 3]])*argon_u.sigmaA
        momenta = np.array([[0, 0, 0], [-1, 0, 0]])*argon_u.momentumA
        atoms = Atoms(symbols, positions=positions, momenta=momenta)
        atoms_aux = atoms.copy()
        atoms_aux.set_positions(momenta)
        atoms.euler_rotate(phi=45, theta=30, psi=10)
        atoms_aux.euler_rotate(phi=45, theta=30, psi=10)
        atoms.set_momenta(atoms_aux.get_positions())
        ffield = 'FFLennardJones'
        syst = ManyAtomsSystem(atoms, ffield, numeric=numeric)
        chparams = {'tstart': 0.0, 'tend': 4.0, 'tstep': 0.005, 'nterms': 4,
                    'liouville': MPLPAQ, 'pbc': False}
        vvparams = {'tstart': 0.0, 'tend': 4.0, 'tstep': 0.005, 'pbc': False}
        atols = {'rt': 1e-2, 'pt': 1e-2, 'ch': 0.1, 'vv': 0.1}
        dim = 3
        self.compare(Chebyshev, dim, syst, chparams, vvparams, atols)
        # MPLPAQ class has an accuracy problem with Newtonian
        # self.compare(Newtonian, dim, syst, chparams, vvparams, atols)

    def test_lj_3d_mplpaq_theano(self):
        """ Two particles with LJ interaction in three dimensions """

        numeric = {'tool': 'theano'}
        argon_u = LJUnitsElement('Ar')
        symbols = 'ArAr'
        positions = np.array([[1, 2, 3], [3, 2, 3]])*argon_u.sigmaA
        momenta = np.array([[0, 0, 0], [-1, 0, 0]])*argon_u.momentumA
        atoms = Atoms(symbols, positions=positions, momenta=momenta)
        atoms_aux = atoms.copy()
        atoms_aux.set_positions(momenta)
        atoms.euler_rotate(phi=45, theta=30, psi=10)
        atoms_aux.euler_rotate(phi=45, theta=30, psi=10)
        atoms.set_momenta(atoms_aux.get_positions())
        ffield = 'FFLennardJones'
        syst = ManyAtomsSystem(atoms, ffield, numeric=numeric)
        chparams = {'tstart': 0.0, 'tend': 4.0, 'tstep': 0.005, 'nterms': 4,
                    'liouville': MPLPAQ, 'pbc': False}
        vvparams = {'tstart': 0.0, 'tend': 4.0, 'tstep': 0.005, 'pbc': False}
        atols = {'rt': 1e-2, 'pt': 1e-2, 'ch': 0.1, 'vv': 0.1}
        dim = 3
        self.compare(Chebyshev, dim, syst, chparams, vvparams, atols)
        # MPLPAQ class has an accuracy problem with Newtonian
        # self.compare(Newtonian, dim, syst, chparams, vvparams, atols)

    def test_lj_3d_mplpvq_theano(self):
        """ Two particles with LJ interaction in three dimensions """

        numeric = {'tool': 'theano'}
        argon_u = LJUnitsElement('Ar')
        symbols = 'ArAr'
        positions = np.array([[1, 2, 3], [3, 2, 3]])*argon_u.sigmaA
        momenta = np.array([[0, 0, 0], [-1, 0, 0]])*argon_u.momentumA
        atoms = Atoms(symbols, positions=positions, momenta=momenta)
        atoms_aux = atoms.copy()
        atoms_aux.set_positions(momenta)
        atoms.euler_rotate(phi=45, theta=30, psi=10)
        atoms_aux.euler_rotate(phi=45, theta=30, psi=10)
        atoms.set_momenta(atoms_aux.get_positions())
        ffield = 'FFLennardJones'
        syst = ManyAtomsSystem(atoms, ffield, numeric=numeric)
        chparams = {'tstart': 0.0, 'tend': 4.0, 'tstep': 0.005, 'nterms': 4,
                    'liouville': MPLPVQ, 'pbc': False}
        vvparams = {'tstart': 0.0, 'tend': 4.0, 'tstep': 0.005, 'pbc': False}
        atols = {'rt': 1e-2, 'pt': 1e-2, 'ch': 0.1, 'vv': 0.1}
        dim = 3
        self.compare(Chebyshev, dim, syst, chparams, vvparams, atols)
        self.compare(Newtonian, dim, syst, chparams, vvparams, atols)

    def test_lj_3d_mplpdr_theano(self):
        """ Two particles with LJ interaction in three dimensions """

        numeric = {'tool': 'theano'}
        argon_u = LJUnitsElement('Ar')
        symbols = 'ArAr'
        positions = np.array([[1, 2, 3], [3, 2, 3]])*argon_u.sigmaA
        momenta = np.array([[0, 0, 0], [-1, 0, 0]])*argon_u.momentumA
        atoms = Atoms(symbols, positions=positions, momenta=momenta)
        atoms_aux = atoms.copy()
        atoms_aux.set_positions(momenta)
        atoms.euler_rotate(phi=45, theta=30, psi=10)
        atoms_aux.euler_rotate(phi=45, theta=30, psi=10)
        atoms.set_momenta(atoms_aux.get_positions())
        ffield = 'FFLennardJones'
        syst = ManyAtomsSystem(atoms, ffield, numeric=numeric)
        chparams = {'tstart': 0.0, 'tend': 4.0, 'tstep': 0.005, 'nterms': 4,
                    'liouville': MPLPDR, 'pbc': False}
        vvparams = {'tstart': 0.0, 'tend': 4.0, 'tstep': 0.005, 'pbc': False}
        atols = {'rt': 1e-2, 'pt': 1e-2, 'ch': 0.1, 'vv': 0.1}
        dim = 3
        self.compare(Chebyshev, dim, syst, chparams, vvparams, atols)
        self.compare(Newtonian, dim, syst, chparams, vvparams, atols)

    def test_lj_3d_pbc_theano(self):
        """ Two particles with LJ interaction in three periodic dimensions """

        numeric = {'tool': 'theano'}
        argon_u = LJUnitsElement('Ar')
        atoms_dict = {
            'symbols': 'ArAr',
            'pbc': (True, True, True),
            'cell': np.array([15, 15, 15])*argon_u.sigmaA,
            'positions': np.array([[1, 0, 0], [3, 0, 0]])*argon_u.sigmaA,
            'momenta': np.array([[0, 0, 0], [-1, 0, 0]])*argon_u.momentumA
        }
        ffield = 'FFLennardJones'
        syst = ManyAtomsSystem(Atoms(**atoms_dict), ffield, numeric=numeric)
        chparams = {'tstart': 0.0, 'tend': 4.0, 'tstep': 0.005, 'nterms': 4,
                    'liouville': MPLPDV, 'pbc': True}
        vvparams = {'tstart': 0.0, 'tend': 4.0, 'tstep': 0.005, 'pbc': True}

        atols = {'rt': 1e-2, 'pt': 1e-2, 'ch': 0.1, 'vv': 0.1}
        dim = 3
        self.compare(Chebyshev, dim, syst, chparams, vvparams, atols)
        self.compare(Newtonian, dim, syst, chparams, vvparams, atols)

    def test_lj_3d_pbc_subs(self):
        """ Two particles with LJ interaction in three periodic dimensions """

        numeric = {'tool': 'subs'}
        argon_u = LJUnitsElement('Ar')
        atoms_dict = {
            'symbols': 'ArAr',
            'pbc': (True, True, True),
            'cell': np.array([15, 15, 15])*argon_u.sigmaA,
            'positions': np.array([[1, 0, 0], [3, 0, 0]])*argon_u.sigmaA,
            'momenta': np.array([[0, 0, 0], [-1, 0, 0]])*argon_u.momentumA
        }
        ffield = 'FFLennardJones'
        syst = ManyAtomsSystem(Atoms(**atoms_dict), ffield, numeric=numeric)
        chparams = {'tstart': 0.0, 'tend': 4.0, 'tstep': 0.005, 'nterms': 4,
                    'liouville': MPLPDV, 'pbc': True}
        vvparams = {'tstart': 0.0, 'tend': 4.0, 'tstep': 0.005, 'pbc': True}
        atols = {'rt': 1e-2, 'pt': 1e-2, 'ch': 0.1, 'vv': 0.1}
        dim = 3
        self.compare(Chebyshev, dim, syst, chparams, vvparams, atols)
        self.compare(Newtonian, dim, syst, chparams, vvparams, atols)

    def test_lj_3d_pbc_lambdify(self):
        """ Two particles with LJ interaction in three periodic dimensions """

        numeric = {'tool': 'lambdify', 'modules': ['math', 'numpy']}
        argon_u = LJUnitsElement('Ar')
        atoms_dict = {
            'symbols': 'ArAr',
            'pbc': (True, True, True),
            'cell': np.array([15, 15, 15])*argon_u.sigmaA,
            'positions': np.array([[1, 0, 0], [3, 0, 0]])*argon_u.sigmaA,
            'momenta': np.array([[0, 0, 0], [-1, 0, 0]])*argon_u.momentumA
        }
        ffield = 'FFLennardJones'
        syst = ManyAtomsSystem(Atoms(**atoms_dict), ffield, numeric=numeric)
        chparams = {'tstart': 0.0, 'tend': 4.0, 'tstep': 0.005, 'nterms': 4,
                    'liouville': MPLPDV, 'pbc': True}
        vvparams = {'tstart': 0.0, 'tend': 4.0, 'tstep': 0.005, 'pbc': True}
        atols = {'rt': 1e-2, 'pt': 1e-2, 'ch': 0.1, 'vv': 0.1}
        dim = 3
        self.compare(Chebyshev, dim, syst, chparams, vvparams, atols)
        self.compare(Newtonian, dim, syst, chparams, vvparams, atols)


class TwoParticleXDSystemTest(unittest.TestCase):
    """ 1P, 2P/1D and 2P/3D equivalent systems with different propagators """

    def run_case(self, case, system_1p, syspar_1d, syspar_3d, ffpars_1d,
                 ffpars_3d, qref, pref):
        """ common block """
        rtol = 0.05
        M = syspar_1d['masses']
        # solve 2p in 1d and reduce 2p solution to 1p solution
        syst_1d = MPPS1D(ffpars_1d, **syspar_1d)
        prop_1d = case['propagator'](syst=syst_1d, **case['params_2p'])
        prop_1d.propagate()
        t, qt_1d, pt_1d = prop_1d.get_trajectory(step=case['ostep'])
        qt_1d_1p = np.abs(qt_1d[:, 0]-qt_1d[:, 1])
        pt_1d_1p = (pt_1d[:, 1]*M[0]-pt_1d[:, 0]*M[1])/np.sum(M)

        # solve 2p in 3d and reduce 2p solution to 1p solution
        syst_3d = MPPS3D(ffpars_3d, **syspar_3d)
        prop_3d = case['propagator'](syst=syst_3d, **case['params_2p'])
        prop_3d.propagate()
        t, qt_3d, pt_3d = prop_3d.get_trajectory_3d(step=case['ostep'])
        rt_3d = np.abs(qt_3d[:, 1]-qt_3d[:, 0])
        qt_3d_1p = np.linalg.norm(rt_3d, axis=1)
        pr_3d = (pt_3d[:, 1]*M[0]-pt_3d[:, 0]*M[1])/np.sum(M)
        rt_3d_normalized = rt_3d/qt_3d_1p[:, np.newaxis]
        pt_3d_1p = np.einsum('ij,ij->i', pr_3d, rt_3d_normalized)

        # solve the equivalent 1p problem
        syspar_1p = {
            'q0': np.abs(syspar_1d['q0'][0][0]-syspar_1d['q0'][1][0]),
            'p0': ((syspar_1d['p0'][1][0]*M[0]-syspar_1d['p0'][0][0]*M[1])
                   /np.sum(M)),
            'mass': np.prod(M)/np.sum(M)
        }
        syst_1p = system_1p(**syspar_1p)
        prop_1p = case['propagator'](syst=syst_1p, **case['params_1p'])
        prop_1p.propagate()
        t, qt_1p, pt_1p = prop_1p.get_trajectory(step=case['ostep'])

        self.assertTrue(np.allclose(qt_1p, qref, rtol=rtol))
        self.assertTrue(np.allclose(qt_1d_1p, qref, rtol=rtol))
        self.assertTrue(np.allclose(qt_3d_1p, qref, rtol=rtol))
        self.assertTrue(np.allclose(pt_1p, pref, rtol=rtol))
        self.assertTrue(np.allclose(pt_1d_1p, pref, rtol=rtol))
        self.assertTrue(np.allclose(pt_3d_1p, pref, rtol=rtol))

    def lj_2p_xd_systems_test(self):
        """ collision of two LJ particles with collinear initial momenta """
        from rotagaporp.liouville import LiouvillePowersQP
        from rotagaporp.systems import LennardJones
        cases = [
            {'propagator': Chebyshev,
             'params_2p': {'tstart': 0., 'tend': 2., 'tstep': 0.01,
                           'nterms': 4, 'liouville': MPLPQP},
             'params_1p': {'tstart': 0., 'tend': 2., 'tstep': 0.01,
                           'nterms': 4, 'liouville': LiouvillePowersQP},
             'ostep': 10},
            {'propagator': Newtonian,
             'params_2p': {'tstart': 0., 'tend': 2., 'tstep': 0.01,
                           'nterms': 4, 'liouville': MPLPQP},
             'params_1p': {'tstart': 0., 'tend': 2., 'tstep': 0.01,
                           'nterms': 4, 'liouville': LiouvillePowersQP},
             'ostep': 10},
            {'propagator': Symplectic,
             'params_2p': {'tstart': 0., 'tend': 2., 'tstep': 0.01},
             'params_1p': {'tstart': 0., 'tend': 2., 'tstep': 0.01},
             'ostep': 10}
        ]

        syspar_3d = {'symbols': ['0x:z', '1x:z'], 'q0': [[0, 0, 0], [2, 0, 0]],
                     'p0': [[1, 0, 0], [-1, 0, 0]], 'masses': [2., 2.]}
        ffpars_3d = [{'class': 'FFLennardJones',
                      'pair': ((0, 1, 2), (3, 4, 5))}]
        syspar_1d = {'symbols': ['0', '1'], 'q0': [[0], [2]],
                     'p0': [[1], [-1]], 'masses': [2., 2.]}
        system_1p = LennardJones
        ffpars_1d = [{'class': 'FFLennardJones', 'pair': ((0,), (1,))}]
        qref = [2.0, 1.8989785779371167, 1.7953437167317878,
                1.6878993741878476, 1.5747404994964764, 1.452727137056799,
                1.3166386344766197, 1.1596563118865453, 1.0043519953977942,
                1.039336868462998, 1.2045097457993696, 1.355362576705463,
                1.4869642100997895, 1.6061633304271423, 1.7175254846946968,
                1.8237844391044642, 1.926613294791016, 2.0270724531554927,
                2.125856098718258, 2.2234314621549567, 2.3201205255469715]
        pref = [-1.0, -1.0217227730037155, -1.0529620299855984,
                -1.0990796848910294, -1.1692873278170506, -1.279710813610805,
                -1.4553481612268637, -1.6770723302493862, -1.036237686848960,
                1.4767257091506143, 1.6243223329300234, 1.3998182640873311,
                1.2444679932243272, 1.1470988469414523, 1.0846628953282689,
                1.0432895225326182, 1.0150513350237833, 0.9952898269484088,
                0.9811643206978317, 0.9708823361443493, 0.9632788136838539]

        for case in cases:
            self.run_case(case, system_1p, syspar_1d, syspar_3d, ffpars_1d,
                          ffpars_3d, qref, pref)

    def morse_2p_xd_systems_test(self):
        """ Two particles in a Morse potential """
        from rotagaporp.liouville import LiouvillePowersQP
        from rotagaporp.systems import Morse
        cases = [
            {'propagator': Chebyshev,
             'params_2p': {'tstart': 0., 'tend': 10., 'tstep': 0.1,
                           'nterms': 4, 'liouville': MPLPQP},
             'params_1p': {'tstart': 0., 'tend': 10., 'tstep': 0.1,
                           'nterms': 4, 'liouville': LiouvillePowersQP},
             'ostep': 5},
            {'propagator': Newtonian,
             'params_2p': {'tstart': 0., 'tend': 10., 'tstep': 0.1,
                           'nterms': 4, 'liouville': MPLPQP},
             'params_1p': {'tstart': 0., 'tend': 10., 'tstep': 0.1,
                           'nterms': 4, 'liouville': LiouvillePowersQP},
             'ostep': 5},
            {'propagator': Symplectic,
             'params_2p': {'tstart': 0., 'tend': 10., 'tstep': 0.1},
             'params_1p': {'tstart': 0., 'tend': 10., 'tstep': 0.1},
             'ostep': 5}
        ]

        syspar_3d = {'symbols': ['0x:z', '1x:z'], 'q0': [[1, 0, 0], [4, 0, 0]],
                     'p0': [[1, 0, 0], [-1, 0, 0]], 'masses': [2., 2.]}
        ffpars_3d = [{'class': 'FFMorse',
                      'pair': ((0, 1, 2), (3, 4, 5)),
                      'params': {'distance': 1.}}]
        syspar_1d = {'symbols': ['0', '1'], 'q0': [[1], [4]],
                     'p0': [[1], [-1]], 'masses': [2., 2.]}
        system_1p = Morse
        ffpars_1d = [{'class': 'FFMorse', 'pair': ((0,), (1,)),
                      'params': {'distance': 1.}}]
        qref = [3.0, 2.466353994207689, 1.8434115864194833,
                1.1045755646962236, 0.38276415690104315, 0.3938315523034868,
                1.1212032614325005, 1.8577341110280325, 2.4784290847793833,
                3.0105466576988023, 3.483835680973514, 3.918331143003803,
                4.326923598702568, 4.717988735340914, 5.097065925975702,
                5.467877754643574, 5.832957533172998, 6.194045504849354,
                6.552345324985267, 6.908693618296527, 7.263673942394301]
        pref = [-1.0, -1.1454780984900894, -1.3587625810660504,
                -1.5713969075646013, -1.0137769377090264, 1.0465935570930196,
                1.5694855398648118, 1.3536023480368258, 1.141742551622132,
                0.9975372922287521, 0.9027157397429713, 0.8398187677216881,
                0.7975122201642489, 0.7687098036400428, 0.7489189166263421,
                0.7352276074603107, 0.7257096914471689, 0.7190700072131954,
                0.7144267511010234, 0.711173975803855, 0.708892482612633]

        for case in cases:
            self.run_case(case, system_1p, syspar_1d, syspar_3d, ffpars_1d,
                          ffpars_3d, qref, pref)
