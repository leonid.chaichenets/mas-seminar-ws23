""" """
import unittest
import numpy as np
from ase.geometry import wrap_positions as wrap2
from ase.geometry import get_distances
from rotagaporp.pbc import get_distance_matrix_lc as dists1
from rotagaporp.pbc import get_distance_matrix_bc as dists2
from rotagaporp.pbc import wrap_positions as wrap1

class PBCTest(unittest.TestCase):
    """ """
    pbc = (True, True, True)

    def test_position_small(self):
        """ """
        cell = np.array([3.0, 4.0, 5.0])
        posvec = np.array([[3.5, 2.0, 2.5], [0.0, -1.0, 4.0]])
        res1 = wrap1(posvec, cell)
        res2 = wrap2(posvec, cell=np.diag(cell), pbc=self.pbc)
        self.assertTrue(np.allclose(res1, res2))

    def test_distance_small(self):
        """ """
        cell = np.array([3.0, 4.0, 5.0])
        posvec = np.array([[1.5, 2.0, 2.5], [2.7, 1.5, 4.3], [1.2, 0.3, 4.2]])
        res1 = np.linalg.norm(dists1(posvec, cell), axis=2)
        res2 = np.linalg.norm(dists2(posvec, cell), axis=2)
        res_ase = get_distances(posvec, cell=np.diag(cell), pbc=self.pbc)[1]
        self.assertAlmostEqual(res1[0, 1], 2.22036033)
        self.assertAlmostEqual(res1[0, 2], 2.42280829)
        self.assertAlmostEqual(res1[1, 2], 1.92353841)
        self.assertTrue(np.allclose(res1, res2))
        self.assertTrue(np.allclose(res1, res_ase))

    def test_distance_big(self):
        """ """
        cell = np.random.rand(3)
        posvec = np.random.rand(100, 3)
        ful1 = dists1(posvec, cell)
        ful2 = dists2(posvec, cell)
        asef = get_distances(posvec, cell=np.diag(cell), pbc=self.pbc)
        ful3 = asef[0]
        self.assertTrue(np.allclose(ful1, ful2))
        self.assertTrue(np.allclose(ful1, ful3))
        self.assertTrue(np.allclose(ful2, ful3))
        res1 = np.linalg.norm(ful1, axis=2)
        res2 = np.linalg.norm(ful2, axis=2)
        res3 = asef[1]
        self.assertTrue(np.allclose(res1, res2))
        self.assertTrue(np.allclose(res1, res3))
        self.assertTrue(np.allclose(res2, res3))
