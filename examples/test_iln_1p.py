""" Analysis of the terms of one-particle (iL)^n in one dimension """
import sympy as sp
from rotagaporp.liouville import SPLP

q = sp.Symbol('q')
p = sp.Symbol('p')
hamiltonian = sp.Function('V', real=True)(q) + sp.Function('T', real=True)(p)

nterms = 5
iln = SPLP(nterms, hamiltonian, q, p, function=q)
powers = list(reversed(range(iln.nterms)))
A = [sp.diff(iln.H, q, n) for n in range(iln.nterms)]
B = [sp.diff(iln.H, p, n) for n in range(iln.nterms)]
derivs = [d for pairs in reversed(list(zip(A, B))) for d in pairs]
subs_q = [sp.Symbol(s+str(k)) for k in powers for s in ['A', 'B']]
subs_p = [sp.Symbol(s+str(k)) for k in powers for s in ['B', 'A']]
replq = list(zip(derivs, subs_q))
replp = list(zip(derivs, subs_p))
ilnq = [ilk.subs(replq) for ilk in iln.Lf]
ilnp = [ilk.subs(replp).subs(q, p)*(-1)**k for k, ilk in enumerate(iln.Lf)]

string = '{0:5d} {1:6d} {2} {3}'
for symb, obj in zip(['q', 'p'], [ilnq, ilnp]):
    print('--- iL^n {0} ---'.format(symb))
    print('power #terms type terms')
    for power, expr in enumerate(obj):
        nterms = 1 if isinstance(expr, sp.Mul) else len(expr.args)
        terms = (expr,) if isinstance(expr, sp.Symbol) else expr.args
        print(string.format(power, nterms, type(expr), terms))
