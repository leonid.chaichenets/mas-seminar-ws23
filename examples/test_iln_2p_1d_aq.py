""" Analysis of (iL)^n terms of a two-particle system in one dimension """
import yaml
import sympy as sp
from rotagaporp.mpliouville import MPLPAQ, get_stats_expr, get_stats_repl

nterms = 4

class System:
    q = sp.Array(sp.symbols(['q1 q2'], real=True))
    p = sp.Array(sp.symbols(['p1 p2'], real=True))
    r = sp.Symbol('r', real=True)
    r0 = 1. 
    # r0 = sp.Symbol('r0', real=True)
    # V = (r-r0)**2/2
    V = 4*((1/r)**12-(1/r)**6)
    V = V.subs(r, abs(q[0]-q[1]))
    T = sum(pp**2/2 for pp in p)
    hamiltonian = (V, T)

system = System()
iln = MPLPAQ(nterms, system, numeric={'tool': 'subs'})

print(yaml.dump(get_stats_expr(iln.ilnqexpr, details=True)))
print(yaml.dump(get_stats_expr(iln.ilnpexpr, details=True)))
print(yaml.dump(get_stats_repl(iln.repl, details=True)))

string = '{0:5d} {1:d}/{2:d} {3:6d} {4} {5}'
for symb, obj in zip(['Q', 'P'], [iln.ilnqexpr, iln.ilnpexpr]):
    print('--- iL^n {0} ---'.format(symb))
    print('power #var/total #terms type terms')
    for power, lst in enumerate(obj):
        for coord, expr in enumerate(lst):
            nterms = 1 if isinstance(expr, sp.Mul) else len(expr.args)
            terms = (expr,) if isinstance(expr, sp.Symbol) else expr.args
            print(string.format(power, coord+1, len(lst), nterms, type(expr), terms))
