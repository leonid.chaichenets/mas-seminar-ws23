""" Analysis of the terms of three-particle (iL)^n in one dimension """
import yaml
import sympy as sp
from rotagaporp.mpliouville import MPLPAQ, get_stats_expr, get_stats_repl
from rotagaporp.forcefield import FFList

nterms = 3

class System:
    q = sp.Array(sp.symbols(['q0 q1 q2'], real=True))
    p = sp.Array(sp.symbols(['p0 p1 p2'], real=True))

    ffparams = [
        {'class': 'FFLennardJones', 'pair': ((0,), (1,))},
        {'class': 'FFLennardJones', 'pair': ((0,), (2,))},
        {'class': 'FFLennardJones', 'pair': ((1,), (2,))}
    ]
    ff = FFList(ffparams, q)
    V = ff.get_potential_energy()
    T = sum(pp**2/2 for pp in p)
    hamiltonian = (V, T)

system = System()
iln = MPLPAQ(nterms, system, numeric={'tool': 'subs'})
# iln = MPLPAQ(nterms, system, numeric={'tool': 'theano'})
# iln = MPLPAQ(nterms, system, numeric={'tool': 'lambdify', 'modules': 'math'})

print(yaml.dump(get_stats_expr(iln.ilnqexpr, details=True)))
print(yaml.dump(get_stats_expr(iln.ilnpexpr, details=True)))
print(yaml.dump(get_stats_repl(iln.repl, details=True)))

string = '{0:5d} {1:d}/{2:d} {3:6d} {4} {5}'
for symb, obj in zip(['Q', 'P'], [iln.ilnqexpr, iln.ilnpexpr]):
    print('--- iL^n {0} ---'.format(symb))
    print('power #var/total #terms type terms')
    for power, lst in enumerate(obj):
        for coord, expr in enumerate(lst):
            nterms = 1 if isinstance(expr, sp.Mul) else len(expr.args)
            terms = (expr,) if isinstance(expr, sp.Symbol) else expr.args
            print(string.format(power, coord+1, len(lst), nterms, type(expr), terms))
