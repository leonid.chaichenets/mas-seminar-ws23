""" Numerical determination of Liouville eigenfunctions for one particle """
import numpy as np
import matplotlib
matplotlib.use('Qt5Cairo')
import matplotlib.pyplot as plt
from rotagaporp.systems import Morse, Harmonic, Anharmonic, LennardJones
from rotagaporp.symplectic import Symplectic

params = {'tstart': 0., 'tend': 1000., 'tstep': 0.1}
syspar = {'q0': 2., 'p0': 0.0}

syst = Morse(**syspar)
# syst = Harmonic(**syspar)
# syst = Anharmonic(**syspar)
# syst = LennardJones(**syspar)
prop = Symplectic(syst=syst, **params)
prop.propagate()
(ti, qt, pt) = prop.get_trajectory()
half = len(ti)//2

fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2)
ax1.plot(ti, qt, label='q')
ax1.plot(ti, pt, label='p')
ax1.set_xlabel(r'$t$')
ax1.set_ylabel(r'$q,\quad p$')
ax1.legend()

ax2.plot(qt, pt)
ax2.set_xlabel(r'$q$')
ax2.set_ylabel(r'$p$')

border = 0.05 # 5%
pmin = np.min(pt)*(1-border)
pmax = np.max(pt)*(1+border)
qmin = np.min(qt)*(1-border)
qmax = np.max(qt)*(1+border)
qsigma = (qmax-qmin)/10
psigma = (pmax-pmin)/10
prange = np.linspace(pmin, pmax, 80)
qrange = np.linspace(qmin, qmax, 80)
qmesh, pmesh = np.meshgrid(qrange, prange, indexing='ij')
rho = np.empty((len(ti), len(qrange), len(prange)), dtype=np.float)
for tind in range(len(ti)):
    rho[tind, :, :] = (np.exp(-(qt[tind]-qmesh)**2/(2*qsigma**2)
                              -(pt[tind]-pmesh)**2/(2*psigma**2))
                        /(2*np.pi)*qsigma*psigma)

# '''
cfft = np.fft.fft(rho, axis=0)
fftf = cfft*np.conjugate(cfft)
'''
corrf = np.empty(rho.shape, dtype=np.float)
sigma = (params['tend']-params['tstart'])/3
center = (params['tstart']+params['tend'])/2
for i in range(rho.shape[1]):
    for j in range(rho.shape[2]):
        corrf[:, i, j] = np.correlate(rho[:, i, j], rho[:, i, j], mode='same')
fftf = np.empty(rho.shape, dtype=np.complex)
for i in range(rho.shape[1]):
    for j in range(rho.shape[2]):
        fftf[:, i, j] = np.fft.fft(corrf[:, i, j])
'''
freq = np.fft.fftfreq(len(ti), d=params['tstep'])*2*np.pi

fftcjg = fftf*np.conjugate(fftf)
avfftf = np.trapz(np.trapz(fftcjg, x=prange, axis=2), x=qrange, axis=1)
ax3.plot(freq[:half], avfftf[:half].real, label='integrated')
ax3.set_xlabel(r'$\omega$')
ax3.set_ylabel(r'$F(\omega)={\cal F} C(\tau)$')
ax3.legend()

show_ev = 1.4
show_ev_index = np.min(np.where(freq >= show_ev))
ax4.contourf(qmesh, pmesh, fftf[show_ev_index, :, :].real)
ax4.set_xlabel('q')
ax4.set_ylabel('p')

fig.subplots_adjust(hspace=0.3)
plt.show()
