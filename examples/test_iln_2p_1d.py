""" Analysis of the terms of two-particle (iL)^n in one dimension """
import sympy as sp
from rotagaporp.mpliouville import MPLP

nterms = 5
Q = sp.Array(sp.symbols(['q1 q2'], real=True))
P = sp.Array(sp.symbols(['p1 p2'], real=True))

# r = sp.Function('r', real=True)(*Q)
# V = sp.Function('V', real=True)(r)
# V = sp.Function('V', real=True)(*Q)
# r = abs(Q[0]-Q[1])
r = sp.Symbol('r', real=True)
r0 = 1. 
# r0 = sp.Symbol('r0', real=True)
V = (r-r0)**2/2
V = V.subs(r, abs(Q[0]-Q[1]))
T = sum(p**2/2 for p in P)
H = (V, T)

ilnq = MPLP(nterms, H, Q, P, Q, recursion='chebyshev', sign=1)
ilnp = MPLP(nterms, H, Q, P, P, recursion='chebyshev', sign=1)
print(ilnq.Lf[nterms-1])
print(ilnp.Lf[nterms-1])
Qval = [1, 1.1]
Pval = [0, -1]
Lf_qval = ilnq.get_val_float(Qval, Pval)
Lf_pval = ilnp.get_val_float(Qval, Pval)
print(Lf_qval)
print(Lf_pval)
#print(iln.Lf[2][0])
#print(iln.Lf[2][0].subs(r, abs(Q[0]-Q[1])).doit())
#print(iln.Lf[2][0].subs(r, abs(Q[0]-Q[1])).doit().subs(list(zip(Q, [1, 0]))))
