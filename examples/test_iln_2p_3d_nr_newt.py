""" Non-recursive Newtonian expansion of (iL)^n for two particles in 3D """
import time
import numpy as np
from rotagaporp.mpliouville import MPLPDV, MPLPNR
from rotagaporp.newtonian import leja_points_ga
from rotagaporp.systems import MPPS3D

nterms = 3

ffparams = [{'class': 'FFLennardJones', 'pair': ((0, 1, 2), (3, 4, 5))}]
symbols = ['0x:z', '1x:z']
masses = np.array([1, 1])
qval = np.random.rand(2, 3)
pval = np.random.rand(2, 3)
numeric={'tool': 'theano'}
# numeric={'tool': 'lambdify', 'modules': 'math'}
# numeric={'tool': 'subs'}
system = MPPS3D(ffparams, symbols, masses, qval, pval, numeric=numeric)

lambdas, scale = leja_points_ga(nterms)

ts = time.time()
ilnnr = MPLPNR(nterms, system, recursion='newton', nodes=lambdas,
               scale=1./scale, numeric=numeric)
te = time.time()
print("preparation time for MPLPNR:", te-ts)

ts = time.time()
ilndv = MPLPDV(nterms, system, recursion='newton', nodes=lambdas,
               scale=1./scale, numeric=numeric)
te = time.time()
print("preparation time for MPLPDV:", te-ts)

ts = time.time()
ilnnr = MPLPNR(nterms, system, recursion='newton', nodes=lambdas,
               scale=1./scale, numeric=numeric)
te = time.time()
print("preparation time for MPLPNR:", te-ts)

assert np.allclose(ilnnr.get_val_float(system.q0, system.p0),
                   ilndv.get_val_float(system.q0, system.p0))

qarr = np.random.rand(1000, 6)
parr = np.random.rand(1000, 6)

ts = time.time()
for q, p in zip(qarr, parr):
    ilnnr.get_val_float(q, p)
te = time.time()
print("propagation time for MPLPNR:", te-ts)

ts = time.time()
for q, p in zip(qarr, parr):
    ilndv.get_val_float(q, p)
te = time.time()
print("propagation time for MPLPDV:", te-ts)
