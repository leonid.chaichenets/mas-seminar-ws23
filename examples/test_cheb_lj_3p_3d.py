""" Three dimensional Lennard-Jones gas with three particles """
import numpy as np
from ase import Atoms
import matplotlib.pyplot as plt
from rotagaporp.chebyshev import Chebyshev
from rotagaporp.symplectic import Symplectic
from rotagaporp.systems import ManyAtomsSystem
from rotagaporp.mpliouville import MPLPAQ
from utilities.units import LJUnitsElement
from utilities.trajectory import write_ase_trajectory

argon_u = LJUnitsElement('Ar')
symbols = 'ArArAr'
# positions = np.array([[0, 0, 0], [2, 0, 0]])*argon_u.sigmaA
# positions translated with [1, 2, 3]:
positions = np.array([[2, 2, 3], [4, 2, 3], [0, 2, 3]])*argon_u.sigmaA
momenta = np.array([[0, 0, 0], [-2, 0, 0], [0, 0, 0]])*argon_u.momentumA
atoms = Atoms(symbols, positions=positions, momenta=momenta)

# positions and momenta rotated by Euler angles 45, 30 and 10 deg
atoms_aux = atoms.copy()
atoms_aux.set_positions(momenta)
atoms.euler_rotate(phi=45, theta=30, psi=10)
atoms_aux.euler_rotate(phi=45, theta=30, psi=10)
atoms.set_momenta(atoms_aux.get_positions())
numeric = {'tool': 'lambdify', 'modules': ['math', 'numpy']}
syst = ManyAtomsSystem(atoms, 'FFLennardJones', numeric=numeric)

params = {'tstart': 0.0, 'tend': 14.0, 'tstep': 0.01, 'nterms': 4,
          'liouville': MPLPAQ, 'tqdm': True}
prop = Chebyshev(syst=syst, **params)
print('propagation begins')
prop.propagate()
prop.analyse()
(ti, qt, pt) = prop.get_trajectory_3d()
write_ase_trajectory(prop, atoms)

params = {'tstart': 0.0, 'tend': 14.0, 'tstep': 0.01, 'tqdm': True}
prop_ref = Symplectic(syst=syst, **params)
prop_ref.propagate()
prop_ref.analyse()
(tir, qtr, ptr) = prop_ref.get_trajectory_3d()

fig = plt.figure()
plot = fig.add_subplot(311)
plt.plot(tir, qtr[:, 0, 0], label='q0 verlet')
plt.plot(ti, qt[:, 0, 0], label='q0 chebyshev')
plt.plot(tir, qtr[:, 1, 0], label='q1 verlet')
plt.plot(ti, qt[:, 1, 0], label='q1 chebyshev')
plt.plot(tir, qtr[:, 2, 0], label='q2 verlet')
plt.plot(ti, qt[:, 2, 0], label='q2 chebyshev')
plot.set_ylabel(r'$q_0,\quad q_1,\quad q_2$')
plt.legend()

# vectorial momenta are invariant of translation
plot = fig.add_subplot(312)
plt.plot(tir, ptr[:, 0, 0], label='p0 verlet')
plt.plot(ti, pt[:, 0, 0], label='p0 chebyshev')
plt.plot(tir, ptr[:, 1, 0], label='p1 verlet')
plt.plot(ti, pt[:, 1, 0], label='p1 chebyshev')
plt.plot(tir, ptr[:, 2, 0], label='p2 verlet')
plt.plot(ti, pt[:, 2, 0], label='p2 chebyshev')
plot.set_ylabel(r'$p_0,\quad p_1,\quad p_2$')
plt.legend()

# inter-particle distances are invariant of translation and rotation
qrtr01 = [syst.get_distances(q)[0] for q in qtr]
qrt01 = [syst.get_distances(q)[0] for q in qt]
qrtr12 = [syst.get_distances(q)[2] for q in qtr]
qrt12 = [syst.get_distances(q)[2] for q in qt]

# abs. values of momenta are invariant of translation and rotation
prt0 = np.linalg.norm(pt[:, 0], axis=1)
prt1 = np.linalg.norm(pt[:, 1], axis=1)
prt2 = np.linalg.norm(pt[:, 2], axis=1)

plot = fig.add_subplot(313)
plt.plot(tir, qrtr01, label='r01 verlet')
plt.plot(ti, qrt01, label='r01 chebyshev')
plt.plot(tir, qrtr12, label='r12 verlet')
plt.plot(ti, qrt12, label='r12 chebyshev')
plt.plot(ti, prt0, label='pr0 chebyshev')
plt.plot(ti, prt1, label='pr1 chebyshev')
plt.plot(ti, prt2, label='pr2 chebyshev')
plot.set_ylabel(r'$r(t)$ $p_r(t)$')
plot.set_xlabel('time')
plt.legend()
# plt.savefig('result.png')
plt.show()
