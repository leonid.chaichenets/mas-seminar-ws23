""" Scalability measurement of the construction of (iL)^n f expressions for an
    N-particle 3D system """
import time
import traceback
import numpy as np
from itertools import combinations
from pandas.io.json import json_normalize
from rotagaporp.mpliouville import MPLPAQ, MPLPVQ, MPLPDR, MPLPDV, MPLPNR
from rotagaporp.chebyshev import Chebyshev
from rotagaporp.systems import MPPS3D

cases = (
     (MPLPAQ, None, {'tool': 'theano'}),
#     (MPLPVQ, None, {'tool': 'theano'}),
#     (MPLPDR, 'r', {'tool': 'theano'}),
#     (MPLPDR, 'dr', {'tool': 'theano'}),
     (MPLPDV, 'r', {'tool': 'theano'}),
     (MPLPDV, 'dv', {'tool': 'theano'}),
     (MPLPNR, 'r', {'tool': 'theano'}),
     (MPLPNR, 'dv', {'tool': 'theano'}),
)

params = {'tstart': 0.0, 'tend': 10.0, 'tstep': 0.001}
qvals = np.array([[0, 0, 0], [2, 0, 0], [0, 2, 0],
                  [0, 0, 2], [2, 2, 0], [0, 2, 2]])
pvals = np.array([[0, 0, 0], [-1, 0, 0], [0, -1, 0],
                  [0, 0, -1], [-1, -1, 0], [0, -1, -1]])

statistics = []
for nprtcls in range(2, 6):
    print('nprtcls:', nprtcls)
    symbols = [str(i)+'x:z' for i in range(nprtcls)]
    indic = list(range(3*nprtcls))
    pairs = list(combinations(zip(indic[0::3], indic[1::3], indic[2::3]), 2))
    ffparams = [{'class': 'FFLennardJones', 'pair': p, 'params': {}} for p in pairs]
    masses = np.full((nprtcls, ), 1)
    qval = qvals[:nprtcls]
    pval = pvals[:nprtcls]

    for nterms in range(2, 6):
        print('nterms:', nterms)
        for mplp, variant, numeric in cases:
            print('class:', mplp.__name__, 'variant:', variant,
                  'numeric:', numeric)
            stats = {}
            stats['propagator'] = 'chebyshev'
            stats['class'] = mplp.__name__
            stats['variant'] = variant
            stats['nterms'] = nterms
            stats['numeric tool'] = numeric['tool']
            stats['nparticles'] = nprtcls
            cheb_params = params.copy()
            cheb_params['nterms'] = nterms
            cheb_params['liouville'] = mplp
            cheb_params['syst'] = MPPS3D(ffparams, symbols, masses, qval, pval,
                                         numeric=numeric)
            cheb_params['liou_params'] = {'numeric': numeric}
            if variant is not None:
                cheb_params['liou_params']['variant'] = variant
            try:
                ts = time.time()
                cheb = Chebyshev(**cheb_params)
                te = time.time()
                stats['preparation time'] = te - ts
            except Exception as err:
                traceback.print_exc()
            try:
                ts = time.time()
                cheb.propagate()
                te = time.time()
                stats['propagation time'] = te - ts
            except Exception as err:
                traceback.print_exc()
            statistics.append(stats)

df = json_normalize(statistics)
df.to_json('time_cheb_lj_np_3d_theano.json', double_precision=15)
