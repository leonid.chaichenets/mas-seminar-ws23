""" One particle in Morse potential - Chebyshev and two-step Chebyshev """
import numpy as np
import matplotlib.pyplot as plt
from rotagaporp.chebyshev import Chebyshev, TwoStepChebyshev
from rotagaporp.symplectic import Symplectic
from rotagaporp.systems import Morse
from generic import spfloat

prec = 15

params = {'tstart': spfloat(0., prec), 'tend': spfloat(100., prec),
          'tstep': spfloat(0.01, prec), 'nterms': 6, 'tqdm': True}
syspar = {'q0': spfloat(3., prec), 'p0': spfloat(0., prec), 'fpprec': 'multi',
          'prec': prec}

system = Morse(**syspar)

# velocity Verlet
params_verlet = params.copy()
del params_verlet['nterms']
vv_prop = Symplectic(syst=system, **params_verlet)
vv_prop.propagate()
vv_prop.analyse()
print('energy drift of velocity verlet prop', np.max(np.fabs(vv_prop.er)))

# one-step propagator
params['fpprec'] = 'multi'
params['prec'] = prec
ch_prop = Chebyshev(syst=system, **params)
ch_prop.propagate()
time_ch, q_ch, p_ch = ch_prop.get_trajectory()
ch_prop.analyse()
print('energy drift of one-step prop', np.max(np.fabs(ch_prop.er)))

# two-step propagator
ts_prop = TwoStepChebyshev(syst=system, **params)
ts_prop.propagate()
time_ts, q_ts, p_ts = ts_prop.get_trajectory()
ts_prop.analyse()
print('energy drift of two-step prop', np.max(np.fabs(ts_prop.er)))

assert len(time_ts) == len(time_ch)
assert np.allclose(time_ts, time_ch)
q_an, p_an = system.solution(times=time_ts)

fig = plt.figure()
plot1 = fig.add_subplot(311)
plot1.plot(time_ch, np.maximum.accumulate(abs(ch_prop.er)), label='Chebyshev')
plot1.plot(time_ts, np.maximum.accumulate(abs(ts_prop.er)), label='Two-step Chebyshev')
plot1.plot(time_ts, np.maximum.accumulate(abs(vv_prop.er)), label='Velocity Verlet')
plot1.set_ylabel('maximum energy drift')
plot1.set_yscale('log')
plot1.set_xscale('log')
plot1.legend()

plot2 = fig.add_subplot(312)
plot2.plot(time_ts, q_ch, label='Chebyshev')
plot2.plot(time_ts, q_ts, label='Two-step Chebyshev')
plot2.plot(time_ts, q_an, label='Analytic solution')
plot2.set_xlabel(r'$t$')
plot2.set_ylabel(r'$q(t)$')
plot2.set_xscale('log')
plot2.legend()

plot3 = fig.add_subplot(313)
plot3.plot(time_ts, p_ch, label='Chebyshev')
plot3.plot(time_ts, p_ts, label='Two-step Chebyshev')
plot3.plot(time_ts, p_an, label='Analytic solution')
plot3.set_xlabel(r'$t$')
plot3.set_ylabel(r'$p(t)$')
plot3.set_xscale('log')
plot3.legend()

plt.show()
