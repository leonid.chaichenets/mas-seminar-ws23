""" One dimensional Lennard-Jones gas with two particles """
import matplotlib.pyplot as plt
from rotagaporp.chebyshev import Chebyshev
from rotagaporp.symplectic import Symplectic
from rotagaporp.systems import MPPS1D
from rotagaporp.mpliouville import MPLPQP

syspar = {'symbols': ['0', '1'], 'q0': [[0], [2]], 'p0': [[0], [-1]],
          'masses': [1.0, 1.0]}
ffpars = [{'class': 'FFLennardJones', 'pair': ((0,), (1,))}]
syst = MPPS1D(ffpars, **syspar)

params = {'tstart': 0.0, 'tend': 2.0, 'tstep': 0.01, 'nterms': 4,
          'liouville': MPLPQP}
prop = Chebyshev(syst=syst, **params)
print('propagation begins')
prop.propagate()
prop.analyse()
(ti, qt, pt) = prop.get_trajectory()

params = {'tstart': 0.0, 'tend': 2.0, 'tstep': 0.01}
prop_ref = Symplectic(syst=syst, **params)
prop_ref.propagate()
prop_ref.analyse()
(tir, qtr, ptr) = prop_ref.get_trajectory()

fig = plt.figure()
plot = fig.add_subplot(211)
plt.plot(tir, qtr[:, 0], label='q0 symplectic')
plt.plot(ti, qt[:, 0], label='q0 chebyshev')
plt.plot(tir, qtr[:, 1], label='q1 symplectic')
plt.plot(ti, qt[:, 1], label='q1 chebyshev')
plot.set_ylabel(r'$q_0,\quad q_1$')
plt.legend()
plot = fig.add_subplot(212)
plt.plot(tir, ptr[:, 0], label='p0 symplectic')
plt.plot(ti, pt[:, 0], label='p0 chebyshev')
plt.plot(tir, ptr[:, 1], label='p1 symplectic')
plt.plot(ti, pt[:, 1], label='p1 chebyshev')
plot.set_xlabel('time')
plot.set_ylabel(r'$p_0,\quad p_1$')
plt.legend()
plt.show()
