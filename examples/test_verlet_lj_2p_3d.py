""" Three dimensional Lennard-Jones gas with two particles """
import numpy as np
from ase import Atoms
import matplotlib.pyplot as plt
from rotagaporp.symplectic import Symplectic
from rotagaporp.systems import ManyAtomsSystem
from utilities.units import LJUnitsElement
from utilities.trajectory import write_ase_trajectory

argon_u = LJUnitsElement('Ar')
symbols = 'ArAr'
positions = np.array([[1, 0, 0], [3, 0, 0]])*argon_u.sigmaA
momenta = np.array([[0, 0, 0], [-1, 0, 0]])*argon_u.momentumA
atoms = Atoms(symbols, positions=positions, momenta=momenta)
# numeric = {'tool': 'lambdify', 'modules': ['math', 'numpy']}
# numeric = {'tool': 'subs'}
numeric = {'tool': 'theano'}
syst = ManyAtomsSystem(atoms, 'FFLennardJones', numeric=numeric)

params = {'tstart': 0.0, 'tend': 28.0, 'tstep': 0.005, 'tqdm': True}
prop_ref = Symplectic(syst=syst, **params)
prop_ref.propagate()
prop_ref.analyse()
enr, err = prop_ref.en, prop_ref.er
(tir, qtr, ptr) = prop_ref.get_trajectory_3d(step=1)
write_ase_trajectory(prop_ref, atoms, step=1)

# distance between the two particles with time
rtr = [syst.get_distances(q)[0] for q in qtr]

fig = plt.figure()
plot = fig.add_subplot(411)
plt.plot(tir, qtr[:, 0, 0], label='q0 verlet')
plt.plot(tir, qtr[:, 1, 0], label='q1 verlet')
plot.set_ylabel(r'$q_0,\quad q_1$')
plt.legend()

plot = fig.add_subplot(412)
plt.plot(tir, ptr[:, 0, 0], label='p0 verlet')
plt.plot(tir, ptr[:, 1, 0], label='p1 verlet')
plot.set_ylabel(r'$p_0,\quad p_1$')
plt.legend()

plot = fig.add_subplot(413)
plt.plot(tir, rtr, label='r verlet')
plot.set_ylabel(r'$r(t)$')
plot.set_xlabel('time')
plt.legend()

plot = fig.add_subplot(414)
plt.plot(tir, enr, label='energy, verlet')
plt.plot(tir, err, label='energy error, verlet')
plot.set_ylabel(r'$E(r)\quad\Delta E(t)$')
plot.set_xlabel('time')
plt.legend()

plt.show()
