""" Test the polynomial expansion for a free particle """
import sympy as sp
from rotagaporp.liouville import SPLP

q = sp.Symbol('q')
p = sp.Symbol('p')
hamiltonian = p**2/2
nterms = 4

ilnq = SPLP(nterms, hamiltonian, q, p, q, recursion='chebyshev', sign=1)
ilnp = SPLP(nterms, hamiltonian, q, p, p, recursion='chebyshev', sign=1)
print(ilnq, ilnp)
print(ilnq.Lf, ilnp.Lf)
