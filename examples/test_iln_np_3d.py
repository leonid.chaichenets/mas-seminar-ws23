""" Analysis of the terms of three-particle (iL)^n in three dimensions """
import yaml
import numpy as np
from itertools import combinations
from rotagaporp.mpliouville import MPLPDV, get_stats_expr, get_stats_repl
from rotagaporp.systems import MPPS3D

nprtcls = 3
# numeric = {'tool': 'subs'}
numeric = {'tool': 'lambdify', 'modules': 'math'}

symbols = [str(i)+'x:z' for i in range(nprtcls)]
indic = list(range(3*nprtcls))
pairs = list(combinations(zip(indic[0::3], indic[1::3], indic[2::3]), 2))
ffparams = [{'class': 'FFLennardJones', 'pair': p, 'params': {}} for p in pairs]
masses = np.full((nprtcls, ), 1)
qval = np.random.rand(nprtcls, 3)
pval = np.random.rand(nprtcls, 3)
system = MPPS3D(ffparams, symbols, masses, qval, pval, numeric=numeric)

nterms = 4

iln = MPLPDV(nterms, system, variant='r', numeric=numeric)
# print(yaml.dump(get_stats_expr(iln.ilnqexpr, details=True)))
# print(yaml.dump(get_stats_expr(iln.ilnpexpr, details=True)))
# print(yaml.dump(get_stats_repl(iln.repl, details=True)))
print(iln.get_val_float(system.q0, system.p0))
