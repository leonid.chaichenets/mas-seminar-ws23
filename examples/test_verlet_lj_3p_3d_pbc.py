""" Three dimensional Lennard-Jones gas with three particles """
import numpy as np
from ase import Atoms
import matplotlib.pyplot as plt
from rotagaporp.symplectic import Symplectic
from rotagaporp.systems import ManyAtomsSystem
from utilities.units import LJUnitsElement
from utilities.trajectory import write_ase_trajectory

argon_u = LJUnitsElement('Ar')
symbols = 'ArArAr'
pbc = (True, True, True)
cell = np.array([15, 15, 15])*argon_u.sigmaA
positions = np.array([[2, 2, 3], [4, 2, 3], [0, 2, 3]])*argon_u.sigmaA
momenta = np.array([[0, 0, 0], [-2, 0, 0], [0, 0, 0]])*argon_u.momentumA
atoms = Atoms(symbols, cell=cell, pbc=pbc, positions=positions, momenta=momenta)

# positions and momenta rotated by Euler angles 45, 30 and 10 deg
atoms_aux = atoms.copy()
atoms_aux.set_positions(momenta)
atoms.euler_rotate(phi=45, theta=30, psi=10)
atoms_aux.euler_rotate(phi=45, theta=30, psi=10)
atoms.set_momenta(atoms_aux.get_positions())
# numeric = {'tool': 'lambdify', 'modules': ['math', 'numpy']}
# numeric = {'tool': 'subs'}
numeric = {'tool': 'theano'}
syst = ManyAtomsSystem(atoms, 'FFLennardJones', numeric=numeric)

params = {'tstart': 0.0, 'tend': 14.0, 'tstep': 0.005, 'tqdm': True, 'pbc': True}
prop_ref = Symplectic(syst=syst, **params)
prop_ref.propagate()
prop_ref.analyse(step=10)
write_ase_trajectory(prop_ref, atoms, step=10)
(tir, qtr, ptr) = prop_ref.get_trajectory_3d(step=10)

fig = plt.figure()
plot = fig.add_subplot(311)
plt.plot(tir, qtr[:, 0, 0], label='q0 verlet')
plt.plot(tir, qtr[:, 1, 0], label='q1 verlet')
plt.plot(tir, qtr[:, 2, 0], label='q2 verlet')
plot.set_ylabel(r'$q_0,\quad q_1,\quad q_2$')
plt.legend()

# vectorial momenta are invariant of translation
plot = fig.add_subplot(312)
plt.plot(tir, ptr[:, 0, 0], label='p0 verlet')
plt.plot(tir, ptr[:, 1, 0], label='p1 verlet')
plt.plot(tir, ptr[:, 2, 0], label='p2 verlet')
plot.set_ylabel(r'$p_0,\quad p_1,\quad p_2$')
plt.legend()

# inter-particle distances are invariant of translation and rotation
qrtr01 = [syst.get_distances(q)[0] for q in qtr]
qrtr12 = [syst.get_distances(q)[2] for q in qtr]

# abs. values of momenta are invariant of translation and rotation
prt0 = np.linalg.norm(ptr[:, 0], axis=1)
prt1 = np.linalg.norm(ptr[:, 1], axis=1)
prt2 = np.linalg.norm(ptr[:, 2], axis=1)

plot = fig.add_subplot(313)
plt.plot(tir, qrtr01, label='r01 verlet')
plt.plot(tir, qrtr12, label='r12 verlet')
plt.plot(tir, prt0, label='p0 verlet')
plt.plot(tir, prt1, label='p1 verlet')
plt.plot(tir, prt2, label='p2 verlet')
plot.set_ylabel(r'$r(t)$ $p_r(t)$')
plot.set_xlabel('time')
plt.legend()
plt.show()
