""" Test the force-field derivatives in 3D """
import sympy as sp
import numpy as np
from rotagaporp.systems import MPPS3D

syspar_3d = {'symbols': ['0x:z', '1x:z'], 'q0': [[1, 2, 3],
             [1.7071, 2.7071, 3]], 'p0': [[0.7071, 0.7071, 0], [0, 0, 0]],
             'masses':  np.full(2, 2.0)}

ffpar_3d = {
    'harmonic': [{'class': 'FFHarmonic', 'pair': ((0, 1, 2), (3, 4, 5)),
                  'params': {'distance': 3.5, 'kappa':  sp.Rational(1, 2)}}],
    'lj': [{'class': 'FFLennardJones', 'pair': ((0, 1, 2), (3, 4, 5)),
            'params': {}}]
}

for ff in ('harmonic', 'lj'):
    syst_3d = MPPS3D(ffpar_3d[ff], **syspar_3d)
    print(ff)
    print(list(syst_3d.ff[0].get_cart_derivs(2, explicit=False)))
    print(list(syst_3d.ff[0].get_cart_derivs(2, explicit=True)))
    print(list(syst_3d.ff[0].get_inte_derivs(2)))
    print(list(syst_3d.ff[0].get_full_derivs(2)))
    print(list(syst_3d.ff[0].get_dist_derivs(2, explicit=False, mixed=False)))
    print(list(syst_3d.ff[0].get_dist_derivs(2, explicit=True, mixed=False)))
    print(list(syst_3d.ff[0].get_dist_derivs(2, explicit=False, mixed=True)))
