""" Two particles interacting in harmonic potential """
import sympy as sp
import numpy as np
import matplotlib.pyplot as plt
from rotagaporp.chebyshev import Chebyshev
from rotagaporp.symplectic import Symplectic
from rotagaporp.systems import Harmonic, MPPS1D
from rotagaporp.mpliouville import MPLPDV

M = np.full(2, 2.0)
r0 = 3.5
kappa = sp.Rational(1, 2)
syspar = {'symbols': ['0', '1'], 'q0': [[0], [1]], 'p0': [[1], [0]],
          'masses': M, 'numeric': {'tool': 'subs'}}
ffpar = [{'class': 'FFHarmonic', 'pair': ((0,), (1,)),
          'params': {'distance': r0, 'kappa': kappa}}]
syst = MPPS1D(ffpar, **syspar)

params = {'tstart': 0.0, 'tend': 4.0, 'tstep': 0.05, 'nterms': 4, 'liouville': MPLPDV, 'tqdm': True}

prop1 = Chebyshev(syst=syst, **params)
prop1.dump()

prop2 = Chebyshev(syst=syst, restart=True, **params)
prop2.propagate()
prop2.analyse()
(ti, qt, pt) = prop2.get_trajectory()

params = {'tstart': 0.0, 'tend': 4.0, 'tstep': 0.05}
prop_ref = Symplectic(syst=syst, **params)
prop_ref.propagate()
prop_ref.analyse()
(tir, qtr, ptr) = prop_ref.get_trajectory()

# analytic solution of equivalent 1p problem
sys_1p_par = {
    'q0': np.abs(syspar['q0'][0][0]-syspar['q0'][1][0])-r0,
    # from the motion of center of mass
    'p0': (M[0]*syspar['p0'][1][0]+M[1]*syspar['p0'][0][0])/np.sum(M),
    # from total kinetic energy = sum of particles's kinetic energies
    # 'p0': np.sqrt((M[0]*syspar['p0'][1]**2+M[1]*syspar['p0'][0]**2)/np.sum(M)),
    'mass': np.prod(M)/np.sum(M)
}
sys_1p = Harmonic(**sys_1p_par)
qrta = np.real(sys_1p.solution(sys_1p_par['q0'], sys_1p_par['p0'], tir)[0])
prta = np.real(sys_1p.solution(sys_1p_par['q0'], sys_1p_par['p0'], tir)[1])

# 2p -> 1p reduced chebyshev solution 
qrtr = np.abs(qt[:, 0]-qt[:, 1]) - r0
prtr = (pt[:, 1]*M[0]-pt[:, 0]*M[1])/np.sum(M)

fig = plt.figure()
plot = fig.add_subplot(311)
plot.plot(tir, qrta, label='r, analytic 1p')
plot.plot(tir, qrtr, label='q_r chebyshev 2p->1p reduced')
plot.plot(tir, prta, label='p_r analytic 1p')
plot.plot(tir, prtr, label='p_r chebyshev 2p->1p reduced')
plot.set_ylabel(r'$r,\quad p_r$')
plt.legend()

plot = fig.add_subplot(312)
plt.plot(tir, qtr[:, 0], label='q0 symplectic')
plt.plot(ti, qt[:, 0], label='q0 chebyshev')
plt.plot(tir, qtr[:, 1], label='q1 symplectic')
plt.plot(ti, qt[:, 1], label='q1 chebyshev')
plot.set_ylabel(r'$q_0,\quad q_1$')
plt.legend()

plot = fig.add_subplot(313)
plt.plot(tir, ptr[:, 0], label='p0 symplectic')
plt.plot(ti, pt[:, 0], label='p0 chebyshev')
plt.plot(tir, ptr[:, 1], label='p1 symplectic')
plt.plot(ti, pt[:, 1], label='p1 chebyshev')
plot.set_xlabel('time')
plot.set_ylabel(r'$p_0,\quad p_1$')
plt.legend()
plt.show()
