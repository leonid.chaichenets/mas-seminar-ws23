""" One particle in Morse potential - Chebyshev and symmetrized Chebyshev """
import numpy as np
import matplotlib.pyplot as plt
from rotagaporp.chebyshev import Chebyshev, SymmetricChebyshev
from rotagaporp.symplectic import Symplectic
from rotagaporp.systems import Morse
from generic import spfloat

prec = 8
numeric = {'tool': 'lambdify', 'modules': 'mpmath'}
# numeric = {'tool': 'lambdify', 'modules': 'numpy'}
# numeric = {'tool': 'subs'}
# numeric = {'tool': 'theano'}

params = {'tstart': spfloat(0., prec), 'tend': spfloat(100., prec),
          'tstep': spfloat(0.2, prec), 'nterms': 6, 'tqdm': True}
# params = {'tstart': 0., 'tend': 100., 'tstep': 0.2, 'nterms': 6, 'tqdm': True}

syspar = {'q0': spfloat(3., prec), 'p0': spfloat(0., prec),
          'fpprec': 'multi', 'prec': prec}

# syspar = {'q0': 3., 'p0': 0., 'fpprec': 'fixed'}

system = Morse(**syspar)

# velocity Verlet
params_verlet = params.copy()
del params_verlet['nterms']
vv_prop = Symplectic(syst=system, **params_verlet)
vv_prop.propagate()
time_vv, q_vv, p_vv = vv_prop.get_trajectory()
vv_prop.analyse()
print('energy drift of velocity verlet prop', np.max(np.fabs(vv_prop.er)))

q_an, p_an = system.solution(times=time_vv)
en0 = system.energy(system.q0, system.p0)
evecf = np.vectorize(system.energy)
err_an = np.array((evecf(q_an, p_an)-en0)/en0, dtype=float)
print('energy drift of analytic solution', np.max(np.fabs(err_an)))

# params['fpprec'] = 'fixed'
params['fpprec'] = 'multi'
params['prec'] = prec

# one-step propagator
ch_prop = Chebyshev(syst=system, **params)
ch_prop.propagate()
time_ch, q_ch, p_ch = ch_prop.get_trajectory()
ch_prop.analyse()
print('energy drift of one-step prop', np.max(np.fabs(ch_prop.er)))

params['numeric'] = numeric

# symmetrized propagator
params['signed'] = False
ts_prop = SymmetricChebyshev(syst=system, **params)
ts_prop.propagate()
time_ts, q_ts, p_ts = ts_prop.get_trajectory()
ts_prop.analyse()
print('energy drift of symmetric prop', np.max(np.fabs(ts_prop.er)))

assert len(time_ts) == len(time_ch) == len(time_vv)
assert np.allclose(time_ts, time_ch)
assert np.allclose(time_ts, time_vv)

fig = plt.figure()
plot1 = fig.add_subplot(311)
plot1.plot(time_ch, np.maximum.accumulate(abs(ch_prop.er)), label='Chebyshev')
plot1.plot(time_ts, np.maximum.accumulate(abs(ts_prop.er)), label='Symmetric Chebyshev')
plot1.plot(time_ts, np.maximum.accumulate(abs(vv_prop.er)), label='Velocity Verlet')
plot1.set_ylabel('maximum energy drift')
plot1.set_yscale('log')
plot1.set_xscale('log')
plot1.legend()

plot2 = fig.add_subplot(312)
plot2.plot(time_ts, q_ch, label='Chebyshev')
plot2.plot(time_ts, q_ts, label='Symmetric Chebyshev')
plot2.plot(time_ts, q_vv, label='Velocity Verlet')
plot2.plot(time_ts, q_an, label='Analytic solution')
plot2.set_xlabel(r'$t$')
plot2.set_ylabel(r'$q(t)$')
plot2.set_xscale('log')
plot2.legend()

plot3 = fig.add_subplot(313)
plot3.plot(time_ts, p_ch, label='Chebyshev')
plot3.plot(time_ts, p_ts, label='Symmetric Chebyshev')
plot3.plot(time_ts, p_vv, label='Velocity Verlet')
plot3.plot(time_ts, p_an, label='Analytic solution')
plot3.set_xlabel(r'$t$')
plot3.set_ylabel(r'$p(t)$')
plot3.set_xscale('log')
plot3.legend()

plt.show()
