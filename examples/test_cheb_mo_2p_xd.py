""" Two particles in Morse potential - 1P, 1D and 3D implementation """
import numpy as np
import matplotlib.pyplot as plt
from rotagaporp.chebyshev import Chebyshev
# from rotagaporp.newtonian import Newtonian
# from rotagaporp.symplectic import Symplectic
from rotagaporp.systems import Morse, MPPS1D, MPPS3D
from rotagaporp.mpliouville import MPLPQP
from rotagaporp.liouville import SPLPQP

propagator = Chebyshev
# propagator = Symplectic
# params_2p = {'tstart': 0.0, 'tend': 10., 'tstep': 0.1}
# params_1p = params_2p
params_2p = {'tstart': 0.0, 'tend': 10., 'tstep': 0.1, 'nterms': 4,
             'liouville': MPLPQP}
params_1p = {'tstart': 0.0, 'tend': 10., 'tstep': 0.1, 'nterms': 4,
             'liouville': SPLPQP}

M = [2.0, 2.0]
syspar_3d = {'symbols': ['0x:z', '1x:z'], 'q0': [[1, 0, 0], [4, 0, 0]],
          'p0': [[0, 0, 0], [0, 0, 0]], 'masses': M}
ffpars_3d = [{'class': 'FFMorse', 'pair': ((0, 1, 2), (3, 4, 5)),
              'params': {'distance': 1.}}]

syspar_1d = {'symbols': ['0', '1'], 'q0': [[1], [4]], 'p0': [[0], [0]],
          'masses': M}
ffpars_1d = [{'class': 'FFMorse', 'pair': ((0,), (1,)),
              'params': {'distance': 1.}}]

syspar_1p = {
    'q0': np.abs(syspar_1d['q0'][0][0]-syspar_1d['q0'][1][0]),
    'p0': (syspar_1d['p0'][1][0]*M[0]-syspar_1d['p0'][0][0]*M[1])/np.sum(M),
    'mass': np.prod(M)/np.sum(M)
}

print(syspar_1p)

# solve 2p in 1d and reduce 2p solution to 1p solution
syst_1d = MPPS1D(ffpars_1d, **syspar_1d)
prop_1d = propagator(syst=syst_1d, **params_2p)
prop_1d.propagate()
t, qt_1d, pt_1d = prop_1d.get_trajectory(step=5)
qt_1d_1p = np.abs(qt_1d[:, 0]-qt_1d[:, 1])
pt_1d_1p = (pt_1d[:, 1]*M[0]-pt_1d[:, 0]*M[1])/np.sum(M)

# solve 2p in 3d and reduce 2p solution to 1p solution
syst_3d = MPPS3D(ffpars_3d, **syspar_3d)
prop_3d = propagator(syst=syst_3d, **params_2p)
prop_3d.propagate()
t, qt_3d, pt_3d = prop_3d.get_trajectory_3d(step=5)
rt_3d = np.abs(qt_3d[:, 1]-qt_3d[:, 0])
qt_3d_1p = np.linalg.norm(rt_3d, axis=1)
pr_3d = (pt_3d[:, 1]*M[0]-pt_3d[:, 0]*M[1])/np.sum(M)
pt_3d_1p = np.einsum('ij,ij->i', pr_3d, rt_3d/qt_3d_1p[:, np.newaxis])

# solve equivalent 1p problem
syst_1p = Morse(**syspar_1p)
prop_1p = propagator(syst=syst_1p, **params_1p)
prop_1p.propagate()
t, qt_1p, pt_1p = prop_1p.get_trajectory(step=5)

fig = plt.figure()
plot1 = fig.add_subplot(211)
plot1.plot(t, qt_1p, label='q(1p)')
plot1.plot(t, qt_1d_1p, label='q(2p 1d)')
plot1.plot(t, qt_3d_1p, label='q(2p 3d)')
plot1.set_ylabel(r'$q(t)$')
plot1.legend()

plot2 = fig.add_subplot(212)
plot2.plot(t, pt_1p, label='p(1p)')
plot2.plot(t, pt_1d_1p, label='p(2p 1d)')
plot2.plot(t, pt_3d_1p, label='p(2p 3d)')
plot2.set_xlabel(r'$t$')
plot2.set_ylabel(r'$p(t)$')
plot2.legend()
plt.show()
