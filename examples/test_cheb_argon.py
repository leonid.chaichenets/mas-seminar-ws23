""" Molecular dynamics of argon at constant energy """
import time
from ase import units
from ase.build import bulk
from ase.md.velocitydistribution import MaxwellBoltzmannDistribution
from rotagaporp.systems import ManyAtomsSystem
from rotagaporp.chebyshev import Chebyshev
from rotagaporp.mpliouville import MPLPDV
from utilities.trajectory import write_ase_trajectory

T0_lj = 3.0 # initial temperature in Ar LJ units
rho_lj = 0.1 # 0.88 # density in Ar LJ units
sigma_a = 3.405*units.Angstrom # in Angstrom for Ar
epsilon_a = 1.65e-21*units.J # in Joule for Ar
T = T0_lj * epsilon_a / units.kB # initial temperature in Kelvin
rho = rho_lj/sigma_a**3 # density in atoms per cubic Angstrom
n_cell = 2 # number of unit cells in one direction
n_atoms = 4*n_cell**3 # total number of atoms, 4 atoms per unit cell
l_box = (n_atoms/rho)**(1./3) # edge length of simulation box
l_cell = l_box / n_cell # latice parameter

atoms = bulk('Ar', 'fcc', a=l_cell, cubic=True).repeat((n_cell, n_cell, n_cell))
rho_real = sum(atoms.get_masses())/atoms.get_volume()*units.m**3/units.kg
print('density, kg/m^3', rho_real)

prep_start = time.time()
MaxwellBoltzmannDistribution(atoms, T*units.kB)
syst = ManyAtomsSystem(atoms, 'FFLennardJones', numeric={'tool': 'theano'})
params = {'tstart': 0.0, 'tend': 10.0, 'tstep': 0.001, 'tqdm': True,
          'nterms': 4, 'liouville': MPLPVR, 'pbc': True}
prop = Chebyshev(syst=syst, **params)
prep_end = time.time()

print('preparation time:', prep_end-prep_start)
print('propagation begins')
prop_start = time.time()
prop.propagate()
prop_end = time.time()
print('propagation time:', prop_end-prop_start)
prop.analyse(step=10)
write_ase_trajectory(prop, atoms, step=10)
