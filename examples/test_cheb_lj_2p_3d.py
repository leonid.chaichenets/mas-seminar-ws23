""" Three dimensional Lennard-Jones gas with two particles """
import numpy as np
from ase import Atoms
import matplotlib.pyplot as plt
from rotagaporp.chebyshev import Chebyshev
from rotagaporp.symplectic import Symplectic
from rotagaporp.systems import ManyAtomsSystem
from rotagaporp.mpliouville import MPLPNR
from utilities.units import LJUnitsElement
from utilities.trajectory import write_ase_trajectory

argon_u = LJUnitsElement('Ar')
symbols = 'ArAr'
# positions translated with [1, 2, 3]:
positions = np.array([[1, 2, 3], [3, 2, 3]])*argon_u.sigmaA
momenta = np.array([[0, 0, 0], [-1, 0, 0]])*argon_u.momentumA
atoms = Atoms(symbols, positions=positions, momenta=momenta)

# positions and momenta rotated by Euler angles 45, 30 and 10 deg
atoms_aux = atoms.copy()
atoms_aux.set_positions(momenta)
atoms.euler_rotate(phi=45, theta=30, psi=10)
atoms_aux.euler_rotate(phi=45, theta=30, psi=10)
atoms.set_momenta(atoms_aux.get_positions())
numeric = {'tool': 'lambdify', 'modules': 'tensorflow'}
# numeric = {'tool': 'theano'}
# numeric = {'tool': 'subs'}
syst = ManyAtomsSystem(atoms, 'FFLennardJones', numeric=numeric)

params = {'tstart': 0.0, 'tend': 10.0, 'tstep': 0.005, 'nterms': 4,
          'liouville': MPLPNR, 'tqdm': True}
prop = Chebyshev(syst=syst, **params)
print('propagation begins')
prop.propagate()
# prop.analyse()
(ti, qt, pt) = prop.get_trajectory_3d()
write_ase_trajectory(prop, atoms)

params = {'tstart': 0.0, 'tend': 10.0, 'tstep': 0.005, 'tqdm': True}
prop_ref = Symplectic(syst=syst, **params)
prop_ref.propagate()
# prop_ref.analyse()
(tir, qtr, ptr) = prop_ref.get_trajectory_3d()

# 2p 3d distance
qrt = [syst.get_distances(q)[0] for q in qt]
qrtr = [syst.get_distances(q)[0] for q in qtr]

# 2p 3d momenta
prt0 = np.linalg.norm(pt[:, 0], axis=1)
prt1 = np.linalg.norm(pt[:, 1], axis=1)

fig = plt.figure()
plot = fig.add_subplot(311)
plt.plot(tir, qtr[:, 0, 0], label='q0 verlet')
plt.plot(ti, qt[:, 0, 0], label='q0 chebyshev')
plt.plot(tir, qtr[:, 1, 0], label='q1 verlet')
plt.plot(ti, qt[:, 1, 0], label='q1 chebyshev')
plot.set_ylabel(r'$q_0,\quad q_1$')
plt.legend()

# these are invariant of translation
plot = fig.add_subplot(312)
plt.plot(tir, ptr[:, 0, 0], label='p0 verlet')
plt.plot(ti, pt[:, 0, 0], label='p0 chebyshev')
plt.plot(tir, ptr[:, 1, 0], label='p1 verlet')
plt.plot(ti, pt[:, 1, 0], label='p1 chebyshev')
plot.set_ylabel(r'$p_0,\quad p_1$')
plt.legend()

# these are invariant of translation and rotation
plot = fig.add_subplot(313)
plt.plot(tir, qrtr, label='r verlet')
plt.plot(ti, qrt, label='r chebyshev')
plt.plot(ti, prt0, label='pr0 chebyshev')
plt.plot(ti, prt1, label='pr1 chebyshev')
plot.set_ylabel(r'$r(t)$ $p_r(t)$')
plot.set_xlabel('time')
plt.legend()

plt.show()
