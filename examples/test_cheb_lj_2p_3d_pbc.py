""" Three dimensional Lennard-Jones gas with two particles """
import numpy as np
from ase import Atoms
import matplotlib.pyplot as plt
from rotagaporp.chebyshev import Chebyshev
from rotagaporp.symplectic import Symplectic
from rotagaporp.systems import ManyAtomsSystem
from rotagaporp.mpliouville import MPLPDV
from utilities.units import LJUnitsElement

argon_u = LJUnitsElement('Ar')
symbols = 'ArAr'
pbc = (True, True, True)
cell = np.array([15, 15, 15])*argon_u.sigmaA
positions = np.array([[1, 0, 0], [3, 0, 0]])*argon_u.sigmaA
momenta = np.array([[0, 0, 0], [-1, 0, 0]])*argon_u.momentumA
atoms = Atoms(symbols, cell=cell, pbc=pbc, positions=positions, momenta=momenta)
# numeric = {'tool': 'subs'}
numeric = {'tool': 'theano'}
# numeric = {'tool': 'lambdify', 'modules': ['math', 'numpy']}
numeric = {'tool': 'lambdify', 'modules': 'tensorflow'}
syst = ManyAtomsSystem(atoms, 'FFLennardJones', numeric=numeric)

params = {'tstart': 0.0, 'tend': 14.0, 'tstep': 0.005, 'nterms': 4,
          'liouville': MPLPDV, 'tqdm': True, 'pbc': True}

filename = 'restart.pkl'
prop1 = Chebyshev(syst=syst, **params)
prop1.dump(filename)

prop2 = Chebyshev(syst=syst, restart=True, restart_file=filename, **params)
print('propagation begins')
prop2.propagate()
# prop2.analyse()
(ti, qt, pt) = prop2.get_trajectory_3d()

params = {'tstart': 0.0, 'tend': 14.0, 'tstep': 0.005, 'tqdm': True,
          'pbc': True}
prop_ref = Symplectic(syst=syst, **params)
prop_ref.propagate()
# prop_ref.analyse()
(tir, qtr, ptr) = prop_ref.get_trajectory_3d()

# distance between the two particles with time
rt = [syst.get_distances(q)[0] for q in qt]
rtr = [syst.get_distances(q)[0] for q in qtr]

fig = plt.figure()
plot = fig.add_subplot(311)
plt.plot(tir, qtr[:, 0, 0], label='q0 verlet')
plt.plot(ti, qt[:, 0, 0], label='q0 chebyshev')
plt.plot(tir, qtr[:, 1, 0], label='q1 verlet')
plt.plot(ti, qt[:, 1, 0], label='q1 chebyshev')
plot.set_ylabel(r'$q_0,\quad q_1$')
plt.legend()

plot = fig.add_subplot(312)
plt.plot(tir, ptr[:, 0, 0], label='p0 verlet')
plt.plot(ti, pt[:, 0, 0], label='p0 chebyshev')
plt.plot(tir, ptr[:, 1, 0], label='p1 verlet')
plt.plot(ti, pt[:, 1, 0], label='p1 chebyshev')
plot.set_ylabel(r'$p_0,\quad p_1$')
plt.legend()

plot = fig.add_subplot(313)
plt.plot(tir, rtr, label='r verlet')
plt.plot(ti, rt, label='r chebyshev')
plot.set_ylabel(r'$r(t)$')
plot.set_xlabel('time')
plt.legend()

plt.show()
