from numpy.random import uniform, seed
# from matplotlib.mlab import griddata
from scipy.interpolate import griddata
import matplotlib.pyplot as plt
import numpy as np
# make up data.
npts = 200
x = uniform(-2, 2, npts)      # 200 random points between -2 to 2
y = uniform(-2, 2, npts)
z = x*np.exp(-x**2 - y**2)
# define grid.
xi = np.linspace(-2.1, 2.1, 100)	# 100 x grid points
yi = np.linspace(-2.1, 2.1, 200)	# 200 y grid points

# mlab.griddata accepts the xi and yi 1d arrays above with different lengths. 
# scipy's griddata requires a full grid array. This is created with the mgrid function. 
xi, yi = np.mgrid[-2.1:2.1:100j, -2.1:2.1:200j]
# grid the data.
# points = np.vstack((x,y)).T
zi = griddata((x,y), z, (xi, yi), method='cubic')
print(zi.tolist())
# contour the gridded data, plotting dots at the nonuniform data points.
CS = plt.contour(xi, yi, zi, 15, linewidths=0.5, colors='k')
CS = plt.contourf(xi, yi, zi, 15)
plt.colorbar()  # draw colorbar
# plot data points.
# plt.scatter(x, y, marker='o', s=5, zorder=10)
plt.xlim(-2, 2)
plt.ylim(-2, 2)
plt.title('griddata test (%d points)' % npts)
plt.show()
