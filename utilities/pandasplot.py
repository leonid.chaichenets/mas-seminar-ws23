""" utility classes to create xy plots and bar charts with pandas """
from itertools import cycle
import pandas as pd
import matplotlib
matplotlib.use('Qt5Cairo')
import matplotlib.pyplot as plt
import numpy as np

class PandasPlot:
    """ create xy plots and bar charts from a pandas DataFrame """

    color_defs = ['black', 'blue', 'green', 'red', 'darkturquoise', 'magenta',
                  'gold', 'darkorange', 'purple', 'brown', 'grey']

    def __init__(self, df, showx=None, showy=None, labels=None,
                 ptype='xyplot', plotobj=None, colors=None,
                 short_legend=False):
        if plotobj is None:
            fig = plt.figure()
            self.plotobj = fig.add_subplot(111)
        else:
            self.plotobj = plotobj
        self.plotobj.set_xlabel(labels[showx])
        self.plotobj.set_ylabel(labels[showy])
        self.df = df
        self.showx = showx
        self.showy = showy
        self.labels = labels
        self.ptype = ptype
        self.short_legend = short_legend
        self.colors = cycle(self.color_defs) if colors is None else colors

    @classmethod
    def from_file(cls, filename, **kwargs):
        """ import a dataframe from file """
        return cls(pd.read_json(filename, precise_float=True), **kwargs)

    def add_datasets(self, select=None, vary=None, marker='o', minx=-np.inf,
                     maxx=np.inf, miny=-np.inf, maxy=np.inf):
        """ add selected datasets from a dataframe to a plot object """

        sel = True
        labs = []
        for key in select:
            labs.append(self.labels[key]+': '+str(select[key]))
            sel &= (self.df[key] == select[key])
        if vary is None or len(vary) == 0:
            vary = {next(iter(select.keys())): [next(iter(select.values()))]}
        for key in vary:
            if self.ptype == 'barchart':
                self.nbars = len(vary[key])
                self.barwidth = 0.5/self.nbars
            for numb, val in enumerate(vary[key]):
                df2 = self.df[sel & (self.df[key] == val)].sort_values(self.showx)
                xdata = df2[self.showx].values
                ydata = df2[self.showy].values
                region = np.full(xdata.shape, True)
                region = np.logical_and(xdata >= minx, region)
                region = np.logical_and(xdata <= maxx, region)
                region = np.logical_and(ydata >= miny, region)
                region = np.logical_and(ydata <= maxy, region)
                xdata = xdata[region]
                ydata = ydata[region]
                if len(ydata) == 0:
                    continue
                legtx = self.labels[key] + ': ' + str(val)
                if not self.short_legend:
                    legtx = ', '.join(labs) + ', ' + legtx
                if self.ptype == 'xyplot':
                    self.plotobj.plot(xdata, ydata, marker=marker,
                                      c=next(self.colors), linestyle='-',
                                      label=legtx)
                elif self.ptype == 'barchart':
                    self.plotobj.bar(xdata+0.5*numb/self.nbars, ydata,
                                     self.barwidth, color=next(self.colors),
                                     label=legtx)

    def show(self, legend=False):
        """ displays the plot interactively """
        if self.ptype == 'barchart':
            index = self.df.sort_values(self.showx)[self.showx].unique()
            self.plotobj.set_xticks(index+self.barwidth*(self.nbars-1)/2)
            self.plotobj.set_xticklabels(index)
        if legend: self.plotobj.legend()
        plt.show()

    def set_axis_type(self, xtype='linear', ytype='linear'):
        """ set axis type: linear, log, logit """
        self.plotobj.set_xscale(xtype)
        self.plotobj.set_yscale(ytype)

    def set_title(self, title=''):
        """ set plot title """
        self.plotobj.set_title(title)
