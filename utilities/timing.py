""" A simple timing function that can be used as decorator """
import time

def timeit(method):
    """ return runtime in seconds of a function """
    def timed(*args, **kw):
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()
        print('Time of', method.__name__, '(s):', te-ts)
        return result
    return timed
