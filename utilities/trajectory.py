""" Trajectory utility functions """
from ase import Atoms
from ase.io import write
from units import LJUnitsElement

def write_ase_trajectory(propagator, atoms, filename='trajectory.traj',
                         step=1):
    """ propagator is an object of any rotagaporp.Propagator subclass,
        currently restricted to a single element, no export of time axis """
    symbols = atoms.get_chemical_symbols()
    assert len(set(symbols)) == 1
    ljunits = LJUnitsElement(symbols[0])
    pbc = atoms.get_pbc()
    cell = atoms.get_cell()
    traj_3d = propagator.get_trajectory_3d(step)
    qs = traj_3d[1]*ljunits.sigmaA
    ps = traj_3d[2]*ljunits.momentumA
    traj = [Atoms(symbols, cell=cell, pbc=pbc, positions=q, momenta=p)
            for q, p in zip(qs, ps)]
    write(filename, traj)
