""" Functions to assess the performance and accuracy of propagators """
import time
import itertools
import numpy as np
import sympy as sp
import pandas as pd
from uuid import uuid4
from datetime import datetime

def assess_single(propagator, syst, params, l2error=False):
    """ Start and evaluate one parameter multiplet """
    import traceback
    res_columns = {}
    try:
        prop = propagator(syst=syst, **params)
        t1 = time.time()
        prop.propagate()
        t2 = time.time()
        prop.analyse()
    except Exception as err:
        traceback.print_exc()
        res_columns['alphan'] = None
        res_columns['time step efficiency'] = None
        res_columns['run time efficiency'] = None
        res_columns['energy drift'] = None
        res_columns['L^2 error'] = None
    else:
        if hasattr(prop, 'efficiency'):
            res_columns['alphan'] = prop.efficiency
        res_columns['time step efficiency'] = params['tstep']/(t2-t1)
        res_columns['run time efficiency'] = (params['tend']-params['tstart'])/(t2-t1)
        res_columns['energy drift'] = np.max(np.fabs(prop.er))
        if l2error:
            qt = prop.get_trajectory()[1]
            qr = syst.solution(times=prop.time)[0]
            res_columns['L^2 error'] = max(abs(qt-qr))
    return res_columns


def assess_multi(propagator, system, params, var_columns, var_columns_names,
        res_columns_names, con_columns, con_columns_names, indx='time stamp',
        l2error=False):
    """ Generate multiplets from parameters ranges and start evaluations """
    data_values = []
    data_index = []
    for multiplet in itertools.product(*var_columns):
        for name, value in zip(var_columns_names, multiplet):
            params[name] = value
        res_columns = assess_single(propagator, system, params, l2error=l2error)
        rvalues = list(multiplet)
        rvalues.extend(res_columns[param] for param in res_columns_names)
        rvalues.extend(param for param in con_columns)
        is_spfloat = lambda f: isinstance(f, sp.Float) or f == sp.S.Zero
        spfloat2float = lambda f: float(f) if is_spfloat(f) else f
        data_values.append(list(map(spfloat2float, rvalues)))
        if indx == 'time stamp':
            data_index.append(datetime.now())
        else:
            data_index.append(str(uuid4()))

    dat_columns_names = var_columns_names + res_columns_names + con_columns_names
    df = pd.DataFrame(
        np.array(data_values),
        index=pd.to_datetime(data_index),
        columns=dat_columns_names)
    df.to_json('results.json', double_precision=15)
