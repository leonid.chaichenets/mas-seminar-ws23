import time
import numpy as np
import sympy as sp
from itertools import chain

# list1 = list(range(1000000))
# list2 = list(range(2000000))

list1 = sp.Array(sp.symbols(' '.join(['q'+str(s) for s in range(1000000)])))
list2 = sp.Array(sp.symbols(' '.join(['p'+str(s) for s in range(2000000)])))

list1 = np.array(list1)
list2 = np.array(list2)

#list1 = np.random.rand(1000000)
#list2 = np.random.rand(2000000)

timings = {'append': 0.0, 'add': 0.0, 'extend': 0.0, 'unpack': 0.0, 'chain': 0.0, 'concatenate': 0.0}

for repeat in range(1):
#    start = time.time()
#    for el in list2:
#        list1.append(el)
#    end = time.time()
#    timings['append'] += (end-start)

#    start = time.time()
#    list1.extend(list2)
#    end = time.time()
#    timings['extend'] += (end-start)

#    start = time.time()
#    list3 = list1 + list2
#    end = time.time()
#    print(list3)
#    timings['add'] += (end-start)

    start = time.time()
    list4 = [*list1, *list2]
    end = time.time()
    timings['unpack'] += (end-start)

    start = time.time()
    list5 = list(chain(list1, list2))
    end = time.time()
    timings['chain'] += (end-start)

    start = time.time()
    list6 = np.concatenate((list1, list2))
    end = time.time()
    timings['concatenate'] += (end-start)

print(timings)
