""" Effort-Accuracy of Chebyshev propagator for an N-particle 3D system """

import numpy as np
from mpliouville import MPLPAQ
from chebyshev import Chebyshev
from pandas.io.json import json_normalize
from systems import MPPS3D
import time

# system parameters
numeric = {'tool': 'lambdify', 'modules': ['mpmath']}
# numeric = {'tool': 'subs'}
masses = np.array([2., 2.])
qval = np.array([[1., 0., 0.], [4., 0., 0.]])
pval = np.array([[0., 0., 0.], [0., 0., 0.]])
symbols = ['0x:z', '1x:z']
ffparams = [{'class': 'FFMorse', 'pair': ((0, 1, 2), (3, 4, 5)),
             'params': {'distance': 1.}}]
syspar = {'fpprec': 'multi', 'prec': 30, 'numeric': numeric}

# propagator parameters
variant = None
mplp = MPLPAQ
propagator = 'chebyshev'
params = {'tstart': 0., 'tend': 10., 'delta': 1., 'fpprec': 'multi', 'prec': 30}
tstep = 0.01
nterms = 2

statistics = []
stats = {}
stats['propagator'] = propagator
stats['class'] = mplp.__name__
stats['variant'] = variant
stats['nterms'] = nterms
stats['tstep'] = tstep
stats['numeric tool'] = numeric['tool']
stats['nparticles'] = 2
stats['delta'] = params['delta']
cheb_params = params.copy()
cheb_params['nterms'] = nterms
cheb_params['tstep'] = tstep
cheb_params['liouville'] = mplp
cheb_params['syst'] = MPPS3D(ffparams, symbols, masses, qval, pval, **syspar)
cheb_params['liou_params'] = {'numeric': numeric}
ts = time.time()
cheb = Chebyshev(**cheb_params)
te = time.time()
stats['preparation time'] = te - ts
stats['alphan'] = cheb.efficiency
ts = time.time()
cheb.propagate()
te = time.time()
cheb.analyse()
runtime = te - ts
stats['propagation time'] = runtime
print('cheb.er', cheb.er)
absvals = np.fabs(cheb.er)
print('absvals', absvals)
stats['energy drift'] = np.max(absvals)
print('edrift', stats['energy drift'])
stats['time step efficiency'] = tstep/runtime
rte = (params['tend']-params['tstart'])/runtime
stats['run time efficiency'] = rte
statistics.append(stats)

df = json_normalize(statistics)
df.to_json('time_cheb_subs_testing_only.json', double_precision=15)
