import numpy as np
from utilities.timing import timeit

def uniq1(seq):
    """ filter out duplicated elements and return the unique ones, faster """
    seen = set()
    seen_add = seen.add
    return [x for x in seq if not (x in seen or seen_add(x))]

def uniq2(seq):
    """ filter out duplicated elements and return the unique ones, slower """
    res = []
    for test in seq:
        if not any(test == c for c in res):
            res.append(test)
    return res

@timeit
def time_uniq1(seq):
    return uniq1(seq)

@timeit
def time_uniq2(seq):
    return uniq2(seq)

for irange in [10, 100]:
    print('integers range', irange)
    seq = np.random.randint(0, irange, (1000000, ))
    assert time_uniq1(seq) == time_uniq2(seq)
