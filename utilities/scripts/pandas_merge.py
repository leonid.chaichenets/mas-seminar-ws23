import sys
import pandas as pd

dataframes = [pd.read_json(f, precise_float=True) for f in sys.argv[1:]]
df = pd.concat(dataframes, sort=False, ignore_index=True)
# df = pd.concat(dataframes, sort=False)
df.to_json('results_merged.json', double_precision=15)
