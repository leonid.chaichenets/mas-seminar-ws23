""" Molecular dynamics of argon at constant energy """

from ase import units
from ase.build import bulk
from ase.md.velocitydistribution import MaxwellBoltzmannDistribution
from ase.calculators.lj import LennardJones
from ase.md.verlet import VelocityVerlet
from ase.md import MDLogger
from ase.io.trajectory import Trajectory

T0_lj = 3.0 # initial temperature in Ar LJ units
rho_lj = 0.88 # density in Ar LJ units
sigma_a = 3.405*units.Angstrom # in Angstrom for Ar
epsilon_a = 1.65e-21*units.J # in Joule for Ar
T = T0_lj * epsilon_a / units.kB # initial temperature in Kelvin
rho = rho_lj/sigma_a**3 # density in atoms per cubic Angstrom
n_cell = 3 # number of unit cells
n_atoms = 4*n_cell**3 # 4 atoms in a unit cell
l_box = (n_atoms/rho)**(1/3) # edge length of simulation box
l_cell = l_box / n_cell # latice parameter

atoms = bulk('Ar', 'fcc', a=l_cell, cubic=True).repeat((n_cell, n_cell, n_cell))
rho_real = sum(atoms.get_masses())/atoms.get_volume()*units.m**3/units.kg
print('density, kg/m^3', rho_real)

lj_params = {'sigma': sigma_a, 'epsilon': epsilon_a}
MaxwellBoltzmannDistribution(atoms, T*units.kB)
atoms.set_calculator(LennardJones(**lj_params))

traj = Trajectory('argon.traj', 'w', atoms)
dyn = VelocityVerlet(atoms, 1.0*units.fs)
logger = MDLogger(dyn, atoms, 'argon.log', header=True, stress=True, mode='w')
dyn.attach(logger, interval=100)
dyn.attach(traj.write, interval=100)

logger() # log initial state
traj.write(atoms) # write initial structure
dyn.run(10000)
