import matplotlib
matplotlib.use('Qt5Cairo')
import matplotlib.pyplot as plt
from scipy.interpolate import griddata
import numpy as np
import pandas as pd

files = [
    'chebyshev_lj_delta/tstep=0.05/results_merged.json',
#    'chebyshev_anharmonic_high_delta_highprec/prec=60/results_merged.json',
#    'chebyshev_morse_high_delta_highprec/prec=60/results_merged.json',
]

dataframes = [pd.read_json(f, precise_float=True) for f in files]
df = pd.concat(dataframes, sort=False)
df = df[np.isfinite(df['energy drift'])]
df['total efficiency'] = np.log10(df['run time efficiency'])-np.log10(df['energy drift'])

df3 = df[(df['tstep'] == 0.05) &
         (df['propagator'] == 'chebyshev') &
         (df['energy drift'] < 1.e0)]
'''
df3 = df[(df['total efficiency'] > 2) &
         (df['delta'] == 12.0) & 
         (df['system'] == 'Morse') &
         (df['propagator'] == 'newtonian')]
'''
# x = df3['delta']
x = df3['alphan']
y = df3['nterms']
# z = df3['total efficiency']
# z1 = df3['run time efficiency']
z2 = np.log10(df3['energy drift'])
# xi = np.linspace(0, 200, 20)
xi = np.linspace(0, 0.8, 20)
yi = np.linspace(10, 160, 20)
# zi = griddata((x, y), z, (xi[None, :], yi[:, None]), method='cubic')
# zi1 = griddata((x, y), z1, (xi[None, :], yi[:, None]), method='cubic')
zi2 = griddata((x, y), z2, (xi[None, :], yi[:, None]), method='cubic')
# zi2 = griddata((x, y), z2, (xi[None, :], yi[:, None]), method='linear')

fig = plt.figure()
pltobj = fig.add_subplot(111)
# contour = pltobj.contour(xi, yi, zi)
# contour1 = plt.contour(xi, yi, zi1)
# contour2 = pltobj.contour(xi, yi, zi2, colors='g')
contour2 = pltobj.contour(xi, yi, zi2)
# plt.clabel(contour, contour.levels)
# plt.clabel(contour1, contour1.levels)
plt.clabel(contour2, contour2.levels, fmt='%1d')
# pltobj.set_xlabel(r'$\Delta L$')
pltobj.set_xlabel(r'$\alpha/N$')
pltobj.set_ylabel(r'$N$')
# plt.legend()
plt.show()
