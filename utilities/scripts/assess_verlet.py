from symplectic import Symplectic
import systems
import assess

system_name = 'Anharmonic'
system = getattr(systems, system_name)()
propagator = Symplectic

# constant parameters
params = {
    'q0': 1.1,
    'p0': 0.0,
    'tstart': 0.0,
    'tend': 10.0
}

# variable parameters
var = {
    'tstep': [0.001, 0.005, 0.01, 0.05, 0.1, 0.5, 1.0]
}

var_labels = {
    'tstep': r'$\Delta t$',
    'time step efficiency': r'$\Delta t/T_{runtime}$',
    'run time efficiency': r'$(t_{start}-t_{end})/T_{run time}$',
    'energy drift': r'$max(|\Delta E/ E|)$',
    'L^2 error': r'$max(|q-q_{ref}|)$',
    'propagator': 'propagator',
    'system': 'system'
}

var_columns_names = ['tstep']
res_columns_names = [
    'time step efficiency',
    'run time efficiency',
    'energy drift',
    'L^2 error'
]
con_columns_names = ['q0', 'p0', 'tstart', 'tend']

var_columns = [var[name] for name in var_columns_names]
con_columns = [params[name] for name in con_columns_names]
con_columns_names.extend(['propagator', 'system'])
con_columns.extend(['symplectic', system_name])

prop = Symplectic(syst=system, tstep=0.0001, **params)
prop.propagate()
q_ref = prop.get_trajectory()[1]

assess.assess_multi(propagator, system, params, var_columns, var_columns_names,
                    res_columns_names, con_columns, con_columns_names,
                    ref=q_ref)

assess.plot(select={'propagator': 'symplectic'}, vary={},
            showx='tstep', showy='L^2 error',
            labels=var_labels, xtype='log')
