from chebyshev import Chebyshev
from symplectic import Symplectic
import systems
import assess

system_name = 'Anharmonic'
system = getattr(systems, system_name)()
propagator = Chebyshev

# constant parameters
params = {
    'q0': 1.1,
    'p0': 0.0,
    'tstart': 0.0,
    'tend': 10.0
}

# variable parameters
var = {
    'delta': [0.01, 0.1, 1.0],
    'nterms': [10, 15, 20, 25, 30, 35, 40],
    'tstep': [0.2, 0.4, 0.6, 0.8, 1.0, 1.2, 1.4, 1.6, 1.8]
}

var_labels = {
    'delta': r'$\Delta\lambda$',
    'nterms': r'Number of terms $N$',
    'tstep': r'$\Delta t$',
    'alphan': r'$\alpha/N$',
    'time step efficiency': r'$\Delta t/T_{runtime}$',
    'run time efficiency': r'$(t_{start}-t_{end})/T_{run time}$',
    'energy drift': r'$max(|\Delta E/ E|)$',
    'L^2 error': r'$max(|q-q_{ref}|)$',
    'propagator': 'propagator',
    'system': 'system'
}

var_columns_names = ['delta', 'nterms', 'tstep']
res_columns_names = ['alphan', 'time step efficiency', 'run time efficiency',
                     'energy drift', 'L^2 error']
con_columns_names = ['q0', 'p0', 'tstart', 'tend']

var_columns = [var[name] for name in var_columns_names]
con_columns = [params[name] for name in con_columns_names]
con_columns_names.extend(['propagator', 'system'])
con_columns.extend(['chebyshev', system_name])

prop = Symplectic(syst=system, tstep=0.000001, **params)
prop.propagate()
q_ref = prop.get_trajectory()[1]

assess.assess_multi(propagator, system, params, var_columns, var_columns_names,
                    res_columns_names, con_columns, con_columns_names, ref=q_ref)

# assess.plot(select={'delta': 1.0}, vary={'nterms': var['nterms']}, title='Chebyshev',
#            showx='run time efficiency', showy='error', xtype='log', labels=var_labels)
