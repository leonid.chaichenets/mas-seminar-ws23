import itertools
import numpy as np
from symplectic import Symplectic
import matplotlib.pyplot as plt
import systems
import pickle

system = systems.Anharmonic

# constant parameters
params = {
    'q0': 1.1,
    'p0': 0.0,
    'tstart': 0.0,
    'tend': 0.1
}

# factors
factors = [5, 10, 40, 50, 100, 1000, 10000, 10000]

# run reference calculation
params['tstep'] = 0.0001
prop = Symplectic(syst=system(), **params)
prop.propagate()
(tir, sqr, spr) = prop.get_trajectory()

color_defs = ['g', 'r', 'c', 'm', 'y', 'k']
fig = plt.figure()
colors = itertools.cycle(color_defs)
plot = fig.add_subplot(121)
max_err_q = []
max_err_p = []
tsteps = []
for factor in factors:
    sq_ref = sqr[0::factor]
    sp_ref = spr[0::factor]
    ti_ref = tir[0::factor]
    params['tstep'] = ti_ref[1]-ti_ref[0]
    nsteps = (params['tend']-params['tstart'])/params['tstep']
    print(params['tstep'], int(np.floor(len(sqr)/nsteps)))
    prop = Symplectic(syst=system(), **params)
    prop.propagate()
    (ti_test, sq_test, sp_test) = prop.get_trajectory()

    diff_p = np.fabs(sp_ref-sp_test)
    diff_q = np.fabs(sq_ref-sq_test)
    diff_t = np.fabs(ti_ref-ti_test)
    c=next(colors)
    plot.plot(ti_test[1:], diff_q[1:], marker='o', c=c, linestyle='-',
              label='q '+str(params['tstep']))
    plot.plot(ti_test[1:], diff_p[1:], marker='x', c=c, linestyle='-',
              label='p '+str(params['tstep']))
    plot.set_xlabel('propagation time')
    plot.set_ylabel('L^2 error')
    plot.set_xscale('log')
    plot.set_yscale('log')
    plot.legend()
    max_err_q.append(np.max(diff_q))
    max_err_p.append(np.max(diff_p))
    tsteps.append(params['tstep'])
# second plot with the maximum errors
plot = fig.add_subplot(122)
plot.plot(tsteps, max_err_q, marker='o', c='b', linestyle='-', label='q')
plot.plot(tsteps, max_err_p, marker='x', c='b', linestyle='-', label='p')
plot.set_xlabel('time step size')
plot.set_ylabel('max L^2 error')
plot.set_xscale('log')
plot.set_yscale('log')
plot.legend()
fig.suptitle('propagator: velocity Verlet, system: Anharmonic')

with open('L2_test.pkl', 'wb') as fh:
    pickle.dump(fig, fh)
